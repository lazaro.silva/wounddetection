package br.edu.ipt.master.survey.model;

import java.util.Calendar;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import br.com.agilizeware.infra.model.EntityIf;
import br.edu.ipt.master.survey.validation.ValidationSisAop;

@javax.persistence.Entity
@Table(name="paciente")
public class Paciente implements EntityIf {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1437997469869522305L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_creation", nullable = false)
	private Date dtCreate;
	@Column(name="fk_user_creation")
	private Long idUserCreate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_updated", nullable = true)
	private Date dtUpdate;
	@Column(name="fk_user_updated", nullable = true)
	private Long idUserUpdate;

	@ValidationSisAop(required=true, label="lbl.name", sizeField=220)
	@Column(name="nome", nullable = false)
	private String nome;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_nascimento", columnDefinition="DATETIME", nullable=false)
	@ValidationSisAop(required=true, label="lbl.dt.nascimento", notAfterNow=true)
	private Calendar dtNascimento;
	@ValidationSisAop(required=false, label="lbl.prontuario", sizeField=200)
	@Column(name="nu_prontuario", nullable = false)
	private String nuProntuario;
	@ManyToOne
	@JoinColumn(nullable=false, name="fk_ferida")
	//@ValidationSisAop(required=true, label="", mainLabels={"lbl.id"})
	private Ferida ferida;
	
	@Transient
	private String foto;
	
	
	public Paciente() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtCreate() {
		return dtCreate;
	}

	public void setDtCreate(Date dtCreate) {
		this.dtCreate = dtCreate;
	}

	public Long getIdUserCreate() {
		return idUserCreate;
	}

	public void setIdUserCreate(Long idUserCreate) {
		this.idUserCreate = idUserCreate;
	}

	public Date getDtUpdate() {
		return dtUpdate;
	}

	public void setDtUpdate(Date dtUpdate) {
		this.dtUpdate = dtUpdate;
	}

	public Long getIdUserUpdate() {
		return idUserUpdate;
	}

	public void setIdUserUpdate(Long idUserUpdate) {
		this.idUserUpdate = idUserUpdate;
	}
	
	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Calendar getDtNascimento() {
		return dtNascimento;
	}

	public void setDtNascimento(Calendar dtNascimento) {
		this.dtNascimento = dtNascimento;
	}

	public String getNuProntuario() {
		return nuProntuario;
	}

	public void setNuProntuario(String nuProntuario) {
		this.nuProntuario = nuProntuario;
	}
	
	public Ferida getFerida() {
		return ferida;
	}

	public void setFerida(Ferida ferida) {
		this.ferida = ferida;
	}
	
	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}
	
	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return nome;
	}
}
