package br.edu.ipt.master.survey.rest;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.io.FilenameUtils;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import br.com.agilizeware.infra.dao.DaoAB;
import br.com.agilizeware.infra.dto.RestResultDto;
import br.com.agilizeware.infra.enums.ErrorCodeEnum;
import br.com.agilizeware.infra.exception.AgilizeException;
import br.com.agilizeware.infra.file.rest.AgilizeFileServiceRest;
import br.com.agilizeware.infra.model.File;
import br.com.agilizeware.infra.rest.ServiceRestEntityAb;
import br.com.agilizeware.infra.security.model.User;
import br.com.agilizeware.infra.security.rest.UserServiceRest;
import br.com.agilizeware.infra.util.AppPropertiesService;
import br.com.agilizeware.infra.util.Util;
import br.edu.ipt.master.survey.dao.FeridaDaoImpl;
import br.edu.ipt.master.survey.enums.CoberturasEnum;
import br.edu.ipt.master.survey.enums.FeridaErrorCodeEnum;
import br.edu.ipt.master.survey.model.Classificacao;
import br.edu.ipt.master.survey.model.ClassificacaoCobertura;
import br.edu.ipt.master.survey.model.ClassificacaoTecido;
import br.edu.ipt.master.survey.model.Ferida;
import br.edu.ipt.master.survey.model.Paciente;
import br.edu.ipt.master.survey.neuralnet.NetDetector;
import br.edu.ipt.master.survey.validation.ValidationSisExecute;

@RestController
@RequestMapping("/ferida")
@Service
public class FeridaServiceRest extends ServiceRestEntityAb<Paciente, Long> {
	
	/*@Autowired
	private UserServiceRest userServiceRest;*/
	@Autowired
	private AgilizeFileServiceRest fileService;
	@Autowired
	private FeridaDaoImpl feridaDaoIf;
	@Autowired
	private AppPropertiesService appPropertiesService;
	@Autowired
	private NetDetector netDetector;
	
	public FeridaServiceRest() {
		super();
	}

	@Override
	protected DaoAB<Paciente, Long> definirDao() {
		return feridaDaoIf;
	}
	
	private File convertStringToImage(String base64, Long idUser) throws IOException {
		
		Date dt = Util.obterDataHoraAtual();
		String base64Image = base64.split(",")[1];
		String name = Util.generateUuid().replaceAll("[^a-zZ-Z1-9 ]", "").substring(0, 8) + "_" + dt.getTime()+".jpg";
		byte[] imageBytes = javax.xml.bind.DatatypeConverter.parseBase64Binary(base64Image);
		String dir = appPropertiesService.getPropertyString("agilize.file.physical.path") + 
				java.io.File.separator + "TEMP";
		java.io.File fDir = new java.io.File(dir);
		if(!fDir.exists()) {
			fDir.mkdirs();
		}
		java.io.File fTemp = new java.io.File(dir + java.io.File.separator + name);
		FileCopyUtils.copy(imageBytes, fTemp);
		
		File saved = new File();
		saved.setContentType("image/jpg");
		saved.setDtCreate(dt);
		saved.setIdUserCreate(idUser);
		saved.setNmEntity("FERIDA_ORIGINAL");
		saved.setSize(fTemp.length());
		saved.setName(name);
		saved.setPathLogical(appPropertiesService.getPropertyString("agilize.file.logical.path"));
		saved.setPathPhysical(FilenameUtils.getFullPath(fTemp.getAbsolutePath()));

		return saved;
	}
	
	@RequestMapping(value="/salvarPaciente", method = RequestMethod.POST)
    @ResponseBody
	@Transactional(isolation=Isolation.READ_COMMITTED, rollbackFor=Exception.class)
    public RestResultDto<Paciente> salvarPaciente(@RequestBody Paciente record, HttpServletRequest httpRequest) throws IOException {
		
		validatePaciente(record);
		RestResultDto<Paciente> retorno = new RestResultDto<Paciente>();
		User usLogado = UserServiceRest.getUserAuthenticated(false);
		Date dtHoraAtual = Util.obterDataHoraAtual();
		
		record.setDtCreate(dtHoraAtual);
		record.setIdUserCreate(usLogado.getId());
		record.setFerida(new Ferida());
		record.getFerida().setDtCreate(dtHoraAtual);
		record.getFerida().setIdUserCreate(usLogado.getId());
		
		List<File> filesFerida = new ArrayList<File>(1);
		File fTemp = convertStringToImage(record.getFoto(), usLogado.getId());
		filesFerida.add(fTemp);
		RestResultDto<List<File>> restFiles = fileService.saveTempFile(filesFerida);
		if(Util.isNotNull(restFiles)) {
			if(!restFiles.getSuccess()) {
				retorno.setMsgError(restFiles.getMsgError());
				retorno.setStrAgilizeExceptionError(restFiles.getStrAgilizeExceptionError());
				retorno.setSuccess(false);
				return retorno;
			}
			record.getFerida().setArquivoOriginal(restFiles.getData().get(0));
		}
		else {
			throw new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), FeridaErrorCodeEnum.ERROR_IMAGEM_FERIDA_NAO_SALVA);
		}
	
		record = feridaDaoIf.salvarPaciente(record);
		record.getFerida().setClassificacao(processarImagem(record.getFerida(), dtHoraAtual, usLogado.getId()));
		
		retorno.setData(record);
		retorno.setSuccess(true);
		return retorno;
	}
	
	private Classificacao processarImagem(Ferida fer, Date dt, Long idUser) {
		
		Mat imRedimensionada = netDetector.redimensionarImagem(fer.getArquivoOriginal().getPathPhysical() + 
				java.io.File.separator + fer.getArquivoOriginal().getName());
		List<ClassificacaoTecido> labelsDetectadas = netDetector.detectWounds(imRedimensionada);
		java.io.File fileFerida = netDetector.saveDiagnosticTemp(imRedimensionada);
		
		File saved = new File();
		saved.setContentType("image/jpg");
		saved.setDtCreate(dt);
		saved.setIdUserCreate(idUser);
		saved.setNmEntity(NetDetector.NAME_ENTITY);
		saved.setSize(fileFerida.length());
		saved.setName(fileFerida.getName());
		saved.setPathLogical(appPropertiesService.getPropertyString("agilize.file.logical.path"));
		saved.setPathPhysical(FilenameUtils.getFullPath(fileFerida.getAbsolutePath()));
		RestResultDto<File> restFile = fileService.save(saved);
		if(Util.isNotNull(restFile)) {
			if(!restFile.getSuccess()) {
				throw restFile.getError();
			}
			fer.setArquivoClassificado(restFile.getData());
		}
		else {
			throw new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), FeridaErrorCodeEnum.ERROR_IMAGEM_FERIDA_NAO_SALVA);
		}
		
		Classificacao cls = new Classificacao();
		cls.setDtCreate(dt);
		cls.setIdUserCreate(idUser);
		if(Util.isListNotNull(labelsDetectadas)) {
			cls.setTecidos(new ArrayList<ClassificacaoTecido>(1));
			ClassificacaoCobertura cb;
			List<CoberturasEnum> coberturas;
			for(ClassificacaoTecido ct : labelsDetectadas) {
				ct.setDtCreate(dt);
				ct.setIdUserCreate(idUser);
				ct.setTecido(feridaDaoIf.findTecidoByLabel(ct.getLabel()));
				ct.setCoberturasClassificadas(new ArrayList<ClassificacaoCobertura>(1));
				
				coberturas = netDetector.getCoberturasPorClassificacao(ct.getLabel());
				if(Util.isListNotNull(coberturas)) {
					for(CoberturasEnum cobEnum : coberturas) {
						cb = new ClassificacaoCobertura();
						cb.setDtCreate(dt);
						cb.setIdUserCreate(idUser);
						cb.setCoberturaClassificada(cobEnum);
						cb.setClassificacaoTecido(ct);
						
						ct.getCoberturasClassificadas().add(cb);
					}
				}
				
				cls.getTecidos().add(ct);
			}
		}
		
		cls = feridaDaoIf.salvarClassificacao(cls, fer);
		return cls;
	}
	
	//TODO: Revisar
	@RequestMapping(value="/fecharAnalise", method = RequestMethod.PUT)
    @ResponseBody
	@Transactional(isolation=Isolation.READ_COMMITTED, rollbackFor=Exception.class)
    public RestResultDto<Long> fecharAnalise(@RequestBody Classificacao record, HttpServletRequest httpRequest) {
		validateClassificacao(record);
		User usLogado = UserServiceRest.getUserAuthenticated(false);
		Date dtHoraAtual = Util.obterDataHoraAtual();
		record.setDtUpdate(dtHoraAtual);
		record.setIdUserUpdate(usLogado.getId());
		feridaDaoIf.alterarClassificacao(record);
		
		if(Util.isListNotNull(record.getTecidos())) {
			for(ClassificacaoTecido ct : record.getTecidos()) {
				if(Util.isListNotNull(ct.getCoberturasClassificadas())) {
					for(ClassificacaoCobertura cob : ct.getCoberturasClassificadas()) {
						cob.setDtUpdate(dtHoraAtual);
						cob.setIdUserUpdate(usLogado.getId());
						feridaDaoIf.alterarClassificacaoCobertura(cob);
					}
				}
			}
		}
		
		RestResultDto<Long> retorno = new RestResultDto<Long>();
		retorno.setSuccess(true);
		retorno.setData(200L);
		return retorno;
	}
	
	@RequestMapping(value="/pesquisarPacientes", method = RequestMethod.GET)
    @ResponseBody
    public RestResultDto<List<Paciente>> pesquisarPacientes(HttpServletRequest httpRequest) throws IOException {
		
		User usLogado = UserServiceRest.getUserAuthenticated(false);
		Long idUsuario = null;
		if(!usLogado.getSuperAdm()) {
			idUsuario = usLogado.getId();
		}
		List<Paciente> pacientes = feridaDaoIf.pesquisar(idUsuario);
		/*ResponseEntity<Resource> re = null;
		for(Paciente p : pacientes) {
			re = fileService.getFileById(p.getFerida().getArquivoOriginal().getId());
			p.getFerida().setFotoResource(re.getBody().getInputStream());
		}*/
		RestResultDto<List<Paciente>> retorno = new RestResultDto<List<Paciente>>();
		retorno.setSuccess(true);
		retorno.setData(pacientes);
		return retorno;
	}
	
	@RequestMapping(value="/nomeCobertura", method = RequestMethod.GET)
    @ResponseBody
    public RestResultDto<String> nomeCobertura(HttpServletRequest httpRequest, @RequestParam(name="label") String label) throws IOException {
		
		String nome = appPropertiesService.getPropertyString(label);
		RestResultDto<String> retorno = new RestResultDto<String>();
		retorno.setSuccess(true);
		retorno.setData(nome);
		return retorno;
	}
	
	private void validatePaciente(Paciente record) {
		
		List<AgilizeException> arrays = new ArrayList<AgilizeException>(1);
		List<String> fieldsForm = new ArrayList<String>(1);
		
		arrays.addAll(ValidationSisExecute.executeValidation(record, "lbl.form.paciente", Paciente.class, fieldsForm));
		
		if(!Util.isNotNull(record.getFoto())) {
			arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.REQUIRED_FIELD, "lbl.imagem"));
		}
		
		if(Util.isListNotNull(arrays)) {
			AgilizeException erro = new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.REQUIREDS_FIELD, arrays);
			erro.setFieldsFormMarked(fieldsForm);
			throw erro;
		}
	}
	
	/**
	 * TODO: Revisar
	 * @param record
	 */
	private void validateClassificacao(Classificacao record) {
		
		List<AgilizeException> arrays = new ArrayList<AgilizeException>(1);
		
		if(record == null) {
			arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.REQUIRED_FIELD, "lbl.form"));
		}
		else {
			
			if(!Util.isNotNull(record.getId())) {
				arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.REQUIRED_FIELD, "lbl.id"));
			}
			
			/*if(!Util.isNotNull(record.getIsAceito())) {
				arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.REQUIRED_FIELD, "lbl.aceitacao.cobertura"));
			}
			else {
				if(record.getIsAceito() && !Util.isNotNull(record.getCoberturaAceita())) {
					arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.REQUIRED_FIELD, "lbl.qual.aceitacao.cobertura"));
				}
				else if(!record.getIsAceito()) {
					record.setCoberturaAceita(null);
				}
			}*/
			
			/*if(!Util.isNotNull(record.getObservacoes())) {
				arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.REQUIRED_FIELD, "lbl.observacoes"));
			}
			else*/ if(Util.isNotNull(record.getObservacoes()) && record.getObservacoes().length() > 2000) {
				arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.INVALID_LENGTH_FIELD, "lbl.observacoes", "2000"));
			}
		}
		
		if(Util.isListNotNull(arrays)) {
			AgilizeException erro = new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.REQUIREDS_FIELD, arrays);
			throw erro;
		}
	}
}
