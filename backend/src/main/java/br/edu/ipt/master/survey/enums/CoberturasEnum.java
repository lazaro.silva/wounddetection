package br.edu.ipt.master.survey.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.agilizeware.infra.enums.IEnum;
import br.com.agilizeware.infra.util.Util;

@JsonDeserialize(using = CoberturasEnumDeserializer.class)
public enum CoberturasEnum implements IEnum<Integer> {
	
	HIDROGEL(1, "HIDROGEL", "lbl.enum.coberturas.HIDROGEL"),
	PAPAINA_GEL_4PERC(2, "PAPAINA_GEL_4PERC", "lbl.enum.coberturas.PAPAINA_GEL_4PERC"),
	COLAGENASE(3, "COLAGENASE", "lbl.enum.coberturas.COLAGENASE"),
	POLIHEXANIDA_SOLUC(4, "POLIHEXANIDA_SOLUC", "lbl.enum.coberturas.POLIHEXANIDA_SOLUC"),
	POLYMEN(5, "POLYMEN", "lbl.enum.coberturas.POLYMEN"),
	MEPILEX_AG(6, "MEPILEX_AG", "lbl.enum.coberturas.MEPILEX_AG"),
	PAPAINA_GEL_4_6PERC(7, "PAPAINA_GEL_4_6PERC", "lbl.enum.coberturas.PAPAINA_GEL_4_6PERC"),
	PAPAINA_GEL_8_10PERC(8, "PAPAINA_GEL_8_10PERC", "lbl.enum.coberturas.PAPAINA_GEL_8_10PERC"),
	PAPAINA_GEL_10PERC(9, "PAPAINA_GEL_10PERC", "lbl.enum.coberturas.PAPAINA_GEL_10PERC"),
	PAPAINA_GEL_2PERC(10, "PAPAINA_GEL_2PERC", "lbl.enum.coberturas.PAPAINA_GEL_2PERC"),
	ADAPTIC(11, "ADAPTIC", "lbl.enum.coberturas.ADAPTIC"),
	AGE(12, "AGE", "lbl.enum.coberturas.AGE"),
	MEPTEL(13, "MEPTEL", "lbl.enum.coberturas.MEPTEL"),
	HIDROCOLOIDE(14, "HIDROCOLOIDE", "lbl.enum.coberturas.HIDROCOLOIDE"),
	ALGINATO_CALCIO(15, "ALGINATO_CALCIO", "lbl.enum.coberturas.ALGINATO_CALCIO"),
	FIBRACOL(16, "FIBRACOL", "lbl.enum.coberturas.FIBRACOL"),
	MEPLEX_BORDER(17, "MEPLEX_BORDER", "lbl.enum.coberturas.MEPLEX_BORDER"),
	ALGINATO_CALCIO_PRATA(18, "ALGINATO_CALCIO_PRATA", "lbl.enum.coberturas.ALGINATO_CALCIO_PRATA"),
	;
	
	private Integer codigo;
	private String descricao;
	private String label;
	
	private CoberturasEnum(Integer codigo, String descricao, String i18ln) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.label = i18ln;
	}
	
	public Integer getId() {
		return codigo;
	}

	public String getLabel() {
		return label;
	}

	public String getDescription() {
		return descricao;
	}
	
	public static CoberturasEnum findByLabel(String label) {
		
		if(!Util.isNotNull(label)) {
			return null;
		}
		
		for (CoberturasEnum enum1 : CoberturasEnum.values()) {
			if (enum1.getLabel().equals(label)) {
				return enum1;
			}
		}
		throw new IllegalArgumentException("Invalid label.");
	}
	
	public static CoberturasEnum findByCode(Integer code) {
		
		if(!Util.isNotNull(code)) {
			return null;
		}
		
		for (CoberturasEnum enum1 : CoberturasEnum.values()) {
			if (enum1.getId().equals(code)) {
				return enum1;
			}
		}
		throw new IllegalArgumentException("Invalid Code.");
	}
	
	public static CoberturasEnum findByIndice(Integer idx) {
		
		if(!Util.isNotNull(idx)) {
			return null;
		}
		
		CoberturasEnum[] array = CoberturasEnum.values();
		return array[idx];
	}
	
	public String toString() {
		return "{\"codigo\": "+codigo+", \"descricao\": \""+descricao+"\", \"label\": \""+label+"\"}";
	}
	

}
