package br.edu.ipt.master.survey.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonAutoDetect;

import br.com.agilizeware.infra.model.EntityIf;
import br.com.agilizeware.infra.model.File;
import br.edu.ipt.master.survey.validation.ValidationSisAop;

@javax.persistence.Entity
@Table(name="ferida")
@JsonAutoDetect(fieldVisibility = JsonAutoDetect.Visibility.ANY)
public class Ferida implements EntityIf {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2850577807858256029L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_creation", nullable = false)
	private Date dtCreate;
	@Column(name="fk_user_creation")
	private Long idUserCreate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_updated", nullable = true)
	private Date dtUpdate;
	@Column(name="fk_user_updated", nullable = true)
	private Long idUserUpdate;
	
	@ManyToOne
	@JoinColumn(nullable=false, name="fk_file_original")
	@ValidationSisAop(required=true, label="", mainLabels={"lbl.id"})
	private File arquivoOriginal;
	@ManyToOne
	@JoinColumn(nullable=true, name="fk_file_detectada")
	@ValidationSisAop(required=false, label="", mainLabels={"lbl.id"})
	private File arquivoClassificado;
	@ManyToOne
	@JoinColumn(nullable=true, name="fk_classificacao")
	@ValidationSisAop(required=false, label="", mainLabels={"lbl.id"})
	private Classificacao classificacao;
	
	/*@Transient
	private InputStream fotoResource;*/
	
	public Ferida() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtCreate() {
		return dtCreate;
	}

	public void setDtCreate(Date dtCreate) {
		this.dtCreate = dtCreate;
	}

	public Long getIdUserCreate() {
		return idUserCreate;
	}

	public void setIdUserCreate(Long idUserCreate) {
		this.idUserCreate = idUserCreate;
	}

	public Date getDtUpdate() {
		return dtUpdate;
	}

	public void setDtUpdate(Date dtUpdate) {
		this.dtUpdate = dtUpdate;
	}

	public Long getIdUserUpdate() {
		return idUserUpdate;
	}

	public void setIdUserUpdate(Long idUserUpdate) {
		this.idUserUpdate = idUserUpdate;
	}
	
	public File getArquivoOriginal() {
		return arquivoOriginal;
	}

	public void setArquivoOriginal(File arquivoOriginal) {
		this.arquivoOriginal = arquivoOriginal;
	}

	public File getArquivoClassificado() {
		return arquivoClassificado;
	}

	public void setArquivoClassificado(File arquivoClassificado) {
		this.arquivoClassificado = arquivoClassificado;
	}

	public Classificacao getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(Classificacao classificacao) {
		this.classificacao = classificacao;
	}
	
	/*public InputStream getFotoResource() {
		return fotoResource;
	}

	public void setFotoResource(InputStream fotoResource) {
		this.fotoResource = fotoResource;
	}*/

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return null;
	}

}
