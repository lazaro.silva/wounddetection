package br.edu.ipt.master.survey.configuration;

import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import br.com.agilizeware.infra.security.component.configuration.PrepareInitContainer;

@Component
public class SurveyPrepareInitContainer extends PrepareInitContainer {

	/**
     * This event is executed as late as conceivably possible to indicate that 
     * the application is ready to service requests.
     */
	  @Override
	  @Transactional(noRollbackFor=Throwable.class)
	  public void onApplicationEvent(final ApplicationReadyEvent event) {
			System.out.println("---- Inicio PrepareInitContainer -----");
			initializeRolesForSuperAdm();
			System.out.println("---- Fim PrepareInitContainer -----");
	  }
}
