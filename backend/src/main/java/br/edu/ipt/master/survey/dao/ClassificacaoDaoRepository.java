package br.edu.ipt.master.survey.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.agilizeware.infra.dao.CrudAgilizeRepositoryIF;
import br.edu.ipt.master.survey.enums.CoberturasEnum;
import br.edu.ipt.master.survey.model.Classificacao;

public interface ClassificacaoDaoRepository extends CrudAgilizeRepositoryIF<Classificacao, Long> {

	/*@Modifying(clearAutomatically = true)
	@Query(" update Classificacao c set c.coberturaAceita = :coberturaAceita, c.isAceito = :isAceito, c.observacoes = :observacoes " + 
		   " c.dtUpdate = :dtUpdate, c.idUserUpdate = :idUserUpdate where c.id = :id ")
	void alterarClassificacao(@Param("coberturaAceita") CoberturasEnum coberturaAceita, @Param("isAceito") Boolean isAceito,
			@Param("observacoes") String observacoes, @Param("dtUpdate") Date dtUpdate, @Param("idUserUpdate") Long idUserUpdate,
			@Param("id") Long id);*/
	
	/*@Modifying
	@Query(" update Classificacao set observacoes = :observacoes "+
		   " dtUpdate = :dtUpdate, idUserUpdate = :idUserUpdate where id = :id")
	void alterarClassificacao(@Param("observacoes") String observacoes, @Param("dtUpdate") Date dtUpdate, @Param("idUserUpdate") Long idUserUpdate,
			@Param("id") Long id);*/
}
