package br.edu.ipt.master.survey.neuralnet;

import static org.bytedeco.javacpp.opencv_imgproc.putText;
import static org.bytedeco.javacpp.opencv_imgproc.rectangle;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bytedeco.javacpp.opencv_core;
import org.bytedeco.javacpp.opencv_core.Mat;
import org.datavec.image.loader.NativeImageLoader;
import org.deeplearning4j.nn.graph.ComputationGraph;
import org.deeplearning4j.nn.layers.objdetect.DetectedObject;
import org.deeplearning4j.nn.layers.objdetect.Yolo2OutputLayer;
import org.deeplearning4j.util.ModelSerializer;
import org.nd4j.linalg.api.ndarray.INDArray;
import org.nd4j.linalg.dataset.api.preprocessor.ImagePreProcessingScaler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import br.com.agilizeware.infra.util.AppPropertiesService;
import br.com.agilizeware.infra.util.Util;
import br.edu.ipt.master.survey.enums.CoberturasEnum;
import br.edu.ipt.master.survey.model.ClassificacaoTecido;
import br.edu.ipt.master.survey.util.ImageUtil;

@Component("netDetector")
public class NetDetector {
	
	public static final String NAME_ENTITY = "FERIDA_DETECTADA";
	private static ComputationGraph model;
	private static Map<String, List<CoberturasEnum>> mapCoberturas;
	@Autowired
	private AppPropertiesService appProperties;
	private ImageUtil iu;
	
	public NetDetector() {
		super();
		iu = new ImageUtil();
	}
	
	public int getYoloWidth() {
		return appProperties.getPropertyLong("yolo.width").intValue();
	}
	
	public int getYoloHeight() {
		return appProperties.getPropertyLong("yolo.height").intValue();
	}
	
	public int getYoloGridWidth() {
		return appProperties.getPropertyLong("yolo.grid.width").intValue();
	}

	public int getYoloGridHeight() {
		return appProperties.getPropertyLong("yolo.grid.height").intValue();
	}
	
	public int getYoloChannels() {
		return appProperties.getPropertyLong("yolo.channels").intValue();
	}
	
	public String[] getYoloClasses() {
		return appProperties.getPropertyString("yolo.classes").split(",");
	}
	
	public String getFolderTempDiagnostic() {
		return appProperties.getPropertyString("agilize.file.physical.path") + 
				java.io.File.separator + NAME_ENTITY;
	}

	public Double getYoloDetectionThreshold() {
		return Double.valueOf(appProperties.getPropertyString("yolo.detection.threshold"));
	}
	
	public List<CoberturasEnum> getCoberturasPorClassificacao(String label) {
		if(!Util.isMapNotNull(mapCoberturas)) {
			preencherMapCoberturas();
		}
		return mapCoberturas.get(label);
	}
	
	private void preencherMapCoberturas() {
		mapCoberturas = new HashMap<String, List<CoberturasEnum>>(1);
		
		mapCoberturas.put("Viavel_Epitelizacao", new ArrayList<CoberturasEnum>(1));
		mapCoberturas.get("Viavel_Epitelizacao").add(CoberturasEnum.ADAPTIC);
		mapCoberturas.get("Viavel_Epitelizacao").add(CoberturasEnum.AGE);
		mapCoberturas.get("Viavel_Epitelizacao").add(CoberturasEnum.MEPTEL);
		mapCoberturas.get("Viavel_Epitelizacao").add(CoberturasEnum.HIDROCOLOIDE);
		mapCoberturas.get("Viavel_Epitelizacao").add(CoberturasEnum.MEPLEX_BORDER);
		
		mapCoberturas.put("Viavel_Granulacao_SemExsudato", new ArrayList<CoberturasEnum>(1));
		mapCoberturas.get("Viavel_Granulacao_SemExsudato").add(CoberturasEnum.HIDROGEL);
		mapCoberturas.get("Viavel_Granulacao_SemExsudato").add(CoberturasEnum.PAPAINA_GEL_2PERC);
		mapCoberturas.get("Viavel_Granulacao_SemExsudato").add(CoberturasEnum.ADAPTIC);
		mapCoberturas.get("Viavel_Granulacao_SemExsudato").add(CoberturasEnum.AGE);
		mapCoberturas.get("Viavel_Granulacao_SemExsudato").add(CoberturasEnum.MEPTEL);
		mapCoberturas.get("Viavel_Granulacao_SemExsudato").add(CoberturasEnum.HIDROCOLOIDE);
		
		mapCoberturas.put("Viavel_Granulacao_Seroso", new ArrayList<CoberturasEnum>(1));
		mapCoberturas.get("Viavel_Granulacao_Seroso").add(CoberturasEnum.ALGINATO_CALCIO);
		mapCoberturas.get("Viavel_Granulacao_Seroso").add(CoberturasEnum.FIBRACOL);
		mapCoberturas.get("Viavel_Granulacao_Seroso").add(CoberturasEnum.ADAPTIC);
		mapCoberturas.get("Viavel_Granulacao_Seroso").add(CoberturasEnum.MEPLEX_BORDER);
		mapCoberturas.get("Viavel_Granulacao_Seroso").add(CoberturasEnum.MEPTEL);

		mapCoberturas.put("Viavel_Granulacao_Hematico", new ArrayList<CoberturasEnum>(1));
		mapCoberturas.get("Viavel_Granulacao_Hematico").add(CoberturasEnum.ALGINATO_CALCIO);

		mapCoberturas.put("Viavel_Granulacao_Demais", new ArrayList<CoberturasEnum>(1));
		mapCoberturas.get("Viavel_Granulacao_Demais").add(CoberturasEnum.ALGINATO_CALCIO_PRATA);
		mapCoberturas.get("Viavel_Granulacao_Demais").add(CoberturasEnum.MEPILEX_AG);
		mapCoberturas.get("Viavel_Granulacao_Demais").add(CoberturasEnum.POLYMEN);
		
		mapCoberturas.put("Inviavel_Esfacelos_Seroso", new ArrayList<CoberturasEnum>(1));
		mapCoberturas.get("Inviavel_Esfacelos_Seroso").add(CoberturasEnum.HIDROGEL);
		mapCoberturas.get("Inviavel_Esfacelos_Seroso").add(CoberturasEnum.PAPAINA_GEL_4PERC);
		mapCoberturas.get("Inviavel_Esfacelos_Seroso").add(CoberturasEnum.COLAGENASE);

		List<CoberturasEnum> listCobs = new ArrayList<CoberturasEnum>(1);
		listCobs.add(CoberturasEnum.POLIHEXANIDA_SOLUC);
		listCobs.add(CoberturasEnum.POLYMEN);
		listCobs.add(CoberturasEnum.MEPILEX_AG);
		listCobs.add(CoberturasEnum.PAPAINA_GEL_4_6PERC);
		listCobs.add(CoberturasEnum.COLAGENASE);
		mapCoberturas.put("Inviavel_Esfacelos_Hematico", listCobs);
		mapCoberturas.put("Inviavel_Esfacelos_Demais", listCobs);

		mapCoberturas.put("Inviavel_NecroseUmida_Seroso", new ArrayList<CoberturasEnum>(1));
		mapCoberturas.get("Inviavel_NecroseUmida_Seroso").add(CoberturasEnum.HIDROGEL);
		mapCoberturas.get("Inviavel_NecroseUmida_Seroso").add(CoberturasEnum.PAPAINA_GEL_8_10PERC);
		mapCoberturas.get("Inviavel_NecroseUmida_Seroso").add(CoberturasEnum.POLIHEXANIDA_SOLUC);
		
		listCobs = new ArrayList<CoberturasEnum>(1);
		listCobs.add(CoberturasEnum.POLIHEXANIDA_SOLUC);
		listCobs.add(CoberturasEnum.PAPAINA_GEL_10PERC);
		mapCoberturas.put("Inviavel_NecroseUmida_Hematico", listCobs);
		mapCoberturas.put("Inviavel_NecroseUmida_Demais", listCobs);

		mapCoberturas.put("Inviavel_NecroseSeca", new ArrayList<CoberturasEnum>(1));
		mapCoberturas.get("Inviavel_NecroseSeca").add(CoberturasEnum.HIDROGEL);
		mapCoberturas.get("Inviavel_NecroseSeca").add(CoberturasEnum.POLIHEXANIDA_SOLUC);
		mapCoberturas.get("Inviavel_NecroseSeca").add(CoberturasEnum.PAPAINA_GEL_10PERC);
	}
	
	private ComputationGraph getYoloModel() {
		try {
			if(model == null) {
				//File f = new File("D:\\TRABALHO_MESTRADO\\VALIDACAO\\NEW\\model_yolo_38_128.zip"); //new File(appProperties.getPropertyString("file.model.diagnostic"));
				//File f = new File("D:\\TRABALHO_MESTRADO\\VALIDACAO\\NEW\\model_yoloV2_23_256.zip"); //new File(appProperties.getPropertyString("file.model.diagnostic"));
				//File f = new File(appProperties.getPropertyString("file.model.diagnostic"));
				File f = new File(System.getProperty("user.dir") + appProperties.getPropertyString("file.model.diagnostic"));
	    		model = ModelSerializer.restoreComputationGraph(f);
			}
		}
		catch(IOException ioe) {
			ioe.printStackTrace();
			throw new RuntimeException("Erro ao recuperar o modelo do Yolo para Detecção de Feridas na Imagem informada: "+ioe.getMessage());
		}
    	return model;
    }

	/*public InputStream redimensionarImagem(String fileImgFisico) {
		return iu.redimensionarImagem(fileImgFisico, getYoloWidth(), getYoloHeight());
	}*/
	
	public Mat redimensionarImagem(String fileImgFisico) {
		return iu.redimensionarImagem(fileImgFisico, getYoloWidth(), getYoloHeight());
	}
	
	public File saveDiagnosticTemp(Mat image) {
		return iu.saveDiagnosticTemp(image, getFolderTempDiagnostic());
	}
	
	public List<ClassificacaoTecido> detectWounds(Mat image) {
        Yolo2OutputLayer yout = (Yolo2OutputLayer) getYoloModel().getOutputLayer(0);
        NativeImageLoader loader = new NativeImageLoader(getYoloWidth(), getYoloHeight(), getYoloChannels());//, new ColorConversionTransform(COLOR_BGR2RGB)
        INDArray ds = null;
        try {
            ds = loader.asMatrix(image);
        } catch (IOException ioe) {
			ioe.printStackTrace();
			throw new RuntimeException("Erro ao transforma Imagem da Ferida informada em matriz: "+ioe.getMessage());
        }
        ImagePreProcessingScaler scaler = new ImagePreProcessingScaler(0, 1);
        scaler.transform(ds);
        INDArray results = getYoloModel().outputSingle(ds);
        List<DetectedObject> objs = yout.getPredictedObjects(results, getYoloDetectionThreshold());
        List<DetectedObject> objects = NonMaxSuppression.getObjects(objs);
        return drawBoxes(image, objects);//use objs to see the use of the NonMax Suppression algorithm
        //return iu.saveDiagnosticTemp(image, getFolderTempDiagnostic());
    }
	
	private opencv_core.Scalar getColor(int aux) {
		opencv_core.Scalar retorno = opencv_core.Scalar.RED;
		if(aux % 7 == 0) {
			retorno = opencv_core.Scalar.GRAY;
		}
		else if(aux % 6 == 0) {
			retorno = opencv_core.Scalar.CYAN;
		}
		else if(aux % 5 == 0) {
			retorno = opencv_core.Scalar.BLACK;
		}
		else if(aux % 4 == 0) {
			retorno = opencv_core.Scalar.GREEN;
		}
		else if(aux % 3 == 0) {
			retorno = opencv_core.Scalar.BLUE;
		}
		else if(aux % 2 == 0) {
			retorno = opencv_core.Scalar.YELLOW;
		}
		return retorno;
	}
	
	private List<ClassificacaoTecido> drawBoxes(Mat image, List<DetectedObject> objects) {
		List<ClassificacaoTecido> labels = new ArrayList<ClassificacaoTecido>(1); 
		ClassificacaoTecido tec;
		int nuFerida = 1;
        for (DetectedObject obj : objects) {
            double[] xy1 = obj.getTopLeftXY();
            double[] xy2 = obj.getBottomRightXY();
            int predictedClass = obj.getPredictedClass();
            
            tec = new ClassificacaoTecido();
            tec.setLabel(getYoloClasses()[predictedClass]);
            
            int x1 = (int) Math.round(getYoloWidth() * xy1[0] / getYoloGridWidth());
            int y1 = (int) Math.round(getYoloHeight() * xy1[1] / getYoloGridHeight());
            int x2 = (int) Math.round(getYoloWidth() * xy2[0] /getYoloGridWidth());
            int y2 = (int) Math.round(getYoloHeight() * xy2[1] / getYoloGridHeight());
            rectangle(image, new opencv_core.Point(x1, y1), new opencv_core.Point(x2, y2), getColor(nuFerida));
            putText(image, tec.getLabel(), new opencv_core.Point(x1 + 2, y2 - 2), 1, .8, getColor(nuFerida));
            
            tec.setCoordenadas(x1+";"+y1+"||||"+x2+";"+y2);
            tec.setOrdem(nuFerida);
            tec.setConfidencia(BigDecimal.valueOf(obj.getConfidence()));
            tec.setxMin(x1);
            tec.setyMin(y1);
            tec.setxMax(x2);
            tec.setyMax(y2);
            
            System.out.println("Predicted class | label = " + tec.getLabel() + " | coordenadas = "+tec.getCoordenadas());
            
            labels.add(tec);
            nuFerida++;
        }
        return labels;
    }
}
