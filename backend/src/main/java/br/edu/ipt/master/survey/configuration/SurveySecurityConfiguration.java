package br.edu.ipt.master.survey.configuration;

import org.springframework.boot.autoconfigure.security.SecurityProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

import br.com.agilizeware.infra.security.component.configuration.SecurityConfiguration;

@Configuration
//@Order(SecurityProperties.BASIC_AUTH_ORDER)
@Order(SecurityProperties.ACCESS_OVERRIDE_ORDER)
public class SurveySecurityConfiguration extends SecurityConfiguration {
	
	protected String[] getPathsPermit() {
		String[] str = {"/survey/login/authenticate", "/survey/facade/enums", "/survey/login/validateToken"};
		return str;
	}
	
	protected String[] getFoldersPermit() {
		String[] str = {"/error", "/failure", "/favicon.ico", "/auth/**", "/survey/fonts/**",
   		                "/signup/**", "/signin/**", "/api/session", "/js/**", "/views/**"};
		return str;
	}
}