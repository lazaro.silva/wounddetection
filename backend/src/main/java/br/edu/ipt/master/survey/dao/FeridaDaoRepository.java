package br.edu.ipt.master.survey.dao;

import br.com.agilizeware.infra.dao.CrudAgilizeRepositoryIF;
import br.edu.ipt.master.survey.model.Ferida;

public interface FeridaDaoRepository extends CrudAgilizeRepositoryIF<Ferida, Long> {

}
