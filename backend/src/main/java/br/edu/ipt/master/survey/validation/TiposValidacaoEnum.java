package br.edu.ipt.master.survey.validation;

import br.com.agilizeware.infra.enums.IEnum;

public enum TiposValidacaoEnum implements IEnum<Integer> {
	
	 NOTHING (0, "NOTHING"),
	 CPF (1, "CPF"),
	 CNPJ (2, "CNPJ"),
	 EMAIL (3, "EMAIL"),
	;	
	
	private Integer codigo;
	private String descricao;
	
	private TiposValidacaoEnum(Integer codigo, String descricao) {
		this.codigo = codigo;
		this.descricao = descricao;
	}
	
	public Integer getId() {
		return codigo;
	}

	public String getLabel() {
		return null;
	}

	public String getDescription() {
		return descricao;
	}


	public static TiposValidacaoEnum findByLabel(String label) {
		for (TiposValidacaoEnum userType : TiposValidacaoEnum.values()) {
			if (userType.getLabel().equals(label)) {
				return userType;
			}
		}
		throw new IllegalArgumentException("Invalid label.");
	}
	
	public static TiposValidacaoEnum findByCode(Integer code) {
		TiposValidacaoEnum[] array = TiposValidacaoEnum.values();
		for(TiposValidacaoEnum t : array) {
			if(t.getId().equals(code)) {
				return t;
			}
		}
		return null;
	}
	
	public String toString() {
		return "{\"codigo\": "+codigo+", \"descricao\": \""+descricao+"}";
	}

}