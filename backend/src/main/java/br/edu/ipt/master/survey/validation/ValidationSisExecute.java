package br.edu.ipt.master.survey.validation;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.http.HttpStatus;

import br.com.agilizeware.infra.dto.DtoAb;
import br.com.agilizeware.infra.enums.ErrorCodeEnum;
import br.com.agilizeware.infra.enums.IEnum;
import br.com.agilizeware.infra.exception.AgilizeException;
import br.com.agilizeware.infra.model.EntityIf;
import br.com.agilizeware.infra.util.Util;

public class ValidationSisExecute {
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static void executeFieldsValidation(Serializable record, Class mainClass) {
		
		List<AgilizeException> arrays = new ArrayList<AgilizeException>(1);
		List<String> fields = new ArrayList<String>(1);
		ValidationSisAop mainFields = (ValidationSisAop)mainClass.getAnnotation(ValidationSisAop.class);
		if(record == null) {
			arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.REQUIRED_FIELD, mainFields.label()));
			fields.addAll(Arrays.asList(mainFields.mainLabels()));
		}
		else {
			arrays.addAll(executeValidation(record, mainFields.label(), mainClass, fields));	
		}
		
		if(Util.isListNotNull(arrays)) {
			AgilizeException erro = new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.REQUIREDS_FIELD, arrays);
			erro.setFieldsFormMarked(fields);
			throw erro;
		}
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<AgilizeException> executeValidation(Serializable record, String mainLabel, Class mainClass, List<String> fields) {
		
		List<AgilizeException> arrays = new ArrayList<AgilizeException>(1);
		ValidationSisAop mainFields = (ValidationSisAop)mainClass.getAnnotation(ValidationSisAop.class);
		if(record == null) {
			arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.REQUIRED_FIELD, mainFields.label()));
			fields.addAll(Arrays.asList(mainFields.mainLabels()));
		}
		else {
				
			for(Field f : record.getClass().getDeclaredFields()) {
				executeInternalValidations(f, record, arrays, fields);
			}
			
			//Validando campos da classe mãe
			Class c = record.getClass().getSuperclass();
			if(Util.isNotNull(c) && c.getName().equals("br.com.agilizeware.infra.dto.PojoAb")) {
				for(Field f : c.getDeclaredFields()) {
					executeInternalValidations(f, record, arrays, fields);
				}
			}				
		}
		
		return arrays;
	}

	private static void addFields(List<String> fields, ValidationSisAop val) {
		if(Util.isNotNull(val.mainLabels()) && val.mainLabels().length > 0) {
			fields.addAll(Arrays.asList(val.mainLabels()));
		}
		else {
			fields.add(val.label());
		}
	}
	
	@SuppressWarnings("rawtypes")
	private static void executeInternalValidations(Field f, Serializable record, List<AgilizeException> arrays, List<String> fields) {
		
		ValidationSisAop val = f.getAnnotation(ValidationSisAop.class);
		
		try {
			if(Util.isNotNull(val)) {
				String nameMethod = "get" + f.getName().substring(0, 1).toUpperCase() + f.getName().substring(1);
				Method getField = record.getClass().getMethod(nameMethod);
				Object valueField = getField.invoke(record);
				
				//Nulidade
				if(val.required() && (!Util.isNotNull(valueField) || (valueField instanceof List && !Util.isListNotNull((List)valueField)))) {
					arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.REQUIRED_FIELD, val.label()));
					addFields(fields, val);
				}
				
				if(Util.isNotNull(valueField)) {
					//Tamanho do Campo
					if(!(valueField instanceof Date || valueField instanceof Calendar || valueField instanceof IEnum ||
							valueField instanceof DtoAb || valueField instanceof EntityIf ||
							valueField instanceof List) && 
							( ((!val.onlyAlphaNumeric() && valueField.toString().length() > val.sizeField()) ||
									(val.onlyAlphaNumeric() && Util.onlyAlphaNumeric(valueField.toString()).length() > val.sizeField()))  ||
							  ((!val.onlyAlphaNumeric() && val.exactField() > 0 && valueField.toString().length() != val.exactField()) ||
											(val.onlyAlphaNumeric() && val.exactField() > 0 && Util.onlyAlphaNumeric(valueField.toString()).length() != val.exactField())))) {
						arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.INVALID_LENGTH_FIELD, val.label(), 
								String.valueOf((val.sizeField()))));
						addFields(fields, val);
					}
					
					//Validando Somente Alfanumerico
					if(val.onlyAlphaNumeric() && ((val.required() && !Util.isNotNull(Util.onlyAlphaNumeric(valueField.toString()))) || 
							(val.exactField() > 0 && Util.onlyAlphaNumeric(valueField.toString()).length() != val.exactField()))) {
						arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.INVALID_FIELD, val.label()));
						addFields(fields, val);
					}
					
					//Validacoes Sobre Tipos
					if(!TiposValidacaoEnum.NOTHING.getId().equals(val.tipoValidacao())) {
						if(TiposValidacaoEnum.CPF.getId().equals(val.tipoValidacao())) {
							if(!Util.isValidCPF(Util.onlyNumbers(valueField.toString()))) {
								arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.INVALID_FIELD, val.label()));
								addFields(fields, val);
							}
						}
						if(TiposValidacaoEnum.CNPJ.getId().equals(val.tipoValidacao())) {
							if(!Util.isValidCNPJ(Util.onlyNumbers(valueField.toString()))) {
								arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.INVALID_FIELD, val.label()));
								addFields(fields, val);
							}
						}
						if(TiposValidacaoEnum.EMAIL.getId().equals(val.tipoValidacao())) {
							if(!Util.isValidEmail(valueField.toString())) {
								arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.INVALID_FIELD, val.label()));
								addFields(fields, val);
							}
						}					
					}

					//Validações sobre Data
					if(valueField instanceof Date) {
						Date dt = (Date)valueField;
						Date dtAtual = Util.obterDataHoraAtual();
						//Não ser Maior que Data Atual
						if(val.notAfterNow()) {
							if(dt.after(dtAtual)) {
								arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.ERROR_DT_AFTER_TODAY, val.label()));
								addFields(fields, val);
							}
						}
						if(val.notBeforeNow()) {
							if(dt.before(dtAtual)) {
								arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.ERROR_DT_BEFORE_TODAY, val.label()));
								addFields(fields, val);
							}
						}
						if(Util.isNotNull(val.notGreatter()) && val.notGreatter().length > 0) {
							Method getDtField = record.getClass().getMethod(val.notGreatter()[0]);
							Object valueDtField = getDtField.invoke(record);
							if(Util.isNotNull(valueDtField) && ((Date)valueDtField).before(dt)) {
								arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.ERROR_DTINI_AFTER_DTTO));
								addFields(fields, val);
							}
						}
					}
					
					if(!(valueField instanceof Date) && Util.isNotNull(val.notGreatter()) && val.notGreatter().length > 0) {
						Method getNbField = record.getClass().getMethod(val.notGreatter()[0]);
						Object valueNbField = getNbField.invoke(record);
						if(Util.isNotNull(valueNbField) && ((new BigDecimal(valueNbField.toString()).compareTo(new BigDecimal(valueField.toString())) < 0))) {
							arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.ERROR_INI_GREATTER_TO, val.label(), val.notGreatter()[1]));
							addFields(fields, val);
						}
					}
				}
				
				//Validando a dependencia condicional
				if(!Util.isNotNull(valueField) && Util.isNotNull(val.requiredDepends()) && val.requiredDepends().length > 0) {
					for(String strMethod : val.requiredDepends()) {
						Method getDependField = record.getClass().getMethod(strMethod);
						Object valueDependField = getDependField.invoke(record);
						if(Util.isNotNull(valueDependField)) {
							arrays.add(new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.REQUIRED_FIELD, val.label()));
							addFields(fields, val);
							break;
						}
					}
					
				}
				
				//Validando a coleação
				if(valueField instanceof List) {
					for(Object obj : (List)valueField) {
						if(obj instanceof EntityIf) {
							arrays.addAll(executeValidation((EntityIf)obj, val.label(), obj.getClass(), fields));
						}
						else if(obj instanceof DtoAb) {
							arrays.addAll(executeValidation((DtoAb)obj, val.label(), obj.getClass(), fields));
						}
					}
				}
				
				//Validando os campos dentro dos Objetos
				if(valueField instanceof EntityIf) {
					arrays.addAll(executeValidation((EntityIf)valueField, val.label(), valueField.getClass(), fields));
				}
				else if(valueField instanceof DtoAb) {
					arrays.addAll(executeValidation((DtoAb)valueField, val.label(), valueField.getClass(), fields));
				}
			}
		}
		catch(InvocationTargetException | NoSuchMethodException | IllegalAccessException e) {
			throw new AgilizeException(HttpStatus.BAD_REQUEST.ordinal(), ErrorCodeEnum.ERROR_REFLECTION, e);
		}
		
	}
	
}
