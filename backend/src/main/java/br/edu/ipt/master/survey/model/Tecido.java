package br.edu.ipt.master.survey.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.agilizeware.infra.model.EntityIf;
import br.com.agilizeware.infra.util.Util;

@javax.persistence.Entity
@Table(name="tecidos")
public class Tecido implements EntityIf {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3270952572277104493L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_creation", nullable = false)
	private Date dtCreate;
	@Column(name="fk_user_creation")
	private Long idUserCreate;

	@Column(name="nome", nullable = false)
	private String nome;
	@Column(name="label", nullable = true)
	private String label;
	@OneToMany(mappedBy="pai")
	private List<Tecido> filhos;
	@JsonIgnore
	@ManyToOne
	@JoinColumn(nullable=true, name="fk_tecido_superior")
	private Tecido pai;
	
	public Tecido() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtCreate() {
		return dtCreate;
	}

	public void setDtCreate(Date dtCreate) {
		this.dtCreate = dtCreate;
	}

	public Long getIdUserCreate() {
		return idUserCreate;
	}

	public void setIdUserCreate(Long idUserCreate) {
		this.idUserCreate = idUserCreate;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}
	
	public List<Tecido> getFilhos() {
		return filhos;
	}

	public void setFilhos(List<Tecido> filhos) {
		this.filhos = filhos;
	}

	public Tecido getPai() {
		return pai;
	}

	public void setPai(Tecido pai) {
		this.pai = pai;
	}

	public String getLabel() {
		if(Util.isNotNull(label)) {
			return label;
		}
		if(Util.isListNotNull(filhos)) {
			for(Tecido t : filhos) {
				if(Util.isNotNull(t.getLabel())) {
					return t.getLabel();
				}
			}
		}
		return null;
	}

	public void setLabel(String label) {
		this.label = label;
	}

}
