package br.edu.ipt.master.survey.model;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.agilizeware.infra.model.EntityIf;
import br.edu.ipt.master.survey.validation.ValidationSisAop;

@javax.persistence.Entity
@Table(name="classificacao_tecido")
//@ValidationSisAop(mainLabels={"lbl.nome", "lbl.cpf.cnpj", "lbl.telefone", "lbl.email"}, label="lbl.form")
public class ClassificacaoTecido implements EntityIf {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2604777009380813463L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_creation", nullable = false)
	private Date dtCreate;
	@Column(name="fk_user_creation")
	private Long idUserCreate;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(nullable=false, name="fk_classificacao")
	@ValidationSisAop(required=true, label="", mainLabels={"lbl.id"})
	private Classificacao classificacao;
	@ManyToOne
	@JoinColumn(nullable=false, name="fk_tecido")
	@ValidationSisAop(required=true, label="", mainLabels={"lbl.id"})
	private Tecido tecido;
	@Column(nullable=false, name="nu_ferida")
	@ValidationSisAop(required=true, label="lbl.num.ferida", sizeField=2)
	private Integer ordem;
	@Column(nullable=false, name="nu_confidence")
	@ValidationSisAop(required=true, label="lbl.num.confidence", sizeField=19)
	private BigDecimal confidencia;
	@Column(nullable=false, name="coordenadas")
	@ValidationSisAop(required=true, label="lbl.coordenadas", sizeField=200)
	private String coordenadas;
	@OneToMany(mappedBy="classificacaoTecido")
	private List<ClassificacaoCobertura> coberturasClassificadas;
	
	@Transient
	private String label;
	@Transient
	private Integer xMin;
	@Transient
	private Integer yMin;
	@Transient
	private Integer xMax;
	@Transient
	private Integer yMax;
	
	public ClassificacaoTecido() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtCreate() {
		return dtCreate;
	}

	public void setDtCreate(Date dtCreate) {
		this.dtCreate = dtCreate;
	}

	public Long getIdUserCreate() {
		return idUserCreate;
	}

	public void setIdUserCreate(Long idUserCreate) {
		this.idUserCreate = idUserCreate;
	}
	
	public Classificacao getClassificacao() {
		return classificacao;
	}

	public void setClassificacao(Classificacao classificacao) {
		this.classificacao = classificacao;
	}
	
	public Tecido getTecido() {
		return tecido;
	}

	public void setTecido(Tecido tecido) {
		this.tecido = tecido;
	}

	public Integer getOrdem() {
		return ordem;
	}

	public void setOrdem(Integer ordem) {
		this.ordem = ordem;
	}

	public String getCoordenadas() {
		return coordenadas;
	}

	public void setCoordenadas(String coordenadas) {
		this.coordenadas = coordenadas;
	}
	
	public List<ClassificacaoCobertura> getCoberturasClassificadas() {
		return coberturasClassificadas;
	}

	public void setCoberturasClassificadas(List<ClassificacaoCobertura> coberturasClassificadas) {
		this.coberturasClassificadas = coberturasClassificadas;
	}
	
	public BigDecimal getConfidencia() {
		return confidencia;
	}

	public void setConfidencia(BigDecimal confidencia) {
		this.confidencia = confidencia;
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return this.label;
	}
	
	public void setLabel(String label) {
		this.label = label;
	}

	public Integer getxMin() {
		return xMin;
	}

	public void setxMin(Integer xMin) {
		this.xMin = xMin;
	}

	public Integer getyMin() {
		return yMin;
	}

	public void setyMin(Integer yMin) {
		this.yMin = yMin;
	}

	public Integer getxMax() {
		return xMax;
	}

	public void setxMax(Integer xMax) {
		this.xMax = xMax;
	}

	public Integer getyMax() {
		return yMax;
	}

	public void setyMax(Integer yMax) {
		this.yMax = yMax;
	}
	
}
