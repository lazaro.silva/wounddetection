package br.edu.ipt.master.survey.configuration;

import java.util.Properties;

import org.apache.velocity.app.VelocityEngine;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.FilterType;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.handler.MappedInterceptor;

import br.com.agilizeware.infra.aop.AuditExecution;
import br.com.agilizeware.infra.aop.TransactionMongoDbExecution;
import br.com.agilizeware.infra.dao.CrudDaoImpl;
import br.com.agilizeware.infra.file.component.FileSystemStorage;
import br.com.agilizeware.infra.file.repository.FileDaoImpl;
import br.com.agilizeware.infra.file.repository.FileDaoRepository;
import br.com.agilizeware.infra.file.rest.AgilizeFileServiceRest;
import br.com.agilizeware.infra.queue.component.SchedulledConfig;
import br.com.agilizeware.infra.queue.repository.QueueDaoImpl;
import br.com.agilizeware.infra.queue.repository.QueueDaoRepository;
import br.com.agilizeware.infra.queue.rest.QueueServiceRest;
import br.com.agilizeware.infra.security.component.authentication.CustomAuthenticationProvider;
import br.com.agilizeware.infra.security.component.configuration.CorsConfiguration;
import br.com.agilizeware.infra.security.component.filter.AuthenticationFilter;
import br.com.agilizeware.infra.security.repository.AddressDaoImpl;
import br.com.agilizeware.infra.security.repository.ApplicationDaoRepository;
import br.com.agilizeware.infra.security.repository.FunctionDaoRepository;
import br.com.agilizeware.infra.security.repository.SecurityDaoImpl;
import br.com.agilizeware.infra.security.repository.UserDaoImpl;
import br.com.agilizeware.infra.security.repository.UtilQueryDaoImpl;
import br.com.agilizeware.infra.security.rest.AddressServiceRest;
import br.com.agilizeware.infra.security.rest.UserServiceRest;
import br.com.agilizeware.infra.security.rest.UtilServiceRest;
import br.com.agilizeware.infra.service.AuditServiceImpl;
import br.com.agilizeware.infra.util.AppPropertiesService;

@Configuration
@ComponentScan(basePackages = {"br.edu.ipt.master.survey.dao.*",
							   "br.com.agilizeware.infra.security.repository.*", /*"br.com.agilizeware.infra.aop.*",*/ 
							   "br.com.agilizeware.infra.service.*", "br.com.agilizeware.infra.dao.*", 
							   "br.com.agilizeware.infra.security.component.authentication.*", "br.com.agilizeware.infra.util.*",
							   "br.com.agilizeware.infra.queue.rest.*", "br.com.agilizeware.infra.queue.repository.*",
							   "br.com.agilizeware.infra.file.rest.*", "br.com.agilizeware.infra.security.rest.*",
							   "br.com.agilizeware.infra.file.repository.*", "br.com.agilizeware.infra.file.component.*",
							   "br.com.agilizeware.infra.payment.rest.*", "br.com.agilizeware.infra.payment.repository.*", 
							   "br.com.agilizeware.infra.queue.component.*"}, 
			basePackageClasses={SecurityDaoImpl.class, CustomAuthenticationProvider.class,
								UserDaoImpl.class,
					            QueueServiceRest.class, QueueDaoImpl.class, AgilizeFileServiceRest.class, UserServiceRest.class, FileDaoImpl.class,
					            FileSystemStorage.class, AppPropertiesService.class,
					            UtilQueryDaoImpl.class, UtilServiceRest.class, 
					            SchedulledConfig.class, Environment.class}, 
			excludeFilters={@ComponentScan.Filter(type=FilterType.ASSIGNABLE_TYPE, value={CrudDaoImpl.class, AddressDaoImpl.class, AddressServiceRest.class, 
										AuditExecution.class, AuditServiceImpl.class, TransactionMongoDbExecution.class}),
							@ComponentScan.Filter(type = FilterType.REGEX, pattern = "br\\.com\\.agilizeware\\.infra\\.aop\\..*")})
@EnableJpaRepositories(basePackages = { "br.edu.ipt.master.survey.dao",
										"br.com.agilizeware.infra.security.repository", "br.com.agilizeware.infra.dao", "br.com.agilizeware.infra.queue.repository",
									    "br.com.agilizeware.infra.file.repository", "br.com.agilizeware.infra.payment.repository"}, 
					   basePackageClasses={ApplicationDaoRepository.class, FunctionDaoRepository.class, QueueDaoRepository.class, FileDaoRepository.class})
@EnableTransactionManagement
@EnableAspectJAutoProxy
public class SurveyCorsConfiguration extends CorsConfiguration {

	//private static final Logger log = LogManager.getLogger(AsbapiCorsConfiguration.class);
	
	@Autowired
	private AppPropertiesService appPropertiesService;
	@Autowired
	private SurveyAuthorizationInterceptor authorizationInterceptor;
	
	@Bean
	public MappedInterceptor getInterceptor() {
		return new MappedInterceptor(new String[] { "/survey/**" }, authorizationInterceptor);
	}
	
	/*@Bean("multipartResolver")
    public MultipartResolver multipartResolver() {
        return new StandardServletMultipartResolver();
    }*/
	
	@Bean("multipartResolver")
    public MultipartResolver multipartResolver() {
		CommonsMultipartResolver c = new CommonsMultipartResolver();
		c.setMaxUploadSize(1000000);
		return c;
    }
	
	@Bean(name = "authenticationFilter")
	public AuthenticationFilter getAuthenticationFilter() {
		return new SurveyAuthenticationFilter();
	}
	
	protected String[] getUrlEnableds() {
		return appPropertiesService.getPropertyString("url.enabled").split(",");
	}
	
	protected String getDriverClassName() {
		return appPropertiesService.getPropertyString("spring.datasource.driverClassName");
	}
	
	protected String getUrlDataSource() {
		return appPropertiesService.getPropertyString("spring.datasource.url");
	}
	
	protected String getUserDataSource() {
		return appPropertiesService.getPropertyString("spring.datasource.username");
	}
	
	protected String getPasswDataSource() {
		return appPropertiesService.getPropertyString("spring.datasource.password");
	}
	
	protected String[] getEntityPackagesToScan() {
		return appPropertiesService.getPropertyString("entitymanager.packagesToScan").split(";");
	}
	
	@Bean
    public VelocityEngine velocityEngine() throws Exception {
        Properties properties = new Properties();
        properties.setProperty("input.encoding", appPropertiesService.getPropertyString("spring.mail.default-encoding"));
        properties.setProperty("output.encoding", appPropertiesService.getPropertyString("spring.mail.default-encoding"));
        properties.setProperty("resource.loader", "class");
        properties.setProperty("class.resource.loader.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
        VelocityEngine velocityEngine = new VelocityEngine(properties);
        return velocityEngine;
    }
}