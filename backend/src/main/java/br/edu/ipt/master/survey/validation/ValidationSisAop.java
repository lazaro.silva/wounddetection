package br.edu.ipt.master.survey.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


@Retention(RetentionPolicy.RUNTIME)
@Target(value={ElementType.TYPE, ElementType.FIELD})
public @interface ValidationSisAop {
	
	boolean required() default false;
	int sizeField() default 12;
	int exactField() default 0;
	int tipoValidacao() default 0;
	String label();
	boolean onlyAlphaNumeric() default false;
	boolean notAfterNow() default false;
	boolean notBeforeNow() default false;
	String[] notGreatter() default {};
	String[] requiredDepends() default {};
	String[] mainLabels() default {};	
}
