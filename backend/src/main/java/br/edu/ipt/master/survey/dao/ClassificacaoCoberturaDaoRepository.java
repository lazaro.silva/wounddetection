package br.edu.ipt.master.survey.dao;

import java.util.Date;

import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.agilizeware.infra.dao.CrudAgilizeRepositoryIF;
import br.edu.ipt.master.survey.model.ClassificacaoCobertura;

public interface ClassificacaoCoberturaDaoRepository extends CrudAgilizeRepositoryIF<ClassificacaoCobertura, Long> {
	
	/*@Modifying
	@Query("Update ClassificacaoCobertura SET dtUpdate=:dtUpdate, idUserUpdate=:idUserUpdate, isAceito=:isAceito WHERE id=:id")
    void alterarClassificacaoCobertura(@Param("dtUpdate") Date dtUpdate, @Param("idUserUpdate") Long idUserUpdate, @Param("isAceito") Boolean isAceito,
    		@Param("id") Long id);*/

}
