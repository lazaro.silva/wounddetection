package br.edu.ipt.master.survey.util;

//import org.opencv.imgcodecs.Imgcodecs;
import static org.bytedeco.javacpp.opencv_imgcodecs.imread;
import static org.bytedeco.javacpp.opencv_imgcodecs.imwrite;
import static org.bytedeco.javacpp.opencv_imgproc.resize;

import java.io.File;
import java.util.List;
import java.util.Random;

import org.apache.commons.io.FileUtils;
//import org.opencv.core.Mat;
import org.bytedeco.javacpp.opencv_core.Mat;
//import org.opencv.core.Size;
import org.bytedeco.javacpp.opencv_core.Size;
import org.opencv.core.Core;
import org.opencv.imgproc.Imgproc;

import br.com.agilizeware.infra.util.Util;
import br.edu.ipt.master.survey.model.ClassificacaoTecido;
import br.edu.ipt.master.survey.neuralnet.NetDetector;

public class ImageUtil {
	
	public ImageUtil() {
		super();
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
	}
	
	public File saveDiagnosticTemp(Mat imgDetected, String dir) {
		java.io.File fDir = new java.io.File(dir);
		if(!fDir.exists()) {
			fDir.mkdirs();
		}
		Random r = new Random(Util.obterDataHoraAtual().getTime());
		String file = dir + java.io.File.separator + "FERIDA_" + r.nextLong() + ".jpg";
		//if(Imgcodecs.imwrite(file, imgDetected)) {
		if(imwrite(file, imgDetected)) {
			return new File(file);
		}
		throw new RuntimeException("Não foi possível salvar a figura da imagem detectada | "+file);
	}
	
	public File saveDiagnosticTemp(Mat imgDetected, String dir, String nameFile) {
		java.io.File fDir = new java.io.File(dir);
		if(!fDir.exists()) {
			fDir.mkdirs();
		}
		String file = dir + java.io.File.separator + nameFile;
		if(imwrite(file, imgDetected)) {
			return new File(file);
		}
		throw new RuntimeException("Não foi possível salvar a figura da imagem detectada = "+file);
	}
	
	
	/*private org.opencv.core.Mat returnMat(@SuppressWarnings("deprecation") CvMat mtx) {
        if (mtx == null) {
            System.out.println("Error to load");
            return null;
        }

        double valor;       

        System.out.println(mtx.rows() + "x" + mtx.cols());
        org.opencv.core.Mat dest = new org.opencv.core.Mat(mtx.rows(), mtx.cols(), CvType.CV_32FC1);

        final int rows = mtx.rows();
        final int cols = mtx.cols();
        final int step = mtx.step()/4;
        FloatBuffer buf = mtx.getFloatBuffer();
        float s = 0.0f;
        for (int row = 0; row < rows; row++) {
            buf.position(row * step);
            for (int col = 0; col< cols; col++) {
                //System.out.println(buf.get());
                valor = buf.get();
                dest.put(row, col, valor);
            }
        }

        return dest;
    }*/
	
	/*public InputStream redimensionarImagem(String fileImgFisico, int width, int height) {
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat im = imread(fileImgFisico);
		Mat imRedimensionado = new Mat();
		resize(im, imRedimensionado, new Size(width, height), 0, 0, Imgproc.INTER_LANCZOS4);
		int tamFile = fileImgFisico.length();
		org.opencv.core.Mat matImRedimensionado = new org.opencv.core.Mat(imRedimensionado.address());
		MatOfByte mob = new MatOfByte();
	    Imgcodecs.imencode(fileImgFisico.substring(tamFile-4), matImRedimensionado, mob);
	    byte ba[] = mob.toArray();
	    InputStream retorno = new ByteArrayInputStream(ba);
	    return retorno;
	}*/
	
	//Open Declaration org.bytedeco.javacpp.opencv_core.Mat
	public Mat redimensionarImagem(String fileImgFisico, int width, int height) {
		//System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		Mat im = imread(fileImgFisico);
		Mat imRedimensionado = new Mat();
		resize(im, imRedimensionado, new Size(width, height), 0, 0, Imgproc.INTER_LANCZOS4);
	    return imRedimensionado;
	}

	/* public static void main(String[] args) throws Exception {

		System.out.println("**** INICIO ****");
		
		System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
		//System.loadLibrary("opencv_java342");
		
		NetDetector netDetector = new NetDetector();
		ImageUtil iu = new ImageUtil();
		Mat imRedimensionada = null;
		List<ClassificacaoTecido> labelsDetectadas = null;
		//File dirImages = new File("D:\\TRABALHO_MESTRADO\\VALIDACAO\\NEW\\ORIGINAIS\\");
		File dirImages = new File("D:\\TRABALHO_MESTRADO\\VALIDACAO\\BBOX\\Images\\001\\");
		String dirDestinoImagens = "D:\\TRABALHO_MESTRADO\\VALIDACAO\\NEW\\416X416\\";
		String dirDestinoImagensDetectadas = "D:\\TRABALHO_MESTRADO\\VALIDACAO\\NEW_618_618_V2\\DETECTADAS\\JPEGImages\\";
		String dirDestinoDetections = "D:\\TRABALHO_MESTRADO\\VALIDACAO\\NEW_618_618_V2\\DETECTADAS\\detections\\";
		
		StringBuffer sb = null;
		File[] filesImages = dirImages.listFiles();
		int aux = 1;
		String nameFile = null;
		for(File image : filesImages) {
			imRedimensionada = netDetector.redimensionarImagem(image.getAbsolutePath());
			
			/if(aux < 10) {
				nameFile = "00" + aux;
			}
			else if(aux < 100) {
				nameFile = "0" + aux;
			}
			else {
				nameFile = String.valueOf(aux);
			}
			
			iu.saveDiagnosticTemp(imRedimensionada, dirDestinoImagens, nameFile+".JPG");/
			nameFile = image.getName().toLowerCase().replace(".jpg", "");
			
			labelsDetectadas = netDetector.detectWounds(imRedimensionada);
			sb = new StringBuffer("");
			for(ClassificacaoTecido ct : labelsDetectadas) {
				sb.append(ct.getLabel()).append(" ");
				sb.append(ct.getConfidencia()).append(" ");
				sb.append(ct.getxMin()).append(" ");
				sb.append(ct.getyMin()).append(" ");
				sb.append(ct.getxMax()).append(" ");
				sb.append(ct.getyMax());
				sb.append(System.lineSeparator());
			}
			
			System.out.println("sb ["+aux+"] = "+sb.toString());
			
			FileUtils.writeStringToFile(new File(dirDestinoDetections + nameFile+".txt"), sb.toString(), false);
			iu.saveDiagnosticTemp(imRedimensionada, dirDestinoImagensDetectadas, nameFile+".JPG");
			aux++;
		}
		
		
		/*File dirImages = new File("D:\\TRABALHO_MESTRADO\\FINAL\\IMAGENS\\TEST\\JPEGImages");
		File[] filesImages = dirImages.listFiles();
		for(File image : filesImages) {
			Mat im = imread(image.getAbsolutePath());	
			Mat im2 = new Mat();
			resize(im, im2, new Size(608, 608), 0, 0, Imgproc.INTER_LANCZOS4);
			imwrite("D:\\TRABALHO_MESTRADO\\FINAL\\IMAGENS\\TEST\\JPEGImages\\NEW" + image.getName(), im2);
		}*/
		
		/*String dir = "D:\\MESTRADO\\DISSERTACAO\\PROJETO\\FINAL\\IMAGENS\\";
		//Mat im = Imgcodecs.imread(dir+"test.jpg");	
		Mat im = imread(dir+"test.jpg");	
		Mat im2 = new Mat();
		Mat im3 = new Mat();
		Mat im4 = new Mat();
		
		System.out.println("Tamanho de Entrada");
		System.out.println("Size = "+im.elemSize() + " | height = "+im.cols()+" | width = "+im.rows());

		Size sz = im.size();
		System.out.println("SIZE");
		System.out.println("Height = "+sz.height() + " | width = "+sz.width());
		
		resize(im, im2, new Size(608, 608), 0, 0, Imgproc.INTER_LANCZOS4);
		resize(im, im3, new Size(720, 720), 0.5, 0.5, Imgproc.INTER_LANCZOS4);
		resize(im, im4, new Size(720, 720), 2.0, 2.0, Imgproc.INTER_LANCZOS4);
		
		System.out.println("IMAGEM 2 = 0 | Size = "+im2.elemSize() + " | height = "+im2.rows()+" | width = "+im2.cols());
		System.out.println("IMAGEM 3 = 0.5 | Size = "+im3.elemSize() + " | height = "+im3.rows()+" | width = "+im3.cols());
		System.out.println("IMAGEM 4 = 2.0 | Size = "+im4.elemSize() + " | height = "+im4.rows()+" | width = "+im4.cols());
		
		/*Imgproc.resize(im, im2, new Size(sz.width*2, sz.height*2));				
		Imgproc.resize(im, im3, new Size(sz.width*0.5, sz.height*0.5));/
		
		imwrite(dir + "test0.jpg", im2);			
		imwrite(dir + "test05.jpg", im3);			
		imwrite(dir + "test20.jpg", im4);/			

		System.out.println("**** FIM ****");
	
	} */

}
