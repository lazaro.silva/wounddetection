package br.edu.ipt.master.survey.configuration;

import java.util.Arrays;
import java.util.List;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import br.com.agilizeware.infra.security.component.filter.AuthenticationFilter;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class SurveyAuthenticationFilter extends AuthenticationFilter {	
	
	private static final List<String> ALLOW_ACCESS = Arrays.asList(
			"/", "/index.html", "/404.html", "/login/authenticate", "/login/change/password", "/login/forgot/password", "/login/validateToken",
			"/build/polyfills.js", "/build/main.css", "/build/main.css.map", "/service-worker.js", "/build/main.js", "/build/main.js.map" , 
			"/build/vendor.js", "/build/vendor.js.map", "/assets/icon/favicon.ico", "/error",
			"/build/sw-toolbox.js", "/build/sw-toolbox.js.map", "/manifest.json", "/node_modules",
			"/assets/data/tree.json", "/assets/data/tree1.json", "/assets/data/tree2.json", "/assets/fonts/ionicons.eot",
			"/assets/fonts/ionicons.scss", "/assets/fonts/ionicons.svg", "/assets/fonts/ionicons.ttf", "/assets/fonts/ionicons.woff", "/assets/fonts/ionicons.woff2",
			"/assets/fonts/noto-sans-bold.ttf", "/assets/fonts/noto-sans-bold.woff", "/assets/fonts/noto-sans-regular.ttf", "/assets/fonts/noto-sans-regular.woff", 
			"/assets/fonts/noto-sans.scss", "/assets/fonts/roboto-bold.ttf", "/assets/fonts/roboto-bold.woff", "/assets/fonts/roboto-bold.woff2", 
			"/assets/fonts/roboto-light.ttf", "/assets/fonts/roboto-light.woff", "/assets/fonts/roboto-light.woff2", "/assets/fonts/roboto-medium.ttf", 
			"/assets/fonts/roboto-medium.woff", "/assets/fonts/roboto-medium.woff2", "/assets/fonts/roboto-regular.woff2", "/assets/fonts/roboto-regular.woff", 
			"/assets/fonts/roboto-regular.tff", "/assets/fonts/roboto.scss", 
			"/assets/imgs/logo-ionic.png", "/css/bootstrap.css", "/js/app.js", "/css/bootstrap.css.map", 
			"/auth/google", "/signin", "/signin/google", 
			"/css/style.css", "/css/font-awesome.css", "/favicon.ico", 
			"/fonts/fontawesome-webfont.woff2", "/fonts/fontawesome-webfont.woff", "/fonts/fontawesome-webfont.ttf"
	);

	protected List<String> getAllowAccess() {
		return ALLOW_ACCESS;
	}
}