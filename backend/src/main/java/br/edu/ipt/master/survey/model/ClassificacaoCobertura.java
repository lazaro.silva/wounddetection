package br.edu.ipt.master.survey.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Convert;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.fasterxml.jackson.annotation.JsonIgnore;

import br.com.agilizeware.infra.model.EntityIf;
import br.edu.ipt.master.survey.enums.CoberturasEnum;
import br.edu.ipt.master.survey.enums.CoberturasEnumConverter;
import br.edu.ipt.master.survey.validation.ValidationSisAop;

@javax.persistence.Entity
@Table(name="classificacao_cobertura")
//@ValidationSisAop(mainLabels={"lbl.nome", "lbl.cpf.cnpj", "lbl.telefone", "lbl.email"}, label="lbl.form")
public class ClassificacaoCobertura implements EntityIf {
	
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3980943059466119233L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_creation", nullable = false)
	private Date dtCreate;
	@Column(name="fk_user_creation")
	private Long idUserCreate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_updated", nullable = true)
	private Date dtUpdate;
	@Column(name="fk_user_updated", nullable = true)
	private Long idUserUpdate;
	
	@JsonIgnore
	@ManyToOne
	@JoinColumn(nullable=false, name="fk_classificacao_tecido")
	@ValidationSisAop(required=true, label="", mainLabels={"lbl.id"})
	private ClassificacaoTecido classificacaoTecido;
	@Convert(converter = CoberturasEnumConverter.class)
	@Column(name = "id_cobertura_classificada", nullable=false)
	@ValidationSisAop(required=true, label="", mainLabels={"lbl.id"})
	private CoberturasEnum coberturaClassificada;
	@Column(name="aceitacao", nullable = true)
	private Boolean isAceito;
	
	public ClassificacaoCobertura() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtCreate() {
		return dtCreate;
	}

	public void setDtCreate(Date dtCreate) {
		this.dtCreate = dtCreate;
	}

	public Long getIdUserCreate() {
		return idUserCreate;
	}

	public void setIdUserCreate(Long idUserCreate) {
		this.idUserCreate = idUserCreate;
	}
	
	public Date getDtUpdate() {
		return dtUpdate;
	}

	public void setDtUpdate(Date dtUpdate) {
		this.dtUpdate = dtUpdate;
	}

	public Long getIdUserUpdate() {
		return idUserUpdate;
	}

	public void setIdUserUpdate(Long idUserUpdate) {
		this.idUserUpdate = idUserUpdate;
	}

	public ClassificacaoTecido getClassificacaoTecido() {
		return classificacaoTecido;
	}

	public void setClassificacaoTecido(ClassificacaoTecido classificacaoTecido) {
		this.classificacaoTecido = classificacaoTecido;
	}

	public Boolean getIsAceito() {
		return isAceito;
	}

	public void setIsAceito(Boolean isAceito) {
		this.isAceito = isAceito;
	}

	public CoberturasEnum getCoberturaClassificada() {
		return coberturaClassificada;
	}

	public void setCoberturaClassificada(CoberturasEnum coberturaClassificada) {
		this.coberturaClassificada = coberturaClassificada;
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return null;
	}
	
}
