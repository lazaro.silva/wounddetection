package br.edu.ipt.master.survey.enums;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import br.com.agilizeware.infra.enums.ErrorCodeEnumDeserializer;
import br.com.agilizeware.infra.enums.IEnum;

@JsonDeserialize(using = ErrorCodeEnumDeserializer.class)
public enum FeridaErrorCodeEnum implements IEnum<Integer> {
	
	ERROR_IMAGEM_FERIDA_NAO_SALVA(2001, "Não foi possível salvar a imagem da ferida", "msg.error.imagem.ferida.nao.salva"),
	;
	
	private Integer codigo;
	private String descricao;
	private String label;
	
	private FeridaErrorCodeEnum(Integer codigo, String descricao, String label) {
		this.codigo = codigo;
		this.descricao = descricao;
		this.label = label;
	}
	
	public Integer getId() {
		return codigo;
	}

	public String getLabel() {
		return label;
	}

	public String getDescription() {
		return descricao;
	}


	public static FeridaErrorCodeEnum findByLabel(String label) {
		for (FeridaErrorCodeEnum userType : FeridaErrorCodeEnum.values()) {
			if (userType.getLabel().equals(label)) {
				return userType;
			}
		}
		throw new IllegalArgumentException("Invalid label.");
	}
	
	public static FeridaErrorCodeEnum findByCode(Integer code) {
		FeridaErrorCodeEnum[] array = FeridaErrorCodeEnum.values();
		for (FeridaErrorCodeEnum codeEnum : array) {
			if(codeEnum.getId().equals(code)) {
				return codeEnum; 
			}
		}
		return null;
	}
	
	public String toString() {
		return "{\"codigo\": "+codigo+", \"descricao\": \""+descricao+"\", \"label\": \""+label+"\"}";
	}

}
