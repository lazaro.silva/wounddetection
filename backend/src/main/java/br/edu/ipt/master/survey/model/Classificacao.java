package br.edu.ipt.master.survey.model;

import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.agilizeware.infra.model.EntityIf;
import br.edu.ipt.master.survey.validation.ValidationSisAop;

@javax.persistence.Entity
@Table(name="classificacao")
@ValidationSisAop(mainLabels={"lbl.observacoes"}, label="lbl.form")
public class Classificacao implements EntityIf {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5885204683955425674L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(unique = true, nullable = false)
	private Long id;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_creation", nullable = false)
	private Date dtCreate;
	@Column(name="fk_user_creation")
	private Long idUserCreate;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="dt_updated", nullable = true)
	private Date dtUpdate;
	@Column(name="fk_user_updated", nullable = true)
	private Long idUserUpdate;
	
	/*@Convert(converter = CoberturasEnumConverter.class)
	@Column(name = "id_cobertura_aceita", nullable=true)
	private CoberturasEnum coberturaAceita;
	@Column(name="aceitacao", nullable = true)
	private Boolean isAceito;*/
	@ValidationSisAop(required=false, label="lbl.observacoes", sizeField=2000)
	@Column(name="observacoes", nullable = true)
	private String observacoes;
	/*@OneToMany(mappedBy="classificacao")
	private List<ClassificacaoCobertura> coberturasClassificadas;*/
	@OneToMany(mappedBy="classificacao")
	private List<ClassificacaoTecido> tecidos;
	
	public Classificacao() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getDtCreate() {
		return dtCreate;
	}

	public void setDtCreate(Date dtCreate) {
		this.dtCreate = dtCreate;
	}

	public Long getIdUserCreate() {
		return idUserCreate;
	}

	public void setIdUserCreate(Long idUserCreate) {
		this.idUserCreate = idUserCreate;
	}

	public Date getDtUpdate() {
		return dtUpdate;
	}

	public void setDtUpdate(Date dtUpdate) {
		this.dtUpdate = dtUpdate;
	}

	public Long getIdUserUpdate() {
		return idUserUpdate;
	}

	public void setIdUserUpdate(Long idUserUpdate) {
		this.idUserUpdate = idUserUpdate;
	}
	
	public String getObservacoes() {
		return observacoes;
	}

	public void setObservacoes(String observacoes) {
		this.observacoes = observacoes;
	}
	
	public List<ClassificacaoTecido> getTecidos() {
		return tecidos;
	}

	public void setTecidos(List<ClassificacaoTecido> tecidos) {
		this.tecidos = tecidos;
	}

	@Override
	public String getLabel() {
		// TODO Auto-generated method stub
		return null;
	}

}
