package br.edu.ipt.master.survey.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.agilizeware.infra.dao.CrudAgilizeRepositoryIF;
import br.edu.ipt.master.survey.model.Paciente;

public interface PacienteDaoRepository extends CrudAgilizeRepositoryIF<Paciente, Long> {
	
	@Query(" select p from Paciente p where :idUsuario is null OR p.idUserCreate = :idUsuario OR p.idUserUpdate = :idUsuario ")
	List<Paciente> pesquisar(@Param("idUsuario") Long idUsuario);
}
