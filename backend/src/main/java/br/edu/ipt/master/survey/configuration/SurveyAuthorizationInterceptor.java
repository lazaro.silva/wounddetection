package br.edu.ipt.master.survey.configuration;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import br.com.agilizeware.infra.security.component.authentication.AuthorizationInterceptor;

@Component
public class SurveyAuthorizationInterceptor extends AuthorizationInterceptor {
	
	private static List<String> urlsAllowed = new ArrayList<String>(1);
	
	static {
		urlsAllowed.add("/survey/login/authenticate");
		urlsAllowed.add("/survey/login/validateToken");
	}
	
	protected List<String> getUrlsAllowed() {
		return urlsAllowed;
	}

}
