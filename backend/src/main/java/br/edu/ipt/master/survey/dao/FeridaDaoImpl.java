package br.edu.ipt.master.survey.dao;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Query;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import br.com.agilizeware.infra.dao.DaoAB;
import br.com.agilizeware.infra.util.Util;
import br.edu.ipt.master.survey.model.Classificacao;
import br.edu.ipt.master.survey.model.ClassificacaoCobertura;
import br.edu.ipt.master.survey.model.ClassificacaoTecido;
import br.edu.ipt.master.survey.model.Ferida;
import br.edu.ipt.master.survey.model.Paciente;
import br.edu.ipt.master.survey.model.Tecido;

@Component("feridaDaoIf")
public class FeridaDaoImpl extends DaoAB<Paciente, Long> {
	
	@Autowired
	private FeridaDaoRepository feridaRepository;
	@Autowired
	private ClassificacaoDaoRepository classificacaoRepository;
	@Autowired
	private ClassificacaoCoberturaDaoRepository classificacaoCoberturaRepository;
	@Autowired
	private ClassificacaoTecidoDaoRepository classificacaoTecidoRepository;

	@Autowired
	public FeridaDaoImpl(PacienteDaoRepository repository) {
		super(Paciente.class, repository);
	}
	
	@Transactional(isolation=Isolation.READ_COMMITTED, propagation=Propagation.MANDATORY, rollbackFor=Exception.class)
	//@AuditAop(action=ActionEnum.INSERT)
	public Paciente salvarPaciente(Paciente p) {
		// TODO Auto-generated method stub
		p.setFerida(feridaRepository.save(p.getFerida()));
		return super.save(p);
	}
	
	@Transactional(isolation=Isolation.READ_COMMITTED, propagation=Propagation.MANDATORY, rollbackFor=Exception.class)
	//@AuditAop(action=ActionEnum.INSERT)
	public Classificacao salvarClassificacao(Classificacao c, Ferida f) {
		
		List<ClassificacaoCobertura> coberturasClassificadas = null;
		List<ClassificacaoTecido> tecidos = null;
		if(Util.isListNotNull(c.getTecidos())) {
			tecidos = new ArrayList<ClassificacaoTecido>(c.getTecidos());
			c.setTecidos(null);
		}
		
		c = classificacaoRepository.save(c);
		if(Util.isListNotNull(tecidos)) {
			c.setTecidos(new ArrayList<ClassificacaoTecido>(1));
			for(ClassificacaoTecido tec : tecidos) {
				tec.setClassificacao(c);
				coberturasClassificadas = null;
				if(Util.isListNotNull(tec.getCoberturasClassificadas())) {
					coberturasClassificadas = new ArrayList<ClassificacaoCobertura>(tec.getCoberturasClassificadas());
					tec.setCoberturasClassificadas(null);
				}
				tec = classificacaoTecidoRepository.save(tec);
				if(Util.isListNotNull(coberturasClassificadas)) {
					tec.setCoberturasClassificadas(new ArrayList<ClassificacaoCobertura>(1));
					for(ClassificacaoCobertura cb : coberturasClassificadas) {
						cb.setClassificacaoTecido(tec);
						cb = classificacaoCoberturaRepository.save(cb);
						tec.getCoberturasClassificadas().add(cb);
					}
				}
				c.getTecidos().add(tec);
			}
		}
		
		alterarFerida(f);
		
		return c;
	}	
	
	@Transactional(isolation=Isolation.READ_COMMITTED, propagation=Propagation.MANDATORY, rollbackFor=Exception.class)
	//@AuditAop(action=ActionEnum.INSERT)
	public void alterarClassificacao(Classificacao c) {
		
		/*sb.append(" update Classificacao c set c.coberturaAceita = :coberturaAceita, c.isAceito = :isAceito, c.observacoes = :observacoes ");
		sb.append(" c.dtUpdate = :dtUpdate, c.idUserUpdate = :idUserUpdate where c.id = :id ");*/
		//q.setParameter("coberturaAceita", c.getCoberturaAceita());
		//q.setParameter("isAceito", c.getIsAceito());

		StringBuffer sb = new StringBuffer("");
		sb.append(" update Classificacao set observacoes = :observacoes, ");
		sb.append(" dtUpdate = :dtUpdate, idUserUpdate = :idUserUpdate where id = :id ");
		Query q = super.em.createQuery(sb.toString());
		q.setParameter("observacoes", c.getObservacoes());
		q.setParameter("dtUpdate", c.getDtUpdate());
		q.setParameter("idUserUpdate", c.getIdUserUpdate());
		q.setParameter("id", c.getId());
		q.executeUpdate();

		//classificacaoRepository.alterarClassificacao(c.getObservacoes(), c.getDtUpdate(), c.getIdUserUpdate(), c.getId());
	}
	
	@Transactional(isolation=Isolation.READ_COMMITTED, propagation=Propagation.MANDATORY, rollbackFor=Exception.class)
	//@AuditAop(action=ActionEnum.INSERT)
	public void alterarClassificacaoCobertura(ClassificacaoCobertura c) {
		
		StringBuffer sb = new StringBuffer("");
		sb.append(" Update ClassificacaoCobertura SET dtUpdate=:dtUpdate, idUserUpdate=:idUserUpdate, isAceito=:isAceito WHERE id=:id ");
		Query q = super.em.createQuery(sb.toString());
		q.setParameter("dtUpdate", c.getDtUpdate());
		q.setParameter("idUserUpdate", c.getIdUserUpdate());
		q.setParameter("isAceito", c.getIsAceito());
		q.setParameter("id", c.getId());
		q.executeUpdate();
		
		//classificacaoCoberturaRepository.alterarClassificacaoCobertura(c.getDtUpdate(), c.getIdUserUpdate(), c.getIsAceito(), c.getId());
	}
	
	private void alterarFerida(Ferida f) {
		StringBuffer sb = new StringBuffer("");
		sb.append(" update Ferida f set f.arquivoClassificado = :arquivoClassificado where f.id = :id ");
		Query q = super.em.createQuery(sb.toString());
		q.setParameter("arquivoClassificado", f.getArquivoClassificado());
		q.setParameter("id", f.getId());
		q.executeUpdate();
	}
	
	public List<Paciente> pesquisar(Long idUsuario) {
		return ((PacienteDaoRepository)super.getRepositorio()).pesquisar(idUsuario);
	}

	public Tecido findTecidoByLabel(String label) {
		return classificacaoTecidoRepository.findByLabel(label);
	}
	
}