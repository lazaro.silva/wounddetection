package br.edu.ipt.master.survey.enums;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import br.com.agilizeware.infra.util.Util;


@Converter
public class CoberturasEnumConverter implements AttributeConverter<CoberturasEnum, Integer> {

	@Override
	public Integer convertToDatabaseColumn(CoberturasEnum arg0) {
		// TODO Auto-generated method stub
		if(Util.isNotNull(arg0)) {
			return arg0.getId();
		}
		return null;
	}

	@Override
	public CoberturasEnum convertToEntityAttribute(Integer arg0) {
		// TODO Auto-generated method stub
		if(Util.isNotNull(arg0)) {
			return CoberturasEnum.findByCode(arg0);
		}
		return null;
	}

	
}
