package br.edu.ipt.master.survey.dao;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import br.com.agilizeware.infra.dao.CrudAgilizeRepositoryIF;
import br.edu.ipt.master.survey.model.ClassificacaoTecido;
import br.edu.ipt.master.survey.model.Tecido;

public interface ClassificacaoTecidoDaoRepository extends CrudAgilizeRepositoryIF<ClassificacaoTecido, Long> {
	
	@Query(" select t from Tecido t where t.label = :label ")
	Tecido findByLabel(@Param("label") String label);

}
