webpackJsonp([15],{

/***/ 160:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api_api__ = __webpack_require__(329);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return __WEBPACK_IMPORTED_MODULE_0__api_api__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__mocks_providers_items__ = __webpack_require__(330);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "b", function() { return __WEBPACK_IMPORTED_MODULE_1__mocks_providers_items__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__settings_settings__ = __webpack_require__(682);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "c", function() { return __WEBPACK_IMPORTED_MODULE_2__settings_settings__["a"]; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__user_user__ = __webpack_require__(683);
/* harmony reexport (binding) */ __webpack_require__.d(__webpack_exports__, "d", function() { return __WEBPACK_IMPORTED_MODULE_3__user_user__["a"]; });




//# sourceMappingURL=index.js.map

/***/ }),

/***/ 161:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return FeridaProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__base__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(77);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var FeridaProvider = /** @class */ (function (_super) {
    __extends(FeridaProvider, _super);
    function FeridaProvider(http) {
        var _this = _super.call(this) || this;
        _this.http = http;
        return _this;
    }
    FeridaProvider.prototype.salvarPaciente = function (paciente) {
        var url = this.api + '/ferida/salvarPaciente';
        return this.http.post(url, JSON.stringify(paciente), this.buildOpts())
            .map(function (response) {
            var _data = response.json().data;
            if (response.json().success)
                return _data;
            else {
                throw _data;
            }
        })
            .catch(function (e) { throw e; });
    };
    FeridaProvider.prototype.pesquisarPacientes = function () {
        var url = this.api + '/ferida/pesquisarPacientes';
        return this.http.get(url, this.buildOpts()).map(function (response) {
            var _data = response.json().data;
            if (response.json().success)
                return _data;
            else {
                throw _data;
            }
        })
            .catch(function (e) { throw e; });
    };
    FeridaProvider.prototype.getNomeCobertura = function (label) {
        var url = this.api + '/ferida/nomeCobertura?label=' + label;
        return this.http.get(url, this.buildOpts()).map(function (response) { return response.json().data; });
    };
    /*pesquisarPacientes(): Promise<any> {
      return new Promise((resolve, reject) => {
        let url = this.api + '/ferida/pesquisarPacientes';
        var req = new XMLHttpRequest();
        req.onloadend = function (data) {
          if (req.status !== 200) {
            reject(req.responseText);
          } else {
            let _data = JSON.parse(req.response);
            if (_data.success)
              resolve(_data.data);
            else {
              throw _data.data;
            }
          }
        };
  
        let tokenStr = localStorage.getItem('token');
        //let idLoja = this.onlyNumber(localStorage.getItem('idLoja'));
        let tokenObj = JSON.parse(tokenStr);
  
        req.open('GET', url, false);
        req.setRequestHeader('Content-Type', 'application/json');
        req.setRequestHeader('x-auth-token', tokenObj.data.token);
        req.send();
      });
    }*/
    FeridaProvider.prototype.obterArquivo = function (pathLogical) {
        var url = this.api + '/' + pathLogical;
        return this.http.get(url, this.buildOpts(true, 'image/jpg', true)).map(function (response) {
            return response; //new Blob([response], { type: 'image/jpg' });
        }).catch(function (e) { throw e; });
    };
    FeridaProvider.prototype.fecharAnalise = function (classificacao) {
        var url = this.api + '/ferida/fecharAnalise';
        return this.http.put(url, JSON.stringify(classificacao), this.buildOpts())
            .map(function (response) {
            var _data = response.json().data;
            if (response.json().success)
                return _data;
            else {
                throw _data;
            }
        })
            .catch(function (e) { throw e; });
    };
    FeridaProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], FeridaProvider);
    return FeridaProvider;
}(__WEBPACK_IMPORTED_MODULE_1__base__["a" /* BaseProvider */]));

//# sourceMappingURL=ferida.js.map

/***/ }),

/***/ 162:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return LoginPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_device_detector__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ngx_device_detector___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_ngx_device_detector__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_alert_alert_controller__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__list_master_list_master__ = __webpack_require__(90);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginPage = /** @class */ (function () {
    // Our translated text strings
    //private loginErrorString: string;
    function LoginPage(navCtrl, 
        //public user: User, 
        alertCtrl, 
        //public toastCtrl: ToastController,
        //public translateService: TranslateService
        auth, deviceService) {
        this.navCtrl = navCtrl;
        this.alertCtrl = alertCtrl;
        this.auth = auth;
        this.deviceService = deviceService;
        // The account fields for the login form.
        // If you're using the username field with or without email, make
        // sure to add it to the type
        this.account = {
            email: 'test@example.com',
            cpf: '11111111111',
            password: 'test'
        };
        /*this.translateService.get('LOGIN_ERROR').subscribe((value) => {
          this.loginErrorString = value;
        })*/
    }
    LoginPage.prototype.checkDevice = function () {
        if (this.deviceService.isMobile()) {
            return 1;
        }
        else if (this.deviceService.isTablet()) {
            return 2;
        }
        return 3;
    };
    LoginPage.prototype.doLogin = function () {
        var _this = this;
        //1=Celular;2=Tablet;3=Desktop
        var device = this.checkDevice();
        //tipo Email = 0; CPF = 1
        this.auth.login(this.account.cpf, this.account.password, 0, device).then(function (retorno) {
            if (retorno) {
                _this.navCtrl.setRoot(__WEBPACK_IMPORTED_MODULE_5__list_master_list_master__["a" /* ListMasterPage */]);
            }
            else {
                //this.navCtrl.setRoot(MainPage);
                _this.alertCtrl.create({
                    message: 'Erro durante o processo de autenticação. Tente novamente!',
                    buttons: ['Ok']
                }).present();
            }
        }).catch(function (retorno) {
            //this.navCtrl.setRoot(MainPage);
            _this.alertCtrl.create({
                message: !retorno.data ? retorno : retorno.data.showMessage,
                buttons: ['Ok']
            }).present();
        });
    };
    LoginPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-login',template:/*ion-inline-start:"D:\MESTRADO\DISSERTACAO\PROJETO\FINAL\WORKSPACE\FONTES\APP\woundDetector\src\pages\login\login.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Login</ion-title>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n  <form (submit)="doLogin()">\n    <ion-list>\n\n      <!-- <ion-item>\n        <ion-label fixed>E-Mail</ion-label>\n        <ion-input type="email" [(ngModel)]="account.email" name="email"></ion-input>\n      </ion-item> -->\n\n      <ion-item>\n        <ion-label fixed>Usuario</ion-label>\n        <ion-input type="text" [(ngModel)]="account.cpf" name="cpf"></ion-input>\n      </ion-item>\n\n      <!--\n      Want to use a Username instead of an Email? Here you go:\n\n      <ion-item>\n        <ion-label floating>{{ \'USERNAME\' | translate }}</ion-label>\n        <ion-input type="text" [(ngModel)]="account.username" name="username"></ion-input>\n      </ion-item>\n      -->\n\n      <ion-item>\n        <ion-label fixed>Senha</ion-label>\n        <ion-input type="password" [(ngModel)]="account.password" name="password"></ion-input>\n      </ion-item>\n\n      <div padding>\n        <button ion-button color="primary" block>OK</button>\n      </div>\n\n    </ion-list>\n  </form>\n</ion-content>\n'/*ion-inline-end:"D:\MESTRADO\DISSERTACAO\PROJETO\FINAL\WORKSPACE\FONTES\APP\woundDetector\src\pages\login\login.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */],
            __WEBPACK_IMPORTED_MODULE_4_ionic_angular_components_alert_alert_controller__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_2__providers_auth_auth__["a" /* AuthProvider */], __WEBPACK_IMPORTED_MODULE_3_ngx_device_detector__["DeviceDetectorService"]])
    ], LoginPage);
    return LoginPage;
}());

//# sourceMappingURL=login.js.map

/***/ }),

/***/ 173:
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncatched exception popping up in devtools
	return Promise.resolve().then(function() {
		throw new Error("Cannot find module '" + req + "'.");
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = 173;

/***/ }),

/***/ 242:
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"../pages/cards/cards.module": [
		703,
		12
	],
	"../pages/content/content.module": [
		704,
		11
	],
	"../pages/item-classification-observacao/item-classification-observacao.module": [
		705,
		10
	],
	"../pages/item-classification/item-classification.module": [
		706,
		0
	],
	"../pages/item-create/item-create.module": [
		707,
		9
	],
	"../pages/item-detail/item-detail.module": [
		708,
		8
	],
	"../pages/list-master/list-master.module": [
		709,
		14
	],
	"../pages/login/login.module": [
		710,
		13
	],
	"../pages/menu/menu.module": [
		711,
		7
	],
	"../pages/search/search.module": [
		712,
		6
	],
	"../pages/settings/settings.module": [
		713,
		5
	],
	"../pages/signup/signup.module": [
		714,
		2
	],
	"../pages/tabs/tabs.module": [
		715,
		1
	],
	"../pages/tutorial/tutorial.module": [
		716,
		4
	],
	"../pages/welcome/welcome.module": [
		717,
		3
	]
};
function webpackAsyncContext(req) {
	var ids = map[req];
	if(!ids)
		return Promise.reject(new Error("Cannot find module '" + req + "'."));
	return __webpack_require__.e(ids[1]).then(function() {
		return __webpack_require__(ids[0]);
	});
};
webpackAsyncContext.keys = function webpackAsyncContextKeys() {
	return Object.keys(map);
};
webpackAsyncContext.id = 242;
module.exports = webpackAsyncContext;

/***/ }),

/***/ 243:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BaseProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_http__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__(416);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);


// import { Network } from '@ionic-native/network';
var extractError = function (error) {
    // In a real world app, we might use a remote logging infrastructure
    var errMsg;
    if (error instanceof __WEBPACK_IMPORTED_MODULE_0__angular_http__["e" /* Response */]) {
        var body = error.json() || '';
        var err = body.error || JSON.stringify(body);
        errMsg = error.status + " - " + (error.statusText || '') + " " + err;
    }
    else {
        errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return errMsg;
};
var BaseProvider = /** @class */ (function () {
    function BaseProvider() {
        this.api = "http://localhost:9518/survey";
    }
    BaseProvider.prototype.handlePromiseError = function (error) {
        return Promise.reject(extractError(error));
    };
    BaseProvider.prototype.handleObservableError = function (error) {
        return __WEBPACK_IMPORTED_MODULE_1_rxjs__["Observable"].throw(extractError(error));
    };
    BaseProvider.prototype.onlyNumber = function (txt) {
        if (txt)
            return txt.toString().replace(/\D/g, '');
        return '';
    };
    BaseProvider.prototype.buildOpts = function (file, contentType, arrayBuffer) {
        var tokenStr = localStorage.getItem('token');
        //let idLoja = this.onlyNumber(localStorage.getItem('idLoja'));
        var tokenObj = JSON.parse(tokenStr);
        var opts = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["d" /* RequestOptions */]();
        opts.headers = new __WEBPACK_IMPORTED_MODULE_0__angular_http__["a" /* Headers */]();
        opts.headers.append('x-auth-token', tokenObj.data.token);
        //opts.headers.append('idLoja', idLoja);
        if (!file)
            opts.headers.append('Content-Type', 'application/json');
        if (contentType) {
            opts.headers.append('Content-Type', contentType);
        }
        if (arrayBuffer)
            opts.responseType = __WEBPACK_IMPORTED_MODULE_0__angular_http__["f" /* ResponseContentType */].ArrayBuffer;
        return opts;
    };
    return BaseProvider;
}());

//# sourceMappingURL=base.js.map

/***/ }),

/***/ 329:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Api; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * Api is a generic REST Api handler. Set your API url first.
 */
var Api = /** @class */ (function () {
    function Api(http) {
        this.http = http;
        this.url = 'https://example.com/api/v1';
    }
    Api.prototype.get = function (endpoint, params, reqOpts) {
        if (!reqOpts) {
            reqOpts = {
                params: new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]()
            };
        }
        // Support easy query params for GET requests
        if (params) {
            reqOpts.params = new __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["c" /* HttpParams */]();
            for (var k in params) {
                reqOpts.params = reqOpts.params.set(k, params[k]);
            }
        }
        return this.http.get(this.url + '/' + endpoint, reqOpts);
    };
    Api.prototype.post = function (endpoint, body, reqOpts) {
        return this.http.post(this.url + '/' + endpoint, body, reqOpts);
    };
    Api.prototype.put = function (endpoint, body, reqOpts) {
        return this.http.put(this.url + '/' + endpoint, body, reqOpts);
    };
    Api.prototype.delete = function (endpoint, reqOpts) {
        return this.http.delete(this.url + '/' + endpoint, reqOpts);
    };
    Api.prototype.patch = function (endpoint, body, reqOpts) {
        return this.http.patch(this.url + '/' + endpoint, body, reqOpts);
    };
    Api = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]])
    ], Api);
    return Api;
}());

//# sourceMappingURL=api.js.map

/***/ }),

/***/ 330:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Items; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__models_item__ = __webpack_require__(681);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var Items = /** @class */ (function () {
    function Items() {
        this.items = [];
        this.defaultItem = {
            "name": "Burt Bear",
            "profilePic": "assets/img/speakers/bear.jpg",
            "about": "Burt is a Bear.",
        };
        var items = [
            {
                "name": "Burt Bear",
                "profilePic": "assets/img/speakers/bear.jpg",
                "about": "Burt is a Bear."
            },
            {
                "name": "Charlie Cheetah",
                "profilePic": "assets/img/speakers/cheetah.jpg",
                "about": "Charlie is a Cheetah."
            },
            {
                "name": "Donald Duck",
                "profilePic": "assets/img/speakers/duck.jpg",
                "about": "Donald is a Duck."
            },
            {
                "name": "Eva Eagle",
                "profilePic": "assets/img/speakers/eagle.jpg",
                "about": "Eva is an Eagle."
            },
            {
                "name": "Ellie Elephant",
                "profilePic": "assets/img/speakers/elephant.jpg",
                "about": "Ellie is an Elephant."
            },
            {
                "name": "Molly Mouse",
                "profilePic": "assets/img/speakers/mouse.jpg",
                "about": "Molly is a Mouse."
            },
            {
                "name": "Paul Puppy",
                "profilePic": "assets/img/speakers/puppy.jpg",
                "about": "Paul is a Puppy."
            }
        ];
        for (var _i = 0, items_1 = items; _i < items_1.length; _i++) {
            var item = items_1[_i];
            this.items.push(new __WEBPACK_IMPORTED_MODULE_1__models_item__["a" /* Item */](item));
        }
    }
    Items.prototype.query = function (params) {
        if (!params) {
            return this.items;
        }
        return this.items.filter(function (item) {
            for (var key in params) {
                var field = item[key];
                if (typeof field == 'string' && field.toLowerCase().indexOf(params[key].toLowerCase()) >= 0) {
                    return item;
                }
                else if (field == params[key]) {
                    return item;
                }
            }
            return null;
        });
    };
    Items.prototype.add = function (item) {
        this.items.push(item);
    };
    Items.prototype.delete = function (item) {
        this.items.splice(this.items.indexOf(item), 1);
    };
    Items = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [])
    ], Items);
    return Items;
}());

//# sourceMappingURL=items.js.map

/***/ }),

/***/ 372:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return BasePage; });
var BasePage = /** @class */ (function () {
    function BasePage(alertCtrl, sanitizer) {
        this.alertCtrl = alertCtrl;
        this.sanitizer = sanitizer;
        this.decimalSeparator = ".";
        this.groupSeparator = ",";
        this.p = 1;
    }
    BasePage.prototype.ionViewCanEnter = function () {
        return true;
    };
    BasePage.prototype.tratarMsg = function (data) {
        var msg = (data.showMessage || data.message) + '<br/>';
        if (data.errors && data.errors.length > 0) {
            data.errors.forEach(function (element) {
                msg += '<p><br/>' + element.showMessage.replace('lbl.', '') + '</p>';
                //msg = `<p>${msg}</p>`;
            });
        }
        return msg;
    };
    BasePage.prototype.mostrarErro = function (msg) {
        var alert = this.alertCtrl.create({
            title: 'Erro',
            message: msg,
            buttons: ['Ok'],
            cssClass: 'custom-alert'
        });
        alert.present();
    };
    BasePage.prototype.mostrarSucesso = function (msg) {
        var alert = this.alertCtrl.create({
            title: 'Sucesso',
            message: msg,
            buttons: ['Ok'],
            cssClass: 'custom-alert'
        });
        alert.present();
    };
    BasePage.prototype.formatCpfCnpj = function (valString) {
        if (!valString) {
            return '';
        }
        var val = this.onlyNumber(valString.toString());
        if (val.length > 14) {
            val = val.substring(0, 14);
        }
        var parts = this.unFormat(val).split(this.decimalSeparator);
        this.pureResult = parts;
        if (parts[0].length <= 11) {
            this.maskedId = this.cpf_mask(parts[0]);
            return this.maskedId;
        }
        else {
            this.maskedId = this.cnpj(parts[0]);
            return this.maskedId;
        }
    };
    ;
    BasePage.prototype.formatCpf = function (valString) {
        if (!valString) {
            return '';
        }
        var val = this.onlyNumber(valString.toString());
        if (val.length > 11) {
            val = val.substring(0, 11);
        }
        var parts = this.unFormat(val).split(this.decimalSeparator);
        this.pureResult = parts;
        this.maskedId = this.cpf_mask(parts[0]);
        return this.maskedId;
    };
    ;
    BasePage.prototype.formatCnpj = function (valString) {
        if (!valString) {
            return '';
        }
        var val = this.onlyNumber(valString.toString());
        if (val.length > 14) {
            val = val.substring(0, 14);
        }
        var parts = this.unFormat(val).split(this.decimalSeparator);
        this.pureResult = parts;
        this.maskedId = this.cnpj(parts[0]);
        return this.maskedId;
    };
    ;
    BasePage.prototype.onlyNumber = function (txt) {
        if (txt)
            return txt.toString().replace(/\D/g, '');
        return '';
    };
    BasePage.prototype.formatPhone = function (valString) {
        if (!valString) {
            return '';
        }
        var val = this.onlyNumber(valString.toString());
        if (val.length > 11) {
            val = val.substring(0, 11);
        }
        var parts = this.unFormat(val).split(this.decimalSeparator);
        this.pureResult = parts;
        this.maskedId = this.phone(parts[0]);
        return this.maskedId;
    };
    ;
    BasePage.prototype.formatValor = function (valString) {
        if (!valString) {
            return '';
        }
        var val = valString.toString();
        var parts = this.unFormat(val).split(this.decimalSeparator);
        this.pureResult = parts;
        this.maskedId = this.valor(parts[0]);
        return this.maskedId;
    };
    ;
    BasePage.prototype.formatCep = function (valString) {
        if (!valString) {
            return '';
        }
        var val = this.onlyNumber(valString.toString());
        if (val.length > 8) {
            val = val.substring(0, 8);
        }
        var parts = this.unFormat(val).split(this.decimalSeparator);
        this.pureResult = parts;
        this.maskedId = this.cep(parts[0]);
        return this.maskedId;
    };
    ;
    BasePage.prototype.unFormat = function (val) {
        if (!val) {
            return '';
        }
        val = val.replace(/\D/g, '');
        if (this.groupSeparator === ',') {
            return val.replace(/,/g, '');
        }
        else {
            return val.replace(/\./g, '');
        }
    };
    ;
    BasePage.prototype.cnpj = function (v) {
        v = v.replace(/\D/g, '');
        v = v.replace(/^(\d{2})(\d)/, '$1.$2');
        v = v.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3');
        v = v.replace(/\.(\d{3})(\d)/, '.$1/$2');
        v = v.replace(/(\d{4})(\d)/, '$1-$2');
        return v;
    };
    BasePage.prototype.cpf_mask = function (v) {
        v = v.replace(/\D/g, '');
        v = v.replace(/(\d{3})(\d)/, '$1.$2');
        v = v.replace(/(\d{3})(\d)/, '$1.$2');
        v = v.replace(/(\d{3})(\d{1,2})$/, '$1-$2');
        return v;
    };
    BasePage.prototype.phone = function (v) {
        v = v.replace(/\D/g, "");
        v = v.replace(/^(\d{2})(\d)/g, "($1) $2");
        v = v.replace(/(\d)(\d{4})$/, "$1-$2");
        return v;
    };
    BasePage.prototype.valor = function (v) {
        if (v && typeof (v) == 'string') {
            v = v.replace(/\D/g, "");
            v = v.replace(/(\d)(\d{8})$/, "$1.$2");
            v = v.replace(/(\d)(\d{5})$/, "$1.$2");
            v = v.replace(/(\d)(\d{2})$/, "$1,$2");
        }
        return v;
    };
    BasePage.prototype.cep = function (v) {
        v = v.replace(/\D/g, "");
        v = v.replace(/^(\d{5})(\d)/, "$1-$2");
        return v;
    };
    BasePage.prototype.sanitizarArquivo = function (blob) {
        //var blob = new Blob([item.ferida.fotoResource], { type: 'image/jpg' });
        console.log('sanitizarArquivo', blob);
        if (blob)
            return this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(blob));
        return '';
    };
    return BasePage;
}());

//# sourceMappingURL=base-page.js.map

/***/ }),

/***/ 374:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__ = __webpack_require__(375);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(379);


Object(__WEBPACK_IMPORTED_MODULE_0__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_1__app_module__["a" /* AppModule */]);
//# sourceMappingURL=main.js.map

/***/ }),

/***/ 379:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* unused harmony export createTranslateLoader */
/* unused harmony export provideSettings */
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AppModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_common_http__ = __webpack_require__(174);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__ = __webpack_require__(31);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__ionic_storage__ = __webpack_require__(182);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__ngx_translate_http_loader__ = __webpack_require__(395);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_ionic_angular__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__mocks_providers_items__ = __webpack_require__(330);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__providers__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__app_component__ = __webpack_require__(702);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ngx_device_detector__ = __webpack_require__(331);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_ngx_device_detector___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_ngx_device_detector__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14__providers_auth_auth__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15__providers_ferida_ferida__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_16__angular_http__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_17__pages_login_login__ = __webpack_require__(162);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_18__pages_list_master_list_master__ = __webpack_require__(90);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};



















// The translate loader needs to know where to load i18n files
// in Ionic's static asset pipeline.
function createTranslateLoader(http) {
    return new __WEBPACK_IMPORTED_MODULE_8__ngx_translate_http_loader__["a" /* TranslateHttpLoader */](http, './assets/i18n/', '.json');
}
function provideSettings(storage) {
    /**
     * The Settings provider takes a set of default settings for your app.
     *
     * You can add new settings options at any time. Once the settings are saved,
     * these values will not overwrite the saved values (this can be done manually if desired).
     */
    return new __WEBPACK_IMPORTED_MODULE_11__providers__["c" /* Settings */](storage, {
        option1: true,
        option2: 'Ionitron J. Framework',
        option3: '3',
        option4: 'Hello'
    });
}
var AppModule = /** @class */ (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_17__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_list_master_list_master__["a" /* ListMasterPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2__angular_platform_browser__["a" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_0__angular_common_http__["b" /* HttpClientModule */],
                __WEBPACK_IMPORTED_MODULE_16__angular_http__["c" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__["b" /* TranslateModule */].forRoot({
                    loader: {
                        provide: __WEBPACK_IMPORTED_MODULE_7__ngx_translate_core__["a" /* TranslateLoader */],
                        useFactory: (createTranslateLoader),
                        deps: [__WEBPACK_IMPORTED_MODULE_0__angular_common_http__["a" /* HttpClient */]]
                    }
                }),
                __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["e" /* IonicModule */].forRoot(__WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */], {}, {
                    links: [
                        { loadChildren: '../pages/cards/cards.module#CardsPageModule', name: 'CardsPage', segment: 'cards', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/content/content.module#ContentPageModule', name: 'ContentPage', segment: 'content', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/item-classification-observacao/item-classification-observacao.module#ItemClassificationObservacaoPageModule', name: 'Observacao', segment: 'item-classification-observacao', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/item-classification/item-classification.module#ItemClassificationPageModule', name: 'ItemClassificationPage', segment: 'item-classification', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/item-create/item-create.module#ItemCreatePageModule', name: 'ItemCreatePage', segment: 'item-create', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/item-detail/item-detail.module#ItemDetailPageModule', name: 'ItemDetailPage', segment: 'item-detail', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/list-master/list-master.module#ListMasterPageModule', name: 'ListMasterPage', segment: 'list-master', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/login/login.module#LoginPageModule', name: 'LoginPage', segment: 'login', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/menu/menu.module#MenuPageModule', name: 'MenuPage', segment: 'menu', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/search/search.module#SearchPageModule', name: 'SearchPage', segment: 'search', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/settings/settings.module#SettingsPageModule', name: 'SettingsPage', segment: 'settings', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/signup/signup.module#SignupPageModule', name: 'SignupPage', segment: 'signup', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tabs/tabs.module#TabsPageModule', name: 'TabsPage', segment: 'tabs', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/tutorial/tutorial.module#TutorialPageModule', name: 'TutorialPage', segment: 'tutorial', priority: 'low', defaultHistory: [] },
                        { loadChildren: '../pages/welcome/welcome.module#WelcomePageModule', name: 'WelcomePage', segment: 'welcome', priority: 'low', defaultHistory: [] }
                    ]
                }),
                __WEBPACK_IMPORTED_MODULE_6__ionic_storage__["a" /* IonicStorageModule */].forRoot(),
                __WEBPACK_IMPORTED_MODULE_13_ngx_device_detector__["DeviceDetectorModule"].forRoot()
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_9_ionic_angular__["c" /* IonicApp */]],
            entryComponents: [
                __WEBPACK_IMPORTED_MODULE_12__app_component__["a" /* MyApp */],
                __WEBPACK_IMPORTED_MODULE_17__pages_login_login__["a" /* LoginPage */],
                __WEBPACK_IMPORTED_MODULE_18__pages_list_master_list_master__["a" /* ListMasterPage */]
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_11__providers__["a" /* Api */],
                __WEBPACK_IMPORTED_MODULE_14__providers_auth_auth__["a" /* AuthProvider */],
                __WEBPACK_IMPORTED_MODULE_15__providers_ferida_ferida__["a" /* FeridaProvider */],
                __WEBPACK_IMPORTED_MODULE_10__mocks_providers_items__["a" /* Items */],
                __WEBPACK_IMPORTED_MODULE_11__providers__["d" /* User */],
                __WEBPACK_IMPORTED_MODULE_3__ionic_native_camera__["a" /* Camera */],
                __WEBPACK_IMPORTED_MODULE_4__ionic_native_splash_screen__["a" /* SplashScreen */],
                __WEBPACK_IMPORTED_MODULE_5__ionic_native_status_bar__["a" /* StatusBar */],
                { provide: __WEBPACK_IMPORTED_MODULE_11__providers__["c" /* Settings */], useFactory: provideSettings, deps: [__WEBPACK_IMPORTED_MODULE_6__ionic_storage__["b" /* Storage */]] },
                // Keep this to enable Ionic's runtime error handling during development
                { provide: __WEBPACK_IMPORTED_MODULE_1__angular_core__["ErrorHandler"], useClass: __WEBPACK_IMPORTED_MODULE_9_ionic_angular__["d" /* IonicErrorHandler */] }
            ]
        })
    ], AppModule);
    return AppModule;
}());

//# sourceMappingURL=app.module.js.map

/***/ }),

/***/ 681:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Item; });
/**
 * A generic model that our Master-Detail pages list, create, and delete.
 *
 * Change "Item" to the noun your app will use. For example, a "Contact," or a
 * "Customer," or an "Animal," or something like that.
 *
 * The Items service manages creating instances of Item, so go ahead and rename
 * that something that fits your app as well.
 */
var Item = /** @class */ (function () {
    function Item(fields) {
        // Quick and dirty extend/assign fields to this model
        for (var f in fields) {
            // @ts-ignore
            this[f] = fields[f];
        }
    }
    return Item;
}());

//# sourceMappingURL=item.js.map

/***/ }),

/***/ 682:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Settings; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_storage__ = __webpack_require__(182);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


/**
 * A simple settings/config class for storing key/value pairs with persistence.
 */
var Settings = /** @class */ (function () {
    function Settings(storage, defaults) {
        this.storage = storage;
        this.SETTINGS_KEY = '_settings';
        this._defaults = defaults;
    }
    Settings.prototype.load = function () {
        var _this = this;
        return this.storage.get(this.SETTINGS_KEY).then(function (value) {
            if (value) {
                _this.settings = value;
                return _this._mergeDefaults(_this._defaults);
            }
            else {
                return _this.setAll(_this._defaults).then(function (val) {
                    _this.settings = val;
                });
            }
        });
    };
    Settings.prototype._mergeDefaults = function (defaults) {
        for (var k in defaults) {
            if (!(k in this.settings)) {
                this.settings[k] = defaults[k];
            }
        }
        return this.setAll(this.settings);
    };
    Settings.prototype.merge = function (settings) {
        for (var k in settings) {
            this.settings[k] = settings[k];
        }
        return this.save();
    };
    Settings.prototype.setValue = function (key, value) {
        this.settings[key] = value;
        return this.storage.set(this.SETTINGS_KEY, this.settings);
    };
    Settings.prototype.setAll = function (value) {
        return this.storage.set(this.SETTINGS_KEY, value);
    };
    Settings.prototype.getValue = function (key) {
        return this.storage.get(this.SETTINGS_KEY)
            .then(function (settings) {
            return settings[key];
        });
    };
    Settings.prototype.save = function () {
        return this.setAll(this.settings);
    };
    Object.defineProperty(Settings.prototype, "allSettings", {
        get: function () {
            return this.settings;
        },
        enumerable: true,
        configurable: true
    });
    Settings = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1__ionic_storage__["b" /* Storage */], Object])
    ], Settings);
    return Settings;
}());

//# sourceMappingURL=settings.js.map

/***/ }),

/***/ 683:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return User; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_toPromise__ = __webpack_require__(317);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_toPromise___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_rxjs_add_operator_toPromise__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__api_api__ = __webpack_require__(329);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



/**
 * Most apps have the concept of a User. This is a simple provider
 * with stubs for login/signup/etc.
 *
 * This User provider makes calls to our API at the `login` and `signup` endpoints.
 *
 * By default, it expects `login` and `signup` to return a JSON object of the shape:
 *
 * ```json
 * {
 *   status: 'success',
 *   user: {
 *     // User fields your app needs, like "id", "name", "email", etc.
 *   }
 * }Ø
 * ```
 *
 * If the `status` field is not `success`, then an error is detected and returned.
 */
var User = /** @class */ (function () {
    function User(api) {
        this.api = api;
    }
    /**
     * Send a POST request to our login endpoint with the data
     * the user entered on the form.
     */
    User.prototype.login = function (accountInfo) {
        var _this = this;
        var seq = this.api.post('login', accountInfo).share();
        seq.subscribe(function (res) {
            // If the API returned a successful response, mark the user as logged in
            if (res.status == 'success') {
                _this._loggedIn(res);
            }
            else {
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    /**
     * Send a POST request to our signup endpoint with the data
     * the user entered on the form.
     */
    User.prototype.signup = function (accountInfo) {
        var _this = this;
        var seq = this.api.post('signup', accountInfo).share();
        seq.subscribe(function (res) {
            // If the API returned a successful response, mark the user as logged in
            if (res.status == 'success') {
                _this._loggedIn(res);
            }
        }, function (err) {
            console.error('ERROR', err);
        });
        return seq;
    };
    /**
     * Log the user out, which forgets the session
     */
    User.prototype.logout = function () {
        this._user = null;
    };
    /**
     * Process a login/signup response to store user data
     */
    User.prototype._loggedIn = function (resp) {
        this._user = resp.user;
    };
    User = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_1__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__api_api__["a" /* Api */]])
    ], User);
    return User;
}());

//# sourceMappingURL=user.js.map

/***/ }),

/***/ 702:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return MyApp; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ionic_native_splash_screen__ = __webpack_require__(180);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__ = __webpack_require__(181);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_alert_alert_controller__ = __webpack_require__(75);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__pages_list_master_list_master__ = __webpack_require__(90);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__pages_login_login__ = __webpack_require__(162);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



//import { TranslateService } from '@ngx-translate/core';

//import { FirstRunPage } from '../pages';
//import { Settings } from '../providers';




var MyApp = /** @class */ (function () {
    /*
    pages: any[] = [
      { title: 'Tutorial', component: 'TutorialPage' },
      { title: 'Welcome', component: 'WelcomePage' },
      { title: 'Tabs', component: 'TabsPage' },
      { title: 'Cards', component: 'CardsPage' },
      { title: 'Content', component: 'ContentPage' },
      { title: 'Login', component: 'LoginPage' },
      { title: 'Signup', component: 'SignupPage' },
      { title: 'Master Detail', component: 'ListMasterPage' },
      { title: 'Menu', component: 'MenuPage' },
      { title: 'Settings', component: 'SettingsPage' },
      { title: 'Search', component: 'SearchPage' }
    ]*/
    function MyApp(//private translate: TranslateService, 
        platform, //settings: Settings, 
        config, statusBar, splashScreen, alertCtrl, auth) {
        var _this = this;
        this.config = config;
        this.statusBar = statusBar;
        this.splashScreen = splashScreen;
        this.alertCtrl = alertCtrl;
        this.auth = auth;
        //rootPage = FirstRunPage;
        this.rootPage = __WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */];
        platform.ready().then(function () {
            // Okay, so the platform is ready and our plugins are available.
            // Here you can do any higher level native things you might need.
            _this.statusBar.styleDefault();
            _this.splashScreen.hide();
            _this.initializeApp();
        });
        //this.initTranslate();
    }
    MyApp.prototype.initializeApp = function () {
        var _this = this;
        if (this.auth.isAuthenticated()) {
            this.auth.validateToken().then(function (retorno) {
                //this.rootPage = ListMasterPage;
                //this.user = this.userProvider.getCurrentUser();
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_6__pages_list_master_list_master__["a" /* ListMasterPage */]);
            }).catch(function (retorno) {
                _this.alertCtrl.create({
                    message: !retorno.data ? retorno : retorno.data.showMessage,
                    buttons: ['Ok']
                }).present();
                //this.rootPage = LoginPage;
                _this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */]);
            });
        }
        else {
            //this.rootPage = LoginPage;
            this.nav.setRoot(__WEBPACK_IMPORTED_MODULE_7__pages_login_login__["a" /* LoginPage */]);
        }
    };
    /*
    initTranslate() {
      // Set the default language for translation strings, and the current language.
      this.translate.setDefaultLang('en');
      const browserLang = this.translate.getBrowserLang();
  
      if (browserLang) {
        if (browserLang === 'zh') {
          const browserCultureLang = this.translate.getBrowserCultureLang();
  
          if (browserCultureLang.match(/-CN|CHS|Hans/i)) {
            this.translate.use('zh-cmn-Hans');
          } else if (browserCultureLang.match(/-TW|CHT|Hant/i)) {
            this.translate.use('zh-cmn-Hant');
          }
        } else {
          this.translate.use(this.translate.getBrowserLang());
        }
      } else {
        this.translate.use('en'); // Set your language here
      }
  
      this.translate.get(['BACK_BUTTON_TEXT']).subscribe(values => {
        this.config.set('ios', 'backButtonText', values.BACK_BUTTON_TEXT);
      });
    }*/
    MyApp.prototype.openPage = function (page) {
        // Reset the content nav to have just this page
        // we wouldn't want the back button to show in this scenario
        this.nav.setRoot(page.component);
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])(__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* Nav */]),
        __metadata("design:type", __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["i" /* Nav */])
    ], MyApp.prototype, "nav", void 0);
    MyApp = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            template: "<ion-menu [content]=\"content\">\n    <ion-header>\n      <ion-toolbar>\n        <ion-title>Pages</ion-title>\n      </ion-toolbar>\n    </ion-header>\n\n    <ion-content>\n      <ion-list>\n        <button menuClose ion-item *ngFor=\"let p of pages\" (click)=\"openPage(p)\">\n          {{p.title}}\n        </button>\n      </ion-list>\n    </ion-content>\n\n  </ion-menu>\n  <ion-nav #content [root]=\"rootPage\"></ion-nav>"
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["l" /* Platform */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["b" /* Config */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_status_bar__["a" /* StatusBar */],
            __WEBPACK_IMPORTED_MODULE_1__ionic_native_splash_screen__["a" /* SplashScreen */], __WEBPACK_IMPORTED_MODULE_5_ionic_angular_components_alert_alert_controller__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */]])
    ], MyApp);
    return MyApp;
}());

//# sourceMappingURL=app.component.js.map

/***/ }),

/***/ 85:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return AuthProvider; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__base__ = __webpack_require__(243);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(77);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_uuid__ = __webpack_require__(684);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_angular2_uuid___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_angular2_uuid__);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthProvider = /** @class */ (function (_super) {
    __extends(AuthProvider, _super);
    function AuthProvider(http) {
        var _this = _super.call(this) || this;
        _this.http = http;
        return _this;
    }
    AuthProvider.prototype.hasPermission = function (rolesGranted) {
        var authorized = false;
        if (localStorage.getItem("token") && localStorage.getItem("token") != "{}") {
            var dataToken = JSON.parse(localStorage.getItem("token")).data;
            if (dataToken && dataToken.authorities) {
                var userAuthorities = dataToken.authorities.map(function (authority) { return authority = authority.authority; });
                var i;
                for (i = 0; i < rolesGranted.length; i++) {
                    authorized = (userAuthorities.indexOf(rolesGranted[i]) >= 0);
                    if (authorized)
                        return authorized;
                }
            }
        }
        return authorized;
    };
    AuthProvider.prototype.login = function (username, password, type, device) {
        var _this = this;
        password = this.encode(password);
        return new Promise(function (resolve, reject) {
            var req = new XMLHttpRequest();
            req.onloadend = function () {
                if (req.status !== 200) {
                    resolve(false);
                }
                else {
                    var r = JSON.parse(req.responseText);
                    if (r.success) {
                        localStorage.setItem("token", req.responseText);
                        resolve(r);
                    }
                    else {
                        reject(r);
                    }
                }
            };
            var bodyPart = {
                "username": username,
                "password": password,
                "typeAuthentication": type,
                "device": device
            };
            req.ontimeout = function (e) {
                // Timeout na chamada XMLHttpRequest. Ação de timeout aqui.
                reject('Servidor Fora: TimeOut');
            };
            req.open("POST", _this.api + '/login/authenticate', true);
            req.timeout = 2000; // tempo em milisegundos
            req.setRequestHeader('Content-Type', 'application/json');
            req.send(JSON.stringify(bodyPart));
        });
    };
    AuthProvider.prototype.logout = function () {
        localStorage.clear();
    };
    AuthProvider.prototype.changePassword = function (email, cpf, oldPassword, newPassword, confirmNewPassword) {
        var _this = this;
        oldPassword = this.encode(oldPassword);
        newPassword = this.encode(newPassword);
        confirmNewPassword = this.encode(confirmNewPassword);
        return new Promise(function (resolve, reject) {
            var req = new XMLHttpRequest();
            req.onloadend = function () {
                if (req.status !== 200) {
                    resolve(false);
                }
                else {
                    var r = JSON.parse(req.responseText);
                    if (r.success) {
                        localStorage.setItem("token", req.responseText);
                        resolve(r);
                    }
                    else {
                        reject(r);
                    }
                }
            };
            var bodyPart = {
                "email": email,
                "cpf": cpf,
                "oldPassword": oldPassword,
                "newPassword": newPassword,
                "confirmNewPassword": confirmNewPassword
            };
            req.open("POST", _this.api + '/login/change/password', true);
            req.setRequestHeader('Content-Type', 'application/json');
            req.send(JSON.stringify(bodyPart));
        });
    };
    AuthProvider.prototype.forgotPassword = function (email, cpf) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var req = new XMLHttpRequest();
            req.onloadend = function () {
                if (req.status !== 200) {
                    resolve(false);
                }
                else {
                    var r = JSON.parse(req.responseText);
                    if (r.success) {
                        resolve(r);
                    }
                    else {
                        reject(r);
                    }
                }
            };
            var bodyPart = {
                "email": email,
                "cpf": cpf,
                "emailSubject": "[ASBAPI] - Alteração de Senha de Usuário"
            };
            req.open("POST", _this.api + '/login/forgot/password', true);
            req.setRequestHeader('Content-Type', 'application/json');
            req.send(JSON.stringify(bodyPart));
        });
    };
    AuthProvider.prototype.validateToken = function () {
        var _this = this;
        var tokenStr = localStorage.getItem("token");
        if (tokenStr) {
            var data = JSON.parse(tokenStr);
            if (data.data) {
                tokenStr = data.data.token;
            }
        }
        return new Promise(function (resolve, reject) {
            var req = new XMLHttpRequest();
            req.onloadend = function () {
                if (req.status !== 200) {
                    resolve(false);
                }
                else {
                    var r = JSON.parse(req.responseText);
                    if (r.success) {
                        //localStorage.setItem("token", req.responseText);
                        resolve(r);
                    }
                    else {
                        reject(r);
                    }
                }
            };
            var bodyPart = {
                "authToken": tokenStr
            };
            req.ontimeout = function (e) {
                // Timeout na chamada XMLHttpRequest. Ação de timeout aqui.
                localStorage.clear();
                reject('Servidor Fora: TimeOut');
            };
            req.open("POST", _this.api + '/login/validateToken', true);
            req.timeout = 2000; // tempo em milisegundos
            req.setRequestHeader('Content-Type', 'application/json');
            req.send(tokenStr);
        });
    };
    AuthProvider.prototype.isAuthenticated = function () {
        var tokenStr = localStorage.getItem("token");
        if (tokenStr) {
            var token = JSON.parse(tokenStr);
            return (token.success);
        }
        else {
            return false;
        }
    };
    AuthProvider.prototype.getCurrentUser = function () {
        var user = {};
        var tokenStr = localStorage.getItem("token");
        if (tokenStr) {
            var obj = JSON.parse(tokenStr).data;
            user.id = obj.id;
            user.name = obj.name;
            user.cpf = obj.cpf;
            user.email = obj.email;
            user.celular = obj.celular;
            user.telephone = obj.telephone;
        }
        return user;
    };
    AuthProvider.prototype.encode = function (text) {
        var uuid = __WEBPACK_IMPORTED_MODULE_3_angular2_uuid__["UUID"].UUID();
        if (text.length > 0) {
            var j = uuid.length - text.length;
            while (j < 0) {
                j = j + uuid.length;
            }
            var result = uuid + "-";
            for (var i = 0; i < text.length; i++) {
                if (j > uuid.length) {
                    j = 0;
                }
                result = result + this.formatNumForCode(text.charCodeAt(i) + uuid.charCodeAt(j));
                j++;
            }
            return result;
        }
        else {
            return text;
        }
    };
    AuthProvider.prototype.formatNumForCode = function (num) {
        var text = "" + num;
        while (text.length < 3) {
            text = "0" + text;
        }
        return text;
    };
    AuthProvider = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2__angular_http__["b" /* Http */]])
    ], AuthProvider);
    return AuthProvider;
}(__WEBPACK_IMPORTED_MODULE_1__base__["a" /* BaseProvider */]));

//# sourceMappingURL=auth.js.map

/***/ }),

/***/ 90:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ListMasterPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__providers__ = __webpack_require__(160);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__base_page__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__ = __webpack_require__(85);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_ferida_ferida__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(31);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ListMasterPage = /** @class */ (function (_super) {
    __extends(ListMasterPage, _super);
    function ListMasterPage(navCtrl, items, auth, modalCtrl, alertCtrl, feridaProvider, sanitizer) {
        var _this = _super.call(this, alertCtrl, sanitizer) || this;
        _this.navCtrl = navCtrl;
        _this.items = items;
        _this.auth = auth;
        _this.modalCtrl = modalCtrl;
        _this.alertCtrl = alertCtrl;
        _this.feridaProvider = feridaProvider;
        _this.sanitizer = sanitizer;
        console.log('construtor ListMasterPage');
        //this.currentItems = this.items.query();
        _this.user = _this.auth.getCurrentUser();
        _this.pesquisarPacientes();
        return _this;
    }
    /**
     * The view loaded, let's query our items for the list
     */
    ListMasterPage.prototype.ionViewDidLoad = function () {
        console.log('ionViewDidLoad');
    };
    ListMasterPage.prototype.pesquisarPacientes = function () {
        var _this = this;
        console.log('pesquisarPacientes');
        this.feridaProvider.pesquisarPacientes().toPromise().then(function (data) {
            console.log('DATA pesquisarPacientes', data);
            _this.pacientes = data;
            _this.preencherArquivosPacientes();
        }).catch(function (e) {
            if (e) {
                _this.mostrarErro(_this.tratarMsg(e));
            }
            else {
                _this.mostrarErro('Erro não especificado.');
            }
        });
        /*this.feridaProvider.pesquisarPacientes().then((data: any) => {
          console.log('DATA pesquisarPacientes', data);
          this.pacientes = data;
          this.preencherArquivosPacientes();
        });*/
        console.log('FIM pesquisarPacientes');
    };
    ListMasterPage.prototype.preencherArquivosPacientes = function () {
        var _this = this;
        console.log('preencherArquivosPacientes');
        this.pacientes.forEach(function (p) {
            _this.feridaProvider.obterArquivo(p.ferida.arquivoOriginal.pathLogical).toPromise().then(function (response) {
                /*var c = response.blob();
                var t = response.text();
                var blob = new Blob([t], { type: 'image/jpg' });*/
                var blob = response.blob();
                p.ferida.fotoResource = blob;
            }).catch(function (e) {
                if (e) {
                    _this.mostrarErro(_this.tratarMsg(e));
                }
                else {
                    _this.mostrarErro('Erro não especificado ao obter imagem.');
                }
            });
            /*this.feridaProvider.obterArquivo(p.ferida.arquivoOriginal.pathLogical).then((blob: Blob) => {
              console.log('BLOB preencherArquivosPacientes', blob);
              p.ferida.fotoResource = blob;
            });*/
        });
        console.log('FIM preencherArquivosPacientes');
    };
    ListMasterPage.prototype.obterArquivo = function (path) {
        /*this.feridaProvider.obterArquivo(path).toPromise().then((response: any) => {
          var blob = new Blob([response], { type: 'image/jpg' });
          return this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(blob));
        }).catch((e) => {
          if (e) {
            this.mostrarErro(this.tratarMsg(e));
          } else {
            this.mostrarErro('Erro não especificado ao obter imagem.');
          }
        });*/
        console.log('obterArquivo', path);
        /*this.feridaProvider.obterArquivo(path).then((blob: Blob) => {
          console.log('SANITIZER obterArquivo', blob);
          return this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(blob));
          //console.log('feridaProvider', blob);
        });*/
        //return 'assets/img/speakers/bear.jpg';
        console.log('FIM obterArquivo');
    };
    /**
     * Prompt the user to add a new item. This shows our ItemCreatePage in a
     * modal and then adds the new item to our data source if the user created one.
     */
    ListMasterPage.prototype.addItem = function () {
        var _this = this;
        var addModal = this.modalCtrl.create('ItemCreatePage');
        addModal.onDidDismiss(function (item) {
            if (item) {
                _this.items.add(item);
            }
            console.log('ItemCreatePage closed');
        });
        addModal.present();
    };
    /**
     * Delete an item from the list of items.
     */
    ListMasterPage.prototype.deleteItem = function (item) {
        this.items.delete(item);
    };
    /**
     * Navigate to the detail page for this item.
     */
    ListMasterPage.prototype.openItem = function (item) {
        /*this.navCtrl.push('ItemDetailPage', {
          item: item
        });*/
        var _this = this;
        if (!item.ferida.arquivoClassificado || !item.ferida.arquivoClassificado.pathLogical) {
            this.mostrarErro('Não há nenhuma análise registrada para o arquivo selecionado!');
            return;
        }
        var addModal = this.modalCtrl.create('ItemClassificationPage', { parameter: item });
        addModal.onDidDismiss(function (item) {
            console.log('ItemClassificationPage closed');
            _this.pesquisarPacientes();
        });
        addModal.present();
    };
    ListMasterPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-list-master',template:/*ion-inline-start:"D:\MESTRADO\DISSERTACAO\PROJETO\FINAL\WORKSPACE\FONTES\APP\woundDetector\src\pages\list-master\list-master.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Olá {{user.name}}</ion-title>\n\n    <ion-buttons end>\n      <button ion-button icon-only (click)="addItem()">\n        <ion-icon name="add"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n<ion-content>\n\n  <!-- <ion-list>\n    <ion-item-sliding *ngFor="let item of currentItems">\n      <button ion-item (click)="openItem(item)">\n        <ion-avatar item-start>\n          <img [src]="item.profilePic" />\n        </ion-avatar>\n        <h2>{{item.name}}</h2>\n        <p>{{item.about}}</p>\n        <ion-note item-end *ngIf="item.note">{{item.note}}</ion-note>\n      </button>\n\n      <ion-item-options>\n        <button ion-button color="danger" (click)="deleteItem(item)">\n          {{ \'DELETE_BUTTON\' | translate }}\n        </button>\n      </ion-item-options>\n    </ion-item-sliding>\n  </ion-list> -->\n\n  <ion-list>\n      <ion-item-sliding *ngFor="let item of pacientes">\n        <button ion-item (click)="openItem(item)">\n          <!-- <ion-avatar item-start>\n            <img [src]="obterArquivo(item.ferida.arquivoOriginal.pathLogical)" />\n          </ion-avatar> -->\n          <ion-avatar item-start>\n            <img [src]="sanitizarArquivo(item.ferida.fotoResource)" />\n          </ion-avatar>\n          <h2>{{item.nome}}</h2>\n          <p>{{item.dtNascimento | date: \'dd/MM/yyyy\'}}</p>\n          <p>{{item.nuProntuario}}</p>\n          <p>{{item.ferida.arquivoOriginal.pathLogical}}</p>\n        </button>\n  \n        <ion-item-options>\n          <button ion-button color="danger" (click)="deleteItem(item)">\n            {{ \'DELETE_BUTTON\' | translate }}\n          </button>\n        </ion-item-options>\n      </ion-item-sliding>\n    </ion-list>\n\n</ion-content>'/*ion-inline-end:"D:\MESTRADO\DISSERTACAO\PROJETO\FINAL\WORKSPACE\FONTES\APP\woundDetector\src\pages\list-master\list-master.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2__providers__["b" /* Items */], __WEBPACK_IMPORTED_MODULE_4__providers_auth_auth__["a" /* AuthProvider */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["h" /* ModalController */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_5__providers_ferida_ferida__["a" /* FeridaProvider */], __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* DomSanitizer */]])
    ], ListMasterPage);
    return ListMasterPage;
}(__WEBPACK_IMPORTED_MODULE_3__base_page__["a" /* BasePage */]));

//# sourceMappingURL=list-master.js.map

/***/ })

},[374]);
//# sourceMappingURL=main.js.map