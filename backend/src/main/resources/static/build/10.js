webpackJsonp([10],{

/***/ 705:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemClassificationObservacaoPageModule", function() { return ItemClassificationObservacaoPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__item_classification_observacao__ = __webpack_require__(721);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ItemClassificationObservacaoPageModule = /** @class */ (function () {
    function ItemClassificationObservacaoPageModule() {
    }
    ItemClassificationObservacaoPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__item_classification_observacao__["a" /* Observacao */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__item_classification_observacao__["a" /* Observacao */]),
                __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__item_classification_observacao__["a" /* Observacao */]
            ]
        })
    ], ItemClassificationObservacaoPageModule);
    return ItemClassificationObservacaoPageModule;
}());

//# sourceMappingURL=item-classification-observacao.module.js.map

/***/ }),

/***/ 721:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return Observacao; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_ionic_angular__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__pages_base_page__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__ = __webpack_require__(31);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var Observacao = /** @class */ (function (_super) {
    __extends(Observacao, _super);
    function Observacao(params, viewCtrl, alertCtrl, sanitizer) {
        var _this = _super.call(this, alertCtrl, sanitizer) || this;
        _this.viewCtrl = viewCtrl;
        _this.alertCtrl = alertCtrl;
        _this.sanitizer = sanitizer;
        console.log('Observacao construtor');
        return _this;
    }
    Observacao.prototype.ionViewDidLoad = function () {
        console.log('Observacao ionViewDidLoad');
    };
    Observacao.prototype.ok = function () {
        if (!this.obs) {
            this.mostrarErro('Observação: Campo Obrigatório!');
            return;
        }
        var data = { rejeite: true, observacao: this.obs };
        this.viewCtrl.dismiss(data);
    };
    Observacao.prototype.dismiss = function () {
        var data = { rejeite: false, observacao: this.obs };
        this.viewCtrl.dismiss(data);
    };
    Observacao.prototype.mostrarErro = function (msg) {
        var alert = this.alertCtrl.create({
            title: 'Erro',
            message: msg,
            buttons: ['Ok'],
            cssClass: 'custom-alert'
        });
        alert.present();
    };
    Observacao = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-item-classification-observacao',template:/*ion-inline-start:"D:\MESTRADO\DISSERTACAO\PROJETO\FINAL\WORKSPACE\FONTES\APP\woundDetector\src\pages\item-classification-observacao\item-classification-observacao.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Rejeite</ion-title>\n    <ion-buttons start>\n      <button ion-button (click)="dismiss()">\n        <span color="primary">\n          Cancelar\n        </span>\n        <ion-icon name="md-close" showWhen="core,android,windows,ios"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons end>\n        <!-- <button ion-button (click)="done()" [disabled]="!isReadyToSave" strong> -->\n        <button ion-button (click)="ok()" strong>\n          <span color="primary">\n            Salvar\n          </span>\n        <ion-icon name="md-checkmark" showWhen="core,android,windows,ios"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <ion-grid>\n      <ion-row>\n        <ion-col col-12>\n          <h2>Observação</h2>\n          <textarea [(ngModel)]="obs"></textarea>\n        </ion-col>\n      </ion-row>\n  </ion-grid>\n</ion-content>'/*ion-inline-end:"D:\MESTRADO\DISSERTACAO\PROJETO\FINAL\WORKSPACE\FONTES\APP\woundDetector\src\pages\item-classification-observacao\item-classification-observacao.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_1_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_3__angular_platform_browser__["c" /* DomSanitizer */]])
    ], Observacao);
    return Observacao;
}(__WEBPACK_IMPORTED_MODULE_2__pages_base_page__["a" /* BasePage */]));

//# sourceMappingURL=item-classification-observacao.js.map

/***/ })

});
//# sourceMappingURL=10.js.map