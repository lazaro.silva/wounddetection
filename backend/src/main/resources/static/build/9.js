webpackJsonp([9],{

/***/ 707:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemCreatePageModule", function() { return ItemCreatePageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__item_create__ = __webpack_require__(725);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};




var ItemCreatePageModule = /** @class */ (function () {
    function ItemCreatePageModule() {
    }
    ItemCreatePageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__item_create__["a" /* ItemCreatePage */],
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__item_create__["a" /* ItemCreatePage */]),
                __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["b" /* TranslateModule */].forChild()
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__item_create__["a" /* ItemCreatePage */],
            ]
        })
    ], ItemCreatePageModule);
    return ItemCreatePageModule;
}());

//# sourceMappingURL=item-create.module.js.map

/***/ }),

/***/ 725:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemCreatePage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__ = __webpack_require__(373);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_ionic_angular__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pages_base_page__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__providers_ferida_ferida__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__ = __webpack_require__(31);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};








var ItemCreatePage = /** @class */ (function (_super) {
    __extends(ItemCreatePage, _super);
    function ItemCreatePage(navCtrl, viewCtrl, formBuilder, camera, alertCtrl, feridaProvider, sanitizer, modalCtrl) {
        var _this = _super.call(this, alertCtrl, sanitizer) || this;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.camera = camera;
        _this.alertCtrl = alertCtrl;
        _this.feridaProvider = feridaProvider;
        _this.sanitizer = sanitizer;
        _this.modalCtrl = modalCtrl;
        _this.paciente = {};
        _this.form = formBuilder.group({
            profilePic: [''],
            profilePhoto: [''],
            name: ['', __WEBPACK_IMPORTED_MODULE_1__angular_forms__["f" /* Validators */].required],
            about: ['']
        });
        // Watch the form for changes, and
        _this.form.valueChanges.subscribe(function (v) {
            _this.isReadyToSave = _this.form.valid;
        });
        return _this;
    }
    ItemCreatePage.prototype.ionViewDidLoad = function () {
    };
    ItemCreatePage.prototype.getPicture = function () {
        /*if (Camera['installed']()) {
          this.camera.getPicture({
            destinationType: this.camera.DestinationType.DATA_URL,
            targetWidth: 96,
            targetHeight: 96
          }).then((data) => {
            this.form.patchValue({ 'profilePic': 'data:image/jpg;base64,' + data });
          }, (err) => {
            alert('Unable to take photo');
          })
        } else {
          this.fileInput.nativeElement.click();
        }*/
        this.fileInput.nativeElement.click();
    };
    ItemCreatePage.prototype.getPhoto = function () {
        var _this = this;
        this.camera.getPicture({ quality: 100,
            destinationType: this.camera.DestinationType.FILE_URI,
            encodingType: this.camera.EncodingType.JPEG,
            mediaType: this.camera.MediaType.PICTURE }).then(function (imageData) {
            // imageData is either a base64 encoded string or a file URI
            // If it's base64 (DATA_URL):
            //let base64Image = 'data:image/jpeg;base64,' + imageData;
            _this.form.patchValue({ 'profilePhoto': 'data:image/jpg;base64,' + imageData });
        }, function (err) {
            // Handle error
            console.log('Erro ao tirar Foto: ' + err);
            _this.mostrarErro(err);
        });
    };
    ItemCreatePage.prototype.eraseImg = function (option) {
        if (option == 2) {
            this.form.patchValue({ 'profilePhoto': undefined });
        }
    };
    ItemCreatePage.prototype.processWebImage = function (event) {
        var _this = this;
        var reader = new FileReader();
        reader.onload = function (readerEvent) {
            var imageData = readerEvent.target.result;
            _this.form.patchValue({ 'profilePic': imageData });
        };
        reader.readAsDataURL(event.target.files[0]);
    };
    ItemCreatePage.prototype.getProfileImageStyle = function () {
        return 'url(' + this.form.controls['profilePic'].value + ')';
    };
    ItemCreatePage.prototype.getProfilePhotoImageStyle = function () {
        return 'url(' + this.form.controls['profilePhoto'].value + ')';
    };
    /**
     * The user cancelled, so we dismiss without sending data back.
     */
    ItemCreatePage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    /**
     * The user is done and wants to create the item, so return it
     * back to the presenter.
     */
    ItemCreatePage.prototype.done = function () {
        var _this = this;
        //if (!this.form.valid) { return; }
        var foto = this.form.controls['profilePic'].value;
        this.paciente.foto = foto;
        this.feridaProvider.salvarPaciente(this.paciente).toPromise().then(function (data) {
            _this.mostrarSucesso('Registro de Paciente Salvo com Sucesso!');
            _this.paciente = data;
            _this.openClassificacao();
            _this.viewCtrl.dismiss(_this.form.value);
        }).catch(function (e) {
            if (e) {
                _this.mostrarErro(_this.tratarMsg(e));
            }
            else {
                _this.mostrarErro('Erro não especificado.');
            }
        });
        /*this.feridaProvider.salvarPaciente(this.paciente).then((retorno: any) => {
          if(retorno) {
            this.viewCtrl.dismiss(this.form.value);
          }else {
            this.mostrarErro('Ocorreram erros ao salvar Formulário!');
          }
        }).catch((retorno) => {
          let msg = this.tratarMsg(retorno.data);
          this.mostrarErro(msg);
        });*/
    };
    ItemCreatePage.prototype.openClassificacao = function () {
        var addModal = this.modalCtrl.create('ItemClassificationPage', { parameter: this.paciente });
        addModal.onDidDismiss(function (item) {
            console.log('ItemClassificationPage closed');
        });
        addModal.present();
    };
    __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["ViewChild"])('fileInput'),
        __metadata("design:type", Object)
    ], ItemCreatePage.prototype, "fileInput", void 0);
    ItemCreatePage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-item-create',template:/*ion-inline-start:"D:\MESTRADO\DISSERTACAO\PROJETO\FINAL\WORKSPACE\FONTES\APP\woundDetector\src\pages\item-create\item-create.html"*/'<ion-header>\n\n  <ion-navbar>\n    <ion-title>Nova Imagem</ion-title>\n    <ion-buttons start>\n      <button ion-button (click)="cancel()">\n        <span color="primary">\n          Cancelar\n        </span>\n        <ion-icon name="md-close" showWhen="core,android,windows,ios"></ion-icon>\n      </button>\n    </ion-buttons>\n    <ion-buttons end>\n        <!-- <button ion-button (click)="done()" [disabled]="!isReadyToSave" strong> -->\n        <button ion-button (click)="done()" strong>\n          <span color="primary">\n            Salvar\n          </span>\n        <ion-icon name="md-checkmark" showWhen="core,android,windows,ios"></ion-icon>\n      </button>\n    </ion-buttons>\n  </ion-navbar>\n\n</ion-header>\n\n\n<ion-content>\n\n    <ion-grid>\n      <ion-row>\n        <ion-col col-1>\n          <label>Nome</label>\n          <input type="text" [(ngModel)]="paciente.nome" class="form-control">\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-1>\n          <label>Data de Nascimento</label>\n          <!--ion-datetime displayFormat="DD/MM/YYYY" class="form-control" [(ngModel)]="proposta.beneficiario.dtNascimento"></ion-datetime-->\n          <input id="date" type="date" date=\'dd/MM/yyyy\' class="form-control" [(ngModel)]="paciente.dtNascimento">\n          <!-- <input type="text" [(ngModel)]="proposta.beneficiario.dtNascimento" class="form-control"> -->\n        </ion-col>\n      </ion-row>\n      <ion-row>\n        <ion-col col-1>\n          <label>Nº do Prontuário</label>\n          <input type="text" [(ngModel)]="paciente.nuProntuario" class="form-control"> \n        </ion-col>\n      </ion-row>\n    </ion-grid>\n    \n  <form *ngIf="form" [formGroup]="form" (ngSubmit)="createItem()">  \n    <input type="file" #fileInput style="visibility: hidden; height: 0px" name="files[]" (change)="processWebImage($event)" />\n    <div class="profile-image-wrapper" (click)="getPicture()">\n      <div class="profile-image-placeholder" *ngIf="!this.form.controls.profilePic.value">\n        <ion-icon name="albums"></ion-icon>\n        <div>\n          Selecione uma imagem Local\n        </div>\n      </div>\n      <div class="profile-image" [style.backgroundImage]="getProfileImageStyle()" *ngIf="this.form.controls.profilePic.value"></div>\n    </div>\n\n    <div class="profile-image-wrapper" (click)="getPhoto()">\n      <div class="profile-image-placeholder" *ngIf="!this.form.controls.profilePhoto.value">\n        <ion-icon name="camera"></ion-icon>\n        <div>\n          Registre uma Foto\n        </div>\n      </div>\n      <div *ngIf="this.form.controls.profilePhoto.value">\n        <div class="profile-image" [style.backgroundImage]="getProfilePhotoImageStyle()"></div>\n        <ion-icon name="trash" (click)="eraseImg(2)"></ion-icon>\n      </div>\n    </div>\n\n    <!-- <ion-list>\n      <ion-item>\n        <ion-input type="text" placeholder="{{ \'ITEM_NAME_PLACEHOLDER\' | translate }}" formControlName="name"></ion-input>\n      </ion-item>\n      <ion-item>\n        <ion-input type="text" placeholder="{{ \'ITEM_ABOUT_PLACEHOLDER\' | translate }}" formControlName="about"></ion-input>\n      </ion-item>\n    </ion-list> -->\n\n  </form>\n\n</ion-content>'/*ion-inline-end:"D:\MESTRADO\DISSERTACAO\PROJETO\FINAL\WORKSPACE\FONTES\APP\woundDetector\src\pages\item-create\item-create.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_3_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2__ionic_native_camera__["a" /* Camera */],
            __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["a" /* AlertController */], __WEBPACK_IMPORTED_MODULE_5__providers_ferida_ferida__["a" /* FeridaProvider */],
            __WEBPACK_IMPORTED_MODULE_6__angular_platform_browser__["c" /* DomSanitizer */], __WEBPACK_IMPORTED_MODULE_3_ionic_angular__["h" /* ModalController */]])
    ], ItemCreatePage);
    return ItemCreatePage;
}(__WEBPACK_IMPORTED_MODULE_4__pages_base_page__["a" /* BasePage */]));

//# sourceMappingURL=item-create.js.map

/***/ })

});
//# sourceMappingURL=9.js.map