webpackJsonp([0],{

/***/ 706:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ItemClassificationPageModule", function() { return ItemClassificationPageModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__ = __webpack_require__(371);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__item_classification__ = __webpack_require__(722);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__pipes_pipes_module__ = __webpack_require__(723);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};





var ItemClassificationPageModule = /** @class */ (function () {
    function ItemClassificationPageModule() {
    }
    ItemClassificationPageModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_3__item_classification__["a" /* ItemClassificationPage */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["f" /* IonicPageModule */].forChild(__WEBPACK_IMPORTED_MODULE_3__item_classification__["a" /* ItemClassificationPage */]),
                __WEBPACK_IMPORTED_MODULE_1__ngx_translate_core__["b" /* TranslateModule */].forChild(),
                __WEBPACK_IMPORTED_MODULE_4__pipes_pipes_module__["a" /* PipesModule */]
            ],
            exports: [
                __WEBPACK_IMPORTED_MODULE_3__item_classification__["a" /* ItemClassificationPage */]
            ]
        })
    ], ItemClassificationPageModule);
    return ItemClassificationPageModule;
}());

//# sourceMappingURL=item-classification.module.js.map

/***/ }),

/***/ 722:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return ItemClassificationPage; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_forms__ = __webpack_require__(25);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_ionic_angular__ = __webpack_require__(63);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__pages_base_page__ = __webpack_require__(372);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__providers_ferida_ferida__ = __webpack_require__(161);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__ = __webpack_require__(31);
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};







var ItemClassificationPage = /** @class */ (function (_super) {
    __extends(ItemClassificationPage, _super);
    function ItemClassificationPage(params, navCtrl, viewCtrl, formBuilder, alertCtrl, feridaProvider, modalCtrl, sanitizer) {
        var _this = _super.call(this, alertCtrl, sanitizer) || this;
        _this.navCtrl = navCtrl;
        _this.viewCtrl = viewCtrl;
        _this.alertCtrl = alertCtrl;
        _this.feridaProvider = feridaProvider;
        _this.modalCtrl = modalCtrl;
        _this.sanitizer = sanitizer;
        console.log('ItemClassificationPage construtor');
        _this.paciente = params.get('parameter');
        _this.obterArquivoClassificado();
        // Watch the form for changes, and
        /*this.form.valueChanges.subscribe((v) => {
          this.isReadyToSave = this.form.valid;
        });*/
        console.log('FIM ItemClassificationPage construtor');
        return _this;
    }
    ItemClassificationPage.prototype.ionViewDidLoad = function () {
        console.log('ItemClassificationPage ionViewDidLoad');
    };
    /**
     * The user cancelled, so we dismiss without sending data back.
     */
    ItemClassificationPage.prototype.cancel = function () {
        this.viewCtrl.dismiss();
    };
    /**
     * The user is done and wants to create the item, so return it
     * back to the presenter.
     */
    ItemClassificationPage.prototype.done = function () {
        var _this = this;
        //if (!this.form.valid) { return; }
        var foto = this.form.controls['profilePic'].value;
        this.paciente.foto = foto;
        this.feridaProvider.salvarPaciente(this.paciente).toPromise().then(function (data) {
            _this.mostrarSucesso('Registro de Paciente Salvo com Sucesso!');
            _this.viewCtrl.dismiss(_this.form.value);
        }).catch(function (e) {
            if (e) {
                _this.mostrarErro(_this.tratarMsg(e));
            }
            else {
                _this.mostrarErro('Erro não especificado.');
            }
        });
    };
    ItemClassificationPage.prototype.obterArquivoClassificado = function () {
        var _this = this;
        console.log('obterArquivoClassificado');
        /*if(!this.paciente.ferida.arquivoClassificado || !this.paciente.ferida.arquivoClassificado.pathLogical) {
          this.mostrarErro('Não há nenhuma análise registrada para o arquivo selecionado!');
          this.viewCtrl.dismiss();
          return;
        }*/
        this.feridaProvider.obterArquivo(this.paciente.ferida.arquivoClassificado.pathLogical).toPromise().then(function (response) {
            var blob = response.blob();
            console.log('BLOB obterArquivoClassificado', blob);
            _this.paciente.ferida.fotoClassificacao = blob;
        }).catch(function (e) {
            if (e) {
                _this.mostrarErro(_this.tratarMsg(e));
            }
            else {
                _this.mostrarErro('Erro não especificado ao obter imagem.');
            }
        });
        console.log('FIM preencherArquivosPacientes');
    };
    ItemClassificationPage.prototype.getNomeCobertura = function (cobertura) {
        var _this = this;
        console.log('getNomeCobertura');
        this.feridaProvider.getNomeCobertura(cobertura.coberturaClassificada.label).toPromise().then(function (response) {
            console.log('STRING getNomeCobertura', response);
            _this.paciente.ferida.fotoClassificacao = response;
        }).catch(function (e) {
            if (e) {
                _this.mostrarErro(_this.tratarMsg(e));
            }
            else {
                _this.mostrarErro('Erro não especificado ao obter imagem.');
            }
        });
        console.log('FIM getNomeCobertura');
    };
    ItemClassificationPage.prototype.selectCobertura = function (item, coberturas) {
        coberturas.forEach(function (cob) {
            if (cob.id === item.id) {
                cob.isAceito = true;
            }
            else {
                cob.isAceito = false;
            }
        });
    };
    ItemClassificationPage.prototype.rejeite = function () {
        var _this = this;
        var obsModal = this.modalCtrl.create('Observacao'); //this.modalCtrl.create(Observacao);
        obsModal.onDidDismiss(function (data) {
            console.log(data);
            if (data.rejeite) {
                _this.paciente.ferida.classificacao.observacoes = data.observacao;
                _this.fechar(false);
            }
        });
        obsModal.present();
    };
    ItemClassificationPage.prototype.fechar = function (flag) {
        var _this = this;
        console.log('fechar');
        if (flag && this.paciente.ferida.classificacao.tecidos) {
            var achou = false;
            var error = false;
            this.paciente.ferida.classificacao.tecidos.forEach(function (tec) {
                achou = false;
                tec.coberturasClassificadas.forEach(function (cob) {
                    if (cob.isAceito) {
                        achou = true;
                    }
                });
                if (!achou) {
                    error = true;
                }
            });
            if (error) {
                this.mostrarErro("Há Classificações sem aceite!");
                return;
            }
        }
        this.feridaProvider.fecharAnalise(this.paciente.ferida.classificacao).toPromise().then(function (response) {
            console.log('LONG fechar', response);
            if (response === 200) {
                _this.mostrarSucesso('Análise fechada com Sucesso!');
                _this.viewCtrl.dismiss();
            }
            else {
                _this.mostrarErro('Houveram erros durante o fechamento da Análise!');
                return;
            }
        }).catch(function (e) {
            if (e) {
                _this.mostrarErro(_this.tratarMsg(e));
            }
            else {
                _this.mostrarErro('Erro não especificado ao obter imagem.');
            }
        });
        console.log('FIM fechar');
    };
    ItemClassificationPage = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'page-item-classification',template:/*ion-inline-start:"D:\MESTRADO\DISSERTACAO\PROJETO\FINAL\WORKSPACE\FONTES\APP\woundDetector\src\pages\item-classification\item-classification.html"*/'<ion-header>\n  <ion-navbar>\n    <ion-title>{{ paciente.nome }}</ion-title>\n    <ion-buttons start>\n      <button ion-button (click)="cancel()">\n        <span color="primary">\n          Cancelar\n        </span>\n        <ion-icon name="md-close" showWhen="core,android,windows,ios"></ion-icon>\n      </button>\n    </ion-buttons>\n    \n  </ion-navbar>\n</ion-header>\n\n<ion-content>\n  <!-- <div class="item-profile" text-center #profilePic [style.background-image]="sanitizarArquivo(paciente.ferida.fotoClassificacao)">\n  </div> -->\n\n  <div class="item-profile" text-center #profilePic>\n    <img [src]="sanitizarArquivo(paciente.ferida.fotoClassificacao)" />\n  </div>\n\n  <ion-card *ngFor="let item of paciente.ferida.classificacao.tecidos | orderBy: \'ordem\'">\n      <ion-item>\n        <h2>{{item.tecido.label}}</h2>\n      </ion-item>\n    \n      <ion-card-content>\n          <ion-list radio-group>\n              <table>\n                <tr>\n                  <th class="checkbox"></th>\n                  <th>Cobertura</th>\n                </tr>\n                <tr *ngFor="let cobertura of item.coberturasClassificadas">\n                  <td>\n                      <ion-radio [checked]="cobertura.isAceito" (click)="selectCobertura(cobertura, item.coberturasClassificadas)" [value]="cobertura.id"></ion-radio>\n                  </td>\n                  <td>&nbsp;{{cobertura.coberturaClassificada.description}}</td>\n                </tr>\n              </table>\n          </ion-list>\n        <!-- <p>{{item.content}}</p> -->\n\n      </ion-card-content>\n  </ion-card>\n\n  <div *ngIf="paciente.ferida.classificacao.observacoes">\n    <ion-grid>\n      <ion-row>\n        <ion-col col-12>\n          <h2>Observação</h2>\n          <textarea disabled [(ngModel)]="paciente.ferida.classificacao.observacoes"></textarea>\n        </ion-col>\n      </ion-row>\n    </ion-grid>\n  </div>\n\n  <div class="item-detail" padding>\n      <button ion-button (click)="rejeite()">Rejeite</button>\n      <button ion-button (click)="fechar(true)">Fechar</button>\n  </div>\n</ion-content>'/*ion-inline-end:"D:\MESTRADO\DISSERTACAO\PROJETO\FINAL\WORKSPACE\FONTES\APP\woundDetector\src\pages\item-classification\item-classification.html"*/
        }),
        __metadata("design:paramtypes", [__WEBPACK_IMPORTED_MODULE_2_ionic_angular__["k" /* NavParams */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["j" /* NavController */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["n" /* ViewController */],
            __WEBPACK_IMPORTED_MODULE_1__angular_forms__["a" /* FormBuilder */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["a" /* AlertController */],
            __WEBPACK_IMPORTED_MODULE_4__providers_ferida_ferida__["a" /* FeridaProvider */], __WEBPACK_IMPORTED_MODULE_2_ionic_angular__["h" /* ModalController */],
            __WEBPACK_IMPORTED_MODULE_5__angular_platform_browser__["c" /* DomSanitizer */]])
    ], ItemClassificationPage);
    return ItemClassificationPage;
}(__WEBPACK_IMPORTED_MODULE_3__pages_base_page__["a" /* BasePage */]));

//# sourceMappingURL=item-classification.js.map

/***/ }),

/***/ 723:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return PipesModule; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__orderBy__ = __webpack_require__(724);
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};


var PipesModule = /** @class */ (function () {
    function PipesModule() {
    }
    PipesModule = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            declarations: [__WEBPACK_IMPORTED_MODULE_1__orderBy__["a" /* OrderBy */]],
            imports: [],
            exports: [__WEBPACK_IMPORTED_MODULE_1__orderBy__["a" /* OrderBy */]]
        })
    ], PipesModule);
    return PipesModule;
}());

//# sourceMappingURL=pipes.module.js.map

/***/ }),

/***/ 724:
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "a", function() { return OrderBy; });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/*
 * Example use
 *		Basic Array of single type: *ngFor="#todo of todoService.todos | orderBy : '-'"
 *		Multidimensional Array Sort on single column: *ngFor="#todo of todoService.todos | orderBy : ['-status']"
 *		Multidimensional Array Sort on multiple columns: *ngFor="#todo of todoService.todos | orderBy : ['status', '-title']"
 */
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};

var OrderBy = /** @class */ (function () {
    function OrderBy() {
    }
    OrderBy_1 = OrderBy;
    OrderBy._orderByComparator = function (a, b) {
        if (a && b) {
            if ((isNaN(parseFloat(a)) || !isFinite(a)) || (isNaN(parseFloat(b)) || !isFinite(b))) {
                //Isn't a number so lowercase the string to properly compare
                if (a.toLowerCase() < b.toLowerCase())
                    return -1;
                if (a.toLowerCase() > b.toLowerCase())
                    return 1;
            }
            else {
                //Parse strings as numbers to compare properly
                if (parseFloat(a) < parseFloat(b))
                    return -1;
                if (parseFloat(a) > parseFloat(b))
                    return 1;
            }
        }
        return 0; //equal each other
    };
    OrderBy.prototype.transform = function (input, _a) {
        var _b = _a[0], config = _b === void 0 ? '+' : _b;
        if (!Array.isArray(input))
            return input;
        if (!Array.isArray(config) || (Array.isArray(config) && config.length == 1)) {
            var propertyToCheck = !Array.isArray(config) ? config : config[0];
            var desc = propertyToCheck.substr(0, 1) == '-';
            //Basic array
            if (!propertyToCheck || propertyToCheck == '-' || propertyToCheck == '+') {
                return !desc ? input.sort() : input.sort().reverse();
            }
            else {
                var property = propertyToCheck.substr(0, 1) == '+' || propertyToCheck.substr(0, 1) == '-'
                    ? propertyToCheck.substr(1)
                    : propertyToCheck;
                return input.sort(function (a, b) {
                    return !desc
                        ? OrderBy_1._orderByComparator(a[property], b[property])
                        : -OrderBy_1._orderByComparator(a[property], b[property]);
                });
            }
        }
        else {
            //Loop over property of the array in order and sort
            return input.sort(function (a, b) {
                for (var i = 0; i < config.length; i++) {
                    var desc = config[i].substr(0, 1) == '-';
                    var property = config[i].substr(0, 1) == '+' || config[i].substr(0, 1) == '-'
                        ? config[i].substr(1)
                        : config[i];
                    var comparison = !desc
                        ? OrderBy_1._orderByComparator(a[property], b[property])
                        : -OrderBy_1._orderByComparator(a[property], b[property]);
                    //Don't return 0 yet in case of needing to sort by next property
                    if (comparison != 0)
                        return comparison;
                }
                return 0; //equal each other
            });
        }
    };
    OrderBy = OrderBy_1 = __decorate([
        Object(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Pipe"])({ name: 'orderBy', pure: false })
    ], OrderBy);
    return OrderBy;
    var OrderBy_1;
}());

//# sourceMappingURL=orderBy.js.map

/***/ })

});
//# sourceMappingURL=0.js.map