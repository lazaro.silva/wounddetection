-- INICIO INFRA-ESTRUTURA 

-- CREATE DATABASE;
CREATE DATABASE experimento_mestrado;

-- CREATE USER
CREATE USER 'mestrado'@'localhost' IDENTIFIED BY 'mestrado';

use experimento_mestrado;

SET SQL_SAFE_UPDATES = 0;
UPDATE mysql.user SET Grant_priv = 1, Super_priv = 1 WHERE user = 'mestrado';
FLUSH PRIVILEGES;

GRANT ALL ON experimento_mestrado.* TO 'mestrado'@'localhost';

set global innodb_large_prefix=on;

-- set global innodb_file_format=on;

set global innodb_large_prefix=on;

set global innodb_file_format=Barracuda;

set global innodb_file_per_table=true;

CREATE TABLE user (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null,
  fk_user_creation INT(12) null,
  name varchar(180) NOT NULL,
  cpf varchar(15) NOT NULL,
  dt_birth date NOT NULL,
  password varchar(300) NOT NULL,
  username varchar(60) NOT NULL,
  non_expired bit not null default 1,
  non_locked bit not null default 1,
  credential_non_expired bit not null default 1,
  enabled bit not null default 1,
  email varchar(200) null,
  telephone varchar(16) null,
  celular varchar(16) null,
  dt_remove timestamp null,
  is_super_adm bit not null default 0,
  PRIMARY KEY (id),
  CONSTRAINT fk_user FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT UNIQUE KEY uk_user (cpf)
);

CREATE TABLE file (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null,
  fk_user_creation INT(12) not null,
  name varchar(180) NOT NULL,
  name_entity varchar(180) NOT NULL,
  path_physical varchar(256) NOT NULL,
  path_logical varchar(180) NULL,
  content_type varchar(180) NULL,
  size INT(10) null,
  PRIMARY KEY (id),
  CONSTRAINT fk_user_file FOREIGN KEY (fk_user_creation) REFERENCES user (id)
);

alter table user add column fk_file int(12) null;
alter table user add CONSTRAINT fk_file_user FOREIGN KEY (fk_file) REFERENCES file (id);

CREATE TABLE user_access (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null,
  fk_user_creation INT(12) not null,
  type_device int(2) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_usacc_user FOREIGN KEY (fk_user_creation) REFERENCES user (id)
);

-- Regras
CREATE TABLE role (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null default now(),
  fk_user_creation INT(12) null,
  name varchar(180) NOT NULL,
  label varchar(180) null,
  PRIMARY KEY (id),
  CONSTRAINT fk_role_user FOREIGN KEY (fk_user_creation) REFERENCES user (id)
);

-- Funcionalidades
CREATE TABLE feature (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null default now(),
  fk_user_creation INT(12) null,
  name varchar(180) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_feat_user FOREIGN KEY (fk_user_creation) REFERENCES user (id)
);

-- Papeis
CREATE TABLE function (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null default now(),
  fk_user_creation INT(12) null,
  name varchar(180) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT UNIQUE KEY uk_function (name),
  CONSTRAINT fk_func_user FOREIGN KEY (fk_user_creation) REFERENCES user (id)
);

-- Agrupamento de Papeis
CREATE TABLE groupment (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null default now(),
  fk_user_creation INT(12) null,
  name varchar(180) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_grou_user FOREIGN KEY (fk_user_creation) REFERENCES user (id)
);

-- Associação entre o grupo e o papel
CREATE TABLE function_group (
  id INT(12) NOT NULL AUTO_INCREMENT,
  fk_groupment INT(12) NOT NULL,
  fk_function INT(12) NOT NULL,
  dt_creation timestamp not null default now(),
  fk_user_creation INT(12) null,
  PRIMARY KEY (id),
  UNIQUE KEY uk_grou_func (fk_groupment, fk_function),
  CONSTRAINT fk_fcgr_user FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_fcgr_grou FOREIGN KEY (fk_groupment) REFERENCES groupment (id),
  CONSTRAINT fk_fcgr_func FOREIGN KEY (fk_function) REFERENCES function (id)
);

-- Associação entre a funcionalidade e a regra
CREATE TABLE feature_role (
  id INT(12) NOT NULL AUTO_INCREMENT,
  fk_feature INT(12) NOT NULL,
  fk_role INT(12) NOT NULL,
  dt_creation timestamp not null default now(),
  fk_user_creation INT(12) null,
  PRIMARY KEY (id),
  UNIQUE KEY uk_feat_role (fk_feature, fk_role),
  CONSTRAINT fk_feto_user FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_feto_feat FOREIGN KEY (fk_feature) REFERENCES feature (id),
  CONSTRAINT fk_feto_role FOREIGN KEY (fk_role) REFERENCES role (id)
);

-- Associação entre o papel e a regra
CREATE TABLE function_feature (
  id INT(12) NOT NULL AUTO_INCREMENT,
  fk_function INT(12) NOT NULL,
  fk_feature INT(12) NULL,
  fk_role INT(12) NULL,
  dt_creation timestamp not null default now(),
  fk_user_creation INT(12) null,
  PRIMARY KEY (id),
  CONSTRAINT fk_fufe_user FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_fufe_func FOREIGN KEY (fk_function) REFERENCES function (id),
  CONSTRAINT fk_fufe_feat FOREIGN KEY (fk_feature) REFERENCES feature (id),
  CONSTRAINT fk_fufe_role FOREIGN KEY (fk_role) REFERENCES role (id)
);

CREATE TABLE user_function (
  id INT(12) NOT NULL AUTO_INCREMENT,
  fk_user INT(12) NOT NULL,
  fk_function INT(12) NOT NULL,
  dt_creation timestamp not null default now(),
  fk_user_creation INT(12) null,
  PRIMARY KEY (id),
  UNIQUE KEY uk_user_func (fk_user, fk_function),
  CONSTRAINT fk_usfc_uscr FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_usfc_user FOREIGN KEY (fk_user) REFERENCES user (id),
  CONSTRAINT fk_usfc_func FOREIGN KEY (fk_function) REFERENCES function (id)
);

CREATE TABLE user_role (
  id INT(12) NOT NULL AUTO_INCREMENT,
  fk_user INT(12) NOT NULL,
  fk_role INT(12) NOT NULL,
  dt_creation timestamp not null default now(),
  fk_user_creation INT(12) null,
  PRIMARY KEY (id),
  UNIQUE KEY uk_user_role (fk_user, fk_role),
  CONSTRAINT fk_usrl_uscr FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_usrl_user FOREIGN KEY (fk_user) REFERENCES user (id),
  CONSTRAINT fk_usrl_func FOREIGN KEY (fk_role) REFERENCES role (id)
);

CREATE TABLE user_group (
  id INT(12) NOT NULL AUTO_INCREMENT,
  fk_user INT(12) NOT NULL,
  fk_groupment INT(12) NOT NULL,
  dt_creation timestamp not null default now(),
  fk_user_creation INT(12) null,
  PRIMARY KEY (id),
  UNIQUE KEY uk_user_grou (fk_user, fk_groupment),
  CONSTRAINT fk_usgr_uscr FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_usgr_user FOREIGN KEY (fk_user) REFERENCES user (id),
  CONSTRAINT fk_usgr_grou FOREIGN KEY (fk_groupment) REFERENCES groupment (id)
);

-- Aplicações
CREATE TABLE application (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null default now(),
  fk_user_creation INT(12) null,
  host varchar(180) NOT NULL,
  name varchar(180) NOT NULL,
  password_service varchar(300) NOT NULL,
  active bit not null default true,
  flg_environment_pay_prod bit not null default false,
  merchant_order_id varchar(300) null,
  merchant_order_key varchar(300) null,
  payment_url_return varchar(300) null,
  paths_free varchar(1000) null,
  url_validation_pre_payment varchar(200) null,
  url_process_pos_payment varchar(200) null,
  PRIMARY KEY (id),
  CONSTRAINT fk_appl_user FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT UNIQUE KEY uk_nm_application (name)
);

-- Paths com Autorizacao Requerida
CREATE TABLE application_path (
  id INT(12) NOT NULL AUTO_INCREMENT,
  fk_application INT(12) not null,
  dt_creation timestamp not null default now(),
  fk_user_creation INT(12) null,
  description varchar(300) NULL,
  path varchar(220) NOT NULL,
  http_method int(2) NOT NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_appt_user FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_appt_appl FOREIGN KEY (fk_application) REFERENCES application (id),
  CONSTRAINT UNIQUE KEY uk_pt_application (path,http_method)
);

-- Autorizações para os Paths
CREATE TABLE path_role (
  id INT(12) NOT NULL AUTO_INCREMENT,
  fk_application_path INT(12) not null,
  fk_role INT(12) not null,
  dt_creation timestamp not null default now(),
  fk_user_creation INT(12) null,
  description varchar(300) NULL,
  PRIMARY KEY (id),
  CONSTRAINT fk_ptro_user FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_ptro_appt FOREIGN KEY (fk_application_path) REFERENCES application_path (id),
  CONSTRAINT fk_ptro_role FOREIGN KEY (fk_role) REFERENCES role (id)
);

alter table path_role ROW_FORMAT=DYNAMIC;
alter table path_role add CONSTRAINT UNIQUE KEY uk_apro_path_role (fk_application_path, fk_role);

CREATE TABLE user_profile (
  fk_user INT(12) NOT NULL,
  user_id varchar(80) NOT NULL,
  first_name varchar(80) NULL,
  last_name varchar(80) NULL,
  image_url varchar(80) NULL,
  PRIMARY KEY (fk_user),
  CONSTRAINT fk_user_uspr FOREIGN KEY (fk_user) REFERENCES user (id)
);

create table UserConnection (
  userId varchar(255) not null,
  providerId varchar(255) not null,
  providerUserId varchar(255),
  rank int not null,
  displayName varchar(255),
  profileUrl varchar(512),
  imageUrl varchar(512),
  accessToken varchar(1024) not null,
  secret varchar(255),
  refreshToken varchar(255),
  expireTime bigint);
  
alter table UserConnection ROW_FORMAT=DYNAMIC;
alter table UserConnection add primary key (userId, providerId, providerUserId);
create unique index UserConnectionRank on UserConnection(userId, providerId, rank);

create table UserProfile (
  userId varchar(255) not null,
  email varchar(255),
  firstName varchar(255),
  lastName varchar(255),
  name  varchar(255),
  username varchar(255));
  
alter table UserProfile ROW_FORMAT=DYNAMIC;

alter table UserProfile add primary key (userId);
create unique index UserProfilePK on UserProfile(userId);
 
alter table application add fk_background_image int(12) null;
alter table application add title_application varchar(120) null;

alter table user add UNIQUE KEY uk_user_username (username);

alter table role add description varchar(120) null;
alter table feature add description varchar(120) null;
alter table function add description varchar(120) null;

- Fila
CREATE TABLE queue (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null,
  fk_user_creation INT(12) not null,
  fk_application INT(12) null,
  type_queue int(2) NOT NULL,
  url varchar(150) NULL,
  parameters LONGTEXT NULL,
  priority int(1) NOT NULL default 2,
  historic bit not null default false,
  count int(1) NOT NULL default 0,
  executed bit not null default false,
  error varchar(10000) null,
  dt_execution timestamp null,
  dt_scheduled timestamp null,
  PRIMARY KEY (id),
  CONSTRAINT fk_user_queue FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_appc_queue FOREIGN KEY (fk_application) REFERENCES application (id)
);


-- Triger para validar a integridade dos dados a serem incluidos na tabela function_feature
DELIMITER $$
create trigger trg_user_function_ins before insert on function_feature 
	for each row
		begin
			declare count_func int(12);
			
			if NEW.fk_feature is not null and NEW.fk_role is not null then
                signal sqlstate '45000' set message_text = 'Não é permitido informar Funcionalidade e Regra no mesmo registro';
			end if;
				
			select count(id) into count_func from function_feature
			where fk_function = NEW.fk_function
			and fk_feature = NEW.fk_feature
			and fk_feature is not null;
			
			if count_func is not null and count_func > 0 then
                signal sqlstate '45001' set message_text = 'Já existe registro para o Papel e Funcionalidade Informados';
			end if;

			set count_func = null;
			
			select count(id) into count_func from function_feature
			where fk_function = NEW.fk_function
			and fk_role = NEW.fk_role
			and fk_role is not null;
			
			if count_func is not null and count_func > 0 then
                signal sqlstate '45002' set message_text = 'Já existe registro para o Papel e Regra Informados';
			end if;
		end;
$$

-- Triger para validar a integridade dos dados a serem alterados na tabela function_feature
DELIMITER $$
create trigger trg_user_function_upd before update on function_feature 
	for each row
		begin
			declare count_func int(12);
			
			if NEW.fk_feature is not null and NEW.fk_role is not null then
                signal sqlstate '45000' set message_text = 'Não é permitido informar Funcionalidade e Regra no mesmo registro';
			end if;
				
			select count(id) into count_func from function_feature
			where fk_function = NEW.fk_function
			and fk_feature = NEW.fk_feature
			and fk_feature is not null
            and id <> NEW.id;
			
			if count_func is not null and count_func > 0 then
                signal sqlstate '45001' set message_text = 'Já existe registro para o Papel e Funcionalidade Informados';
			end if;

			set count_func = null;
			
			select count(id) into count_func from function_feature
			where fk_function = NEW.fk_function
			and fk_role = NEW.fk_role
			and fk_role is not null
            and id <> NEW.id;
			
			if count_func is not null and count_func > 0 then
                signal sqlstate '45002' set message_text = 'Já existe registro para o Papel e Regra Informados';
			end if;
		end;
$$

-- Tabelas da aplicaçao
CREATE TABLE tecidos (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null,
  fk_user_creation INT(12) not null,
  nome varchar(220) NOT NULL,
  label varchar(220) NULL,
  fk_tecido_superior INT(12) null,
  PRIMARY KEY (id),
  CONSTRAINT fk_user_tecd FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_tecd_tecd FOREIGN KEY (fk_tecido_superior) REFERENCES tecidos (id)
);

CREATE TABLE classificacao (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null,
  fk_user_creation INT(12) not null,
  dt_updated timestamp null,
  fk_user_updated INT(12) null,
  -- id_cobertura_aceita INT(2) null,
  -- aceitacao bit null,
  observacoes varchar(2000) null,
  PRIMARY KEY (id),
  CONSTRAINT fk_user_clss_ins FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_user_clss_upd FOREIGN KEY (fk_user_updated) REFERENCES user (id)
);

CREATE TABLE classificacao_tecido (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null,
  fk_user_creation INT(12) not null,
  fk_classificacao INT(12) not null,
  fk_tecido INT(12) not null,
  nu_ferida INT(2) not null,
  
  nu_confidence decimal(18,15) null,
  
  coordenadas varchar(200) not null,
  PRIMARY KEY (id),
  CONSTRAINT fk_user_cltc FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_clss_cltc FOREIGN KEY (fk_classificacao) REFERENCES classificacao (id),
  CONSTRAINT fk_tecd_cltc FOREIGN KEY (fk_tecido) REFERENCES tecidos (id)
);

CREATE TABLE classificacao_cobertura (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null,
  fk_user_creation INT(12) not null,
  dt_updated timestamp null,
  fk_user_updated INT(12) null,
  fk_classificacao_tecido INT(12) not null,
  id_cobertura_classificada INT(2) not null,
  aceitacao bit null,
  PRIMARY KEY (id),
  CONSTRAINT fk_user_clcb_ins FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_user_clcb_upd FOREIGN KEY (fk_user_updated) REFERENCES user (id),
  CONSTRAINT fk_cltc_clcb FOREIGN KEY (fk_classificacao_tecido) REFERENCES classificacao_tecido (id)
);

CREATE TABLE ferida (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null,
  fk_user_creation INT(12) not null,
  dt_updated timestamp null,
  fk_user_updated INT(12) null,
  fk_file_original INT(12) not null,
  fk_file_detectada INT(12) null,
  fk_classificacao INT(12) null,
  PRIMARY KEY (id),
  CONSTRAINT fk_user_ferd_ins FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_user_ferd_upd FOREIGN KEY (fk_user_updated) REFERENCES user (id),
  CONSTRAINT fk_file_ferd_ori FOREIGN KEY (fk_file_original) REFERENCES file (id),
  CONSTRAINT fk_file_ferd_dtc FOREIGN KEY (fk_file_detectada) REFERENCES file (id),
  CONSTRAINT fk_clss_ferd FOREIGN KEY (fk_classificacao) REFERENCES classificacao (id)
);

CREATE TABLE paciente (
  id INT(12) NOT NULL AUTO_INCREMENT,
  dt_creation timestamp not null,
  fk_user_creation INT(12) not null,
  dt_updated timestamp null,
  fk_user_updated INT(12) null,
  nome varchar(220) NOT NULL,
  dt_nascimento timestamp not null,
  nu_prontuario varchar(200) NULL,
  fk_ferida INT(12) not null,
  PRIMARY KEY (id),
  CONSTRAINT fk_user_paci_ins FOREIGN KEY (fk_user_creation) REFERENCES user (id),
  CONSTRAINT fk_user_paci_upd FOREIGN KEY (fk_user_updated) REFERENCES user (id),
  CONSTRAINT fk_ferd_paci FOREIGN KEY (fk_ferida) REFERENCES ferida (id)
);

-- DADOS

-- Aplicação
insert into application
(id, host, name, password_service)
values
(1, 'http://localhost:9518/survey', 'SURVEY', '69KDKAM5nckshp90j03nINUU8ncisUhgvnskLLKK82040smUU9sSSaI290kcsjU1');

-- Function
insert into function
(id, name, description)
values
(1, 'ADMINISTRATOR', 'Super Papel dos Administradores do Sistema');

-- Usuário
insert into user
(id, dt_creation, fk_user_creation,	name, cpf,	dt_birth, password,	username, non_expired, non_locked,	credential_non_expired,	enabled, email,	dt_remove, is_super_adm, fk_file)
values
(1, curdate(), null, 'Administrador da Ferramenta', '13005553787', STR_TO_DATE('10/31/2001', '%m/%d/%Y'), '484b619f23f7d4b141c27a494ee02497a95f945232b5ec397e91a641d82fc085', '13005553787',	1,	1,	1,	1, 'teste@gmail.com', null,	1, null);

update user
set email='lazarogilvan@yahoo.com.br',
username='lazarogilvan@yahoo.com.br'
where id=1;

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(1, curdate(), 1, 'Ferida', null, null);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(2, curdate(), 1, 'Viáveis', null, 1);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(3, curdate(), 1, 'Inviáveis', null, 1);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(4, curdate(), 1, 'Epitelial', 'Viavel_Epitelizacao', 2);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(5, curdate(), 1, 'Granulação', 'Viavel_Granulacao_SemExsudato', 2);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(6, curdate(), 1, 'Seroso', 'Viavel_Granulacao_Seroso', 5);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(7, curdate(), 1, 'Hemático', 'Viavel_Granulacao_Hematico', 5);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(8, curdate(), 1, 'Demais', 'Viavel_Granulacao_Demais', 5);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(9, curdate(), 1, 'Necrose Seca', 'Inviavel_NecroseSeca', 3);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(10, curdate(), 1, 'Necrose Úmida', null, 3);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(11, curdate(), 1, 'Seroso', 'Inviavel_NecroseUmida_Seroso', 10);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(12, curdate(), 1, 'Hemático', 'Inviavel_NecroseUmida_Hematico', 10);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(13, curdate(), 1, 'Demais', 'Inviavel_NecroseUmida_Demais', 10);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(14, curdate(), 1, 'Esfacelos', null, 3);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(15, curdate(), 1, 'Seroso', 'Inviavel_Esfacelos_Seroso', 14);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(16, curdate(), 1, 'Hemático', 'Inviavel_Esfacelos_Hematico', 14);

insert into tecidos
(id, dt_creation, fk_user_creation, nome, label, fk_tecido_superior)
values
(17, curdate(), 1, 'Hemático', 'Inviavel_Esfacelos_Demais', 14);

