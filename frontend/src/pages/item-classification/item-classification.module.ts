import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { ItemClassificationPage } from './item-classification';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    ItemClassificationPage
  ],
  imports: [
    IonicPageModule.forChild(ItemClassificationPage),
    TranslateModule.forChild(),
    PipesModule
  ],
  exports: [
    ItemClassificationPage
  ]
})
export class ItemClassificationPageModule { }
