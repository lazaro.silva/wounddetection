import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, ViewController, NavParams, ModalController } from 'ionic-angular';
import { PacienteModel } from "../../models/paciente.model";
import { ClassificacaoCoberturaModel } from "../../models/classificacao.cobertura.model";
import { BasePage } from "../../pages/base-page";
import { AlertController } from 'ionic-angular';
import { FeridaProvider } from "../../providers/ferida/ferida";
import { DomSanitizer } from '@angular/platform-browser';
import { observeOn } from 'rxjs/operator/observeOn';

@IonicPage()
@Component({
  selector: 'page-item-classification',
  templateUrl: 'item-classification.html'
})
export class ItemClassificationPage extends BasePage {

  isReadyToSave: boolean;
  form: FormGroup;
  paciente: PacienteModel;

  constructor(params: NavParams, public navCtrl: NavController, public viewCtrl: ViewController, 
              formBuilder: FormBuilder, public alertCtrl: AlertController, 
              private feridaProvider:FeridaProvider, public modalCtrl: ModalController,
              public sanitizer: DomSanitizer) {
    super(alertCtrl, sanitizer);

    console.log('ItemClassificationPage construtor');

    this.paciente = params.get('parameter');
    this.obterArquivoClassificado();
    // Watch the form for changes, and
    /*this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });*/

    console.log('FIM ItemClassificationPage construtor');
  }

  ionViewDidLoad() {
    console.log('ItemClassificationPage ionViewDidLoad');
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    //if (!this.form.valid) { return; }
    let foto = this.form.controls['profilePic'].value;
    this.paciente.foto = foto;

    this.feridaProvider.salvarPaciente(this.paciente).toPromise().then((data) => {
      this.mostrarSucesso('Registro de Paciente Salvo com Sucesso!');
      this.viewCtrl.dismiss(this.form.value);
    }).catch((e) => {
      if (e) {
        this.mostrarErro(this.tratarMsg(e));
      } else {
        this.mostrarErro('Erro não especificado.');
      }
    });
  }

  obterArquivoClassificado() {
    console.log('obterArquivoClassificado');
    /*if(!this.paciente.ferida.arquivoClassificado || !this.paciente.ferida.arquivoClassificado.pathLogical) {
      this.mostrarErro('Não há nenhuma análise registrada para o arquivo selecionado!');
      this.viewCtrl.dismiss();
      return;
    }*/

    this.feridaProvider.obterArquivo(this.paciente.ferida.arquivoClassificado.pathLogical).toPromise().then((response: any) => {
      var blob = response.blob();
      console.log('BLOB obterArquivoClassificado', blob);
      this.paciente.ferida.fotoClassificacao = blob;
    }).catch((e) => {
      if (e) {
        this.mostrarErro(this.tratarMsg(e));
      } else {
        this.mostrarErro('Erro não especificado ao obter imagem.');
      }
    });
    console.log('FIM preencherArquivosPacientes');
  }

  getNomeCobertura(cobertura:ClassificacaoCoberturaModel) {
    console.log('getNomeCobertura');
    this.feridaProvider.getNomeCobertura(cobertura.coberturaClassificada.label).toPromise().then((response: any) => {
      console.log('STRING getNomeCobertura', response);
      this.paciente.ferida.fotoClassificacao = response;
    }).catch((e) => {
      if (e) {
        this.mostrarErro(this.tratarMsg(e));
      } else {
        this.mostrarErro('Erro não especificado ao obter imagem.');
      }
    });
    console.log('FIM getNomeCobertura');
  }

  selectCobertura(item:ClassificacaoCoberturaModel, coberturas:ClassificacaoCoberturaModel[]) {
    coberturas.forEach(cob => {
      if(cob.id === item.id) {
        cob.isAceito = true;
      }
      else {
        cob.isAceito = false;
      }
    });
  }

  rejeite() {
    let obsModal = this.modalCtrl.create('Observacao'); //this.modalCtrl.create(Observacao);
    obsModal.onDidDismiss(data => {
      console.log(data);
      if(data.rejeite) {
        this.paciente.ferida.classificacao.observacoes = data.observacao;
        this.fechar(false);
      }
    });
    obsModal.present();
  }

  fechar(flag:boolean) {

    console.log('fechar');
    
    if(flag && this.paciente.ferida.classificacao.tecidos) {
      var achou = false;
      var error = false;
      this.paciente.ferida.classificacao.tecidos.forEach(tec => {
        achou = false;
        tec.coberturasClassificadas.forEach(cob => {
          if(cob.isAceito) {
            achou = true;
          }
        });
        if(!achou) {
          error = true;
        }
      });

      if(error) {
        this.mostrarErro("Há Classificações sem aceite!");
        return;
      }
    }
    
    this.feridaProvider.fecharAnalise(this.paciente.ferida.classificacao).toPromise().then((response: any) => {
      console.log('LONG fechar', response);
      if(response===200) {
        this.mostrarSucesso('Análise fechada com Sucesso!');
        this.viewCtrl.dismiss();
      }
      else {
        this.mostrarErro('Houveram erros durante o fechamento da Análise!');
        return;
      }
    }).catch((e) => {
      if (e) {
        this.mostrarErro(this.tratarMsg(e));
      } else {
        this.mostrarErro('Erro não especificado ao obter imagem.');
      }
    });
    console.log('FIM fechar');
  }

}
