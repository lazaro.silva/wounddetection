import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, AlertController } from 'ionic-angular';
import { Item } from '../../models/item';
import { Items } from '../../providers';
import { BasePage } from '../base-page';
import { AuthProvider } from '../../providers/auth/auth';
import { FeridaProvider } from '../../providers/ferida/ferida';
import { Usuario } from '../../models/usuario.model';
import { PacienteModel } from '../../models/paciente.model';
import { DomSanitizer } from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-list-master',
  templateUrl: 'list-master.html'
})
export class ListMasterPage extends BasePage {
  currentItems: Item[];
  user: Usuario;
  pacientes: PacienteModel[];

  constructor(public navCtrl: NavController, public items: Items, private auth: AuthProvider,
              public modalCtrl: ModalController, public alertCtrl: AlertController,
              private feridaProvider:FeridaProvider, public sanitizer: DomSanitizer) {
    
    super(alertCtrl, sanitizer);
    console.log('construtor ListMasterPage');
    //this.currentItems = this.items.query();
    this.user = this.auth.getCurrentUser();
    this.pesquisarPacientes();
  }

  /**
   * The view loaded, let's query our items for the list
   */
  ionViewDidLoad() {
    console.log('ionViewDidLoad');
  }

  pesquisarPacientes() {
    console.log('pesquisarPacientes');

    this.feridaProvider.pesquisarPacientes().toPromise().then((data) => {
      console.log('DATA pesquisarPacientes', data);
      this.pacientes = data;
      this.preencherArquivosPacientes();
    }).catch((e) => {
      if (e) {
        this.mostrarErro(this.tratarMsg(e));
      } else {
        this.mostrarErro('Erro não especificado.');
      }
    });

    /*this.feridaProvider.pesquisarPacientes().then((data: any) => {
      console.log('DATA pesquisarPacientes', data);
      this.pacientes = data;
      this.preencherArquivosPacientes();
    });*/

    console.log('FIM pesquisarPacientes');
  }

  preencherArquivosPacientes() {
    console.log('preencherArquivosPacientes');
    this.pacientes.forEach(p => {
      this.feridaProvider.obterArquivo(p.ferida.arquivoOriginal.pathLogical).toPromise().then((response: any) => {
        /*var c = response.blob();
        var t = response.text();
        var blob = new Blob([t], { type: 'image/jpg' });*/
        var blob = response.blob();
        p.ferida.fotoResource = blob;
      }).catch((e) => {
        if (e) {
          this.mostrarErro(this.tratarMsg(e));
        } else {
          this.mostrarErro('Erro não especificado ao obter imagem.');
        }
      });

      /*this.feridaProvider.obterArquivo(p.ferida.arquivoOriginal.pathLogical).then((blob: Blob) => {
        console.log('BLOB preencherArquivosPacientes', blob);
        p.ferida.fotoResource = blob;
      });*/
    });
    console.log('FIM preencherArquivosPacientes');
  }

  obterArquivo(path:string) {
    /*this.feridaProvider.obterArquivo(path).toPromise().then((response: any) => {
      var blob = new Blob([response], { type: 'image/jpg' });
      return this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(blob));
    }).catch((e) => {
      if (e) {
        this.mostrarErro(this.tratarMsg(e));
      } else {
        this.mostrarErro('Erro não especificado ao obter imagem.');
      }
    });*/

    console.log('obterArquivo', path);
    /*this.feridaProvider.obterArquivo(path).then((blob: Blob) => {
      console.log('SANITIZER obterArquivo', blob);
      return this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(blob));
      //console.log('feridaProvider', blob);
    });*/
    //return 'assets/img/speakers/bear.jpg';
    console.log('FIM obterArquivo');
  }

  /**
   * Prompt the user to add a new item. This shows our ItemCreatePage in a
   * modal and then adds the new item to our data source if the user created one.
   */
  addItem() {
    let addModal = this.modalCtrl.create('ItemCreatePage');
    addModal.onDidDismiss(item => {
      if (item) {
        this.items.add(item);
      }
      console.log('ItemCreatePage closed');
    })
    addModal.present();
  }

  /**
   * Delete an item from the list of items.
   */
  deleteItem(item) {
    this.items.delete(item);
  }

  /**
   * Navigate to the detail page for this item.
   */
  openItem(item: PacienteModel) {
    /*this.navCtrl.push('ItemDetailPage', {
      item: item
    });*/

    if(!item.ferida.arquivoClassificado || !item.ferida.arquivoClassificado.pathLogical) {
      this.mostrarErro('Não há nenhuma análise registrada para o arquivo selecionado!');
      return;
    }
    let addModal = this.modalCtrl.create('ItemClassificationPage', {parameter: item});
    addModal.onDidDismiss(item => {
      console.log('ItemClassificationPage closed');
      this.pesquisarPacientes();
    })
    addModal.present();
  }
}
