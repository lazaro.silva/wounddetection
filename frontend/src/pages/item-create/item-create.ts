import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, ViewController, ModalController } from 'ionic-angular';
import { PacienteModel } from "../../models/paciente.model";
import { BasePage } from "../../pages/base-page";
import { AlertController } from 'ionic-angular';
import { FeridaProvider } from "../../providers/ferida/ferida";
import { DomSanitizer } from '@angular/platform-browser';

@IonicPage()
@Component({
  selector: 'page-item-create',
  templateUrl: 'item-create.html'
})
export class ItemCreatePage extends BasePage {
  @ViewChild('fileInput') fileInput;

  isReadyToSave: boolean;

  item: any;

  form: FormGroup;
  paciente: PacienteModel;

  constructor(public navCtrl: NavController, public viewCtrl: ViewController, 
              formBuilder: FormBuilder, public camera: Camera, 
              public alertCtrl: AlertController, private feridaProvider:FeridaProvider, 
              public sanitizer: DomSanitizer, public modalCtrl: ModalController) {
    super(alertCtrl, sanitizer);
    this.paciente = <PacienteModel>{};
    this.form = formBuilder.group({
      profilePic: [''],
      profilePhoto: [''],
      name: ['', Validators.required],
      about: ['']
    });

    // Watch the form for changes, and
    this.form.valueChanges.subscribe((v) => {
      this.isReadyToSave = this.form.valid;
    });
  }

  ionViewDidLoad() {

  }

  getPicture() {
    /*if (Camera['installed']()) {
      this.camera.getPicture({
        destinationType: this.camera.DestinationType.DATA_URL,
        targetWidth: 96,
        targetHeight: 96
      }).then((data) => {
        this.form.patchValue({ 'profilePic': 'data:image/jpg;base64,' + data });
      }, (err) => {
        alert('Unable to take photo');
      })
    } else {
      this.fileInput.nativeElement.click();
    }*/
    this.fileInput.nativeElement.click();
  }

  getPhoto() {
    this.camera.getPicture({quality: 100,
                            destinationType: this.camera.DestinationType.FILE_URI,
                            encodingType: this.camera.EncodingType.JPEG,
                            mediaType: this.camera.MediaType.PICTURE}).then((imageData) => {
      // imageData is either a base64 encoded string or a file URI
      // If it's base64 (DATA_URL):
      //let base64Image = 'data:image/jpeg;base64,' + imageData;
      this.form.patchValue({ 'profilePhoto': 'data:image/jpg;base64,' + imageData });
    }, (err) => {
     // Handle error
      console.log('Erro ao tirar Foto: '+err);
      this.mostrarErro(err);
    });
  }

  eraseImg(option: number) {
    if(option == 2) {
      this.form.patchValue({ 'profilePhoto': undefined });
    }
  }

  processWebImage(event) {
    let reader = new FileReader();
    reader.onload = (readerEvent) => {

      let imageData = (readerEvent.target as any).result;
      this.form.patchValue({ 'profilePic': imageData });
    };

    reader.readAsDataURL(event.target.files[0]);
  }

  getProfileImageStyle() {
    return 'url(' + this.form.controls['profilePic'].value + ')'
  }

  getProfilePhotoImageStyle() {
    return 'url(' + this.form.controls['profilePhoto'].value + ')'
  }

  /**
   * The user cancelled, so we dismiss without sending data back.
   */
  cancel() {
    this.viewCtrl.dismiss();
  }

  /**
   * The user is done and wants to create the item, so return it
   * back to the presenter.
   */
  done() {
    //if (!this.form.valid) { return; }
    let foto = this.form.controls['profilePic'].value;
    this.paciente.foto = foto;

    this.feridaProvider.salvarPaciente(this.paciente).toPromise().then((data) => {
      this.mostrarSucesso('Registro de Paciente Salvo com Sucesso!');
      this.paciente = data;
      this.openClassificacao();
      this.viewCtrl.dismiss(this.form.value);
    }).catch((e) => {
      if (e) {
        this.mostrarErro(this.tratarMsg(e));
      } else {
        this.mostrarErro('Erro não especificado.');
      }
    });

    /*this.feridaProvider.salvarPaciente(this.paciente).then((retorno: any) => {
      if(retorno) {
        this.viewCtrl.dismiss(this.form.value);
      }else {
        this.mostrarErro('Ocorreram erros ao salvar Formulário!');
      }
    }).catch((retorno) => {
      let msg = this.tratarMsg(retorno.data);
      this.mostrarErro(msg);
    });*/
  }

  openClassificacao() {
    let addModal = this.modalCtrl.create('ItemClassificationPage', {parameter: this.paciente});
    addModal.onDidDismiss(item => {
      console.log('ItemClassificationPage closed');
    })
    addModal.present();
  }
}
