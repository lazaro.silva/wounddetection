import { NgModule } from '@angular/core';
import { TranslateModule } from '@ngx-translate/core';
import { IonicPageModule } from 'ionic-angular';
import { Observacao } from './item-classification-observacao';
import { PipesModule } from '../../pipes/pipes.module';

@NgModule({
  declarations: [
    Observacao
  ],
  imports: [
    IonicPageModule.forChild(Observacao),
    TranslateModule.forChild()
  ],
  exports: [
    Observacao
  ]
})
export class ItemClassificationObservacaoPageModule { }