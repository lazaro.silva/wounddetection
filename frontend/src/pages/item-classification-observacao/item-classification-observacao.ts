import { Component, ViewChild } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { IonicPage, NavController, ViewController, NavParams, ModalController } from 'ionic-angular';
import { PacienteModel } from "../../models/paciente.model";
import { ClassificacaoCoberturaModel } from "../../models/classificacao.cobertura.model";
import { BasePage } from "../../pages/base-page";
import { AlertController } from 'ionic-angular';
import { FeridaProvider } from "../../providers/ferida/ferida";
import { DomSanitizer } from '@angular/platform-browser';
import { observeOn } from 'rxjs/operator/observeOn';

@IonicPage()
@Component({
  selector: 'page-item-classification-observacao',
  templateUrl: 'item-classification-observacao.html'
})
export class Observacao extends BasePage {

  obs: String;

  constructor(params: NavParams, public viewCtrl: ViewController, 
              public alertCtrl: AlertController, public sanitizer: DomSanitizer) {
    super(alertCtrl, sanitizer);
    console.log('Observacao construtor');
  }

  ionViewDidLoad() {
    console.log('Observacao ionViewDidLoad');
  }

  ok() {
    if(!this.obs) {
      this.mostrarErro('Observação: Campo Obrigatório!');
      return;
    }
    let data = { rejeite: true, observacao: this.obs};
    this.viewCtrl.dismiss(data);
  }

  dismiss() {
    let data = { rejeite: false, observacao: this.obs };
    this.viewCtrl.dismiss(data);
  }

  mostrarErro(msg) {
    let alert = this.alertCtrl.create({
        title: 'Erro',
        message: msg,
        buttons: ['Ok'],
        cssClass: 'custom-alert'
    });
    alert.present();
  }
}