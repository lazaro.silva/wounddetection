import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { IonicPage, NavController, ToastController } from 'ionic-angular';

import { User } from '../../providers';
import { MainPage } from '../';
import { AuthProvider } from '../../providers/auth/auth';
import { DeviceDetectorService } from 'ngx-device-detector';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ListMasterPage } from '../list-master/list-master';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html'
})
export class LoginPage {
  // The account fields for the login form.
  // If you're using the username field with or without email, make
  // sure to add it to the type
  account: { email: string, cpf: string, password: string } = {
    email: 'test@example.com',
    cpf: '11111111111',
    password: 'test'
  };

  // Our translated text strings
  //private loginErrorString: string;

  constructor(public navCtrl: NavController,
    //public user: User, 
    public alertCtrl: AlertController,
    //public toastCtrl: ToastController,
    //public translateService: TranslateService
    private auth: AuthProvider, private deviceService: DeviceDetectorService) {
    
    /*this.translateService.get('LOGIN_ERROR').subscribe((value) => {
      this.loginErrorString = value;
    })*/
  }

  private checkDevice(): number {
    if(this.deviceService.isMobile()) {
      return 1;
    } else if(this.deviceService.isTablet()) {
      return 2;
    } 
    return 3;
  }
  
  doLogin() {
    
    //1=Celular;2=Tablet;3=Desktop
    const device = this.checkDevice();
    //tipo Email = 0; CPF = 1
    this.auth.login(this.account.cpf, this.account.password, 0, device).then((retorno: any) => {
        if(retorno) {
          this.navCtrl.setRoot(ListMasterPage);
        }else {
          //this.navCtrl.setRoot(MainPage);
          this.alertCtrl.create({
            message: 'Erro durante o processo de autenticação. Tente novamente!',
            buttons: ['Ok']
          }).present();
        }
      }).catch((retorno) => {
        //this.navCtrl.setRoot(MainPage);
        this.alertCtrl.create({
          message: !retorno.data ? retorno : retorno.data.showMessage,
          buttons: ['Ok']
        }).present();
      });
  }

  // Attempt to login in through our User service
  /*doLogin() {
    this.user.login(this.account).subscribe((resp) => {
      this.navCtrl.push(MainPage);
    }, (err) => {
      this.navCtrl.push(MainPage);
      // Unable to log in
      let toast = this.toastCtrl.create({
        message: this.loginErrorString,
        duration: 3000,
        position: 'top'
      });
      toast.present();
    });
  }*/
}
