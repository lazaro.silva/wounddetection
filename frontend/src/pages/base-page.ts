import { Component } from '@angular/core';
import { NavController, NavParams, AlertController } from 'ionic-angular';
import { AuthProvider } from '../providers/auth/auth';
import { DomSanitizer } from '@angular/platform-browser';

export class BasePage {

    decimalSeparator = ".";
    groupSeparator = ",";
    pureResult: any;
    maskedId: any;
    val: any;
    v: any;
    p: number = 1;

    constructor(public alertCtrl: AlertController, public sanitizer: DomSanitizer) {
    }

    ionViewCanEnter() {
        return true;
    }

    tratarMsg(data): string {
        let msg = (data.showMessage || data.message) + '<br/>';
        if (data.errors && data.errors.length > 0) {
            data.errors.forEach(element => {
                msg += '<p><br/>' + element.showMessage.replace('lbl.', '')+'</p>';
                //msg = `<p>${msg}</p>`;
            });
        }
        return msg;
    }

    mostrarErro(msg) {
        let alert = this.alertCtrl.create({
            title: 'Erro',
            message: msg,
            buttons: ['Ok'],
            cssClass: 'custom-alert'
        });
        alert.present();
    }

    mostrarSucesso(msg) {
        let alert = this.alertCtrl.create({
            title: 'Sucesso',
            message: msg,
            buttons: ['Ok'],
            cssClass: 'custom-alert'
        });
        alert.present();
    }

    formatCpfCnpj(valString) {
        if (!valString) {
            return '';
        }
        let val = this.onlyNumber(valString.toString());
        if(val.length > 14) {
            val = val.substring(0,14);
        }
        const parts = this.unFormat(val).split(this.decimalSeparator);
        this.pureResult = parts;
        if(parts[0].length <= 11){
            this.maskedId = this.cpf_mask(parts[0]);
            return this.maskedId;
        }else{
            this.maskedId = this.cnpj(parts[0]);
            return this.maskedId;
        }
    };

    formatCpf(valString) {
        if (!valString) {
            return '';
        }
        let val = this.onlyNumber(valString.toString());
        if(val.length > 11) {
            val = val.substring(0,11);
        }
        const parts = this.unFormat(val).split(this.decimalSeparator);
        this.pureResult = parts;
        this.maskedId = this.cpf_mask(parts[0]);
        return this.maskedId;
    };

    formatCnpj(valString) {
        if (!valString) {
            return '';
        }
        let val = this.onlyNumber(valString.toString());
        if(val.length > 14) {
            val = val.substring(0,14);
        }
        const parts = this.unFormat(val).split(this.decimalSeparator);
        this.pureResult = parts;
        this.maskedId = this.cnpj(parts[0]);
        return this.maskedId;
    };

    onlyNumber(txt: string) {
        if (txt)
          return txt.toString().replace(/\D/g, '');
        return '';
    }

    formatPhone(valString) {
        if (!valString) {
            return '';
        }
        let val = this.onlyNumber(valString.toString());
        if(val.length > 11) {
            val = val.substring(0,11);
        }
        const parts = this.unFormat(val).split(this.decimalSeparator);
        this.pureResult = parts;
        this.maskedId = this.phone(parts[0]);
        return this.maskedId;
    };

    formatValor(valString) {
        if (!valString) {
            return '';
        }
        let val = valString.toString();
        const parts = this.unFormat(val).split(this.decimalSeparator);
        this.pureResult = parts;
        this.maskedId = this.valor(parts[0]);
        return this.maskedId;
    };

    formatCep(valString) {
        if (!valString) {
            return '';
        }
        let val = this.onlyNumber(valString.toString());
        if(val.length > 8) {
            val = val.substring(0,8);
        }
        const parts = this.unFormat(val).split(this.decimalSeparator);
        this.pureResult = parts;
        this.maskedId = this.cep(parts[0]);
        return this.maskedId;
    };

    unFormat(val) {
        if (!val) {
            return '';
        }
        val = val.replace(/\D/g, '');

        if (this.groupSeparator === ',') {
            return val.replace(/,/g, '');
        } else {
            return val.replace(/\./g, '');
        }
    };

    cnpj(v) {
        v = v.replace(/\D/g, '');
        v = v.replace(/^(\d{2})(\d)/, '$1.$2');
        v = v.replace(/^(\d{2})\.(\d{3})(\d)/, '$1.$2.$3');
        v = v.replace(/\.(\d{3})(\d)/, '.$1/$2');
        v = v.replace(/(\d{4})(\d)/, '$1-$2');
        return v;
    }

    cpf_mask(v) {
        v = v.replace(/\D/g, '');
        v = v.replace(/(\d{3})(\d)/, '$1.$2');
        v = v.replace(/(\d{3})(\d)/, '$1.$2');
        v = v.replace(/(\d{3})(\d{1,2})$/, '$1-$2');
        return v;
    }

    phone(v) {
        v = v.replace(/\D/g, "");
        v = v.replace(/^(\d{2})(\d)/g, "($1) $2");
        v = v.replace(/(\d)(\d{4})$/, "$1-$2");
        return v;
    }

    valor(v) {
        if (v && typeof (v) == 'string') {
            v = v.replace(/\D/g, "");
            v = v.replace(/(\d)(\d{8})$/, "$1.$2");
            v = v.replace(/(\d)(\d{5})$/, "$1.$2");
            v = v.replace(/(\d)(\d{2})$/, "$1,$2");
        }
        return v;
    }

    cep(v){
        v=v.replace(/\D/g,"")
        v=v.replace(/^(\d{5})(\d)/,"$1-$2")
        return v
    }

    sanitizarArquivo(blob:Blob) {
        //var blob = new Blob([item.ferida.fotoResource], { type: 'image/jpg' });
        console.log('sanitizarArquivo', blob);
        if(blob)
          return this.sanitizer.bypassSecurityTrustUrl(window.URL.createObjectURL(blob));
        return '';
    }    

}
