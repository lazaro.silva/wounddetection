import { Injectable } from '@angular/core';
import { BaseProvider } from '../base';
import { Http, Response } from '@angular/http';
import { UUID } from 'angular2-uuid';
import { Observable } from 'rxjs/Observable';
import { Usuario } from '../../models/usuario.model';


@Injectable()
export class AuthProvider extends BaseProvider {

  constructor(public http: Http) {
    super();
  }

  hasPermission(rolesGranted: string[]) {
    let authorized = false;
    if (localStorage.getItem("token") && localStorage.getItem("token") != "{}") {
      let dataToken = JSON.parse(localStorage.getItem("token")).data;
      if (dataToken && dataToken.authorities) {
        let userAuthorities = dataToken.authorities.map((authority) => authority = authority.authority);
        var i;
        for (i = 0; i < rolesGranted.length; i++) {
          authorized = (userAuthorities.indexOf(rolesGranted[i]) >= 0)
          if (authorized)
            return authorized;
        }
      }
    }
    return authorized;
  }

  login(username: string, password: string, type: number, device: number) {
    password = this.encode(password);
    return new Promise((resolve, reject) => {
      var req = new XMLHttpRequest();
      req.onloadend = function () {
        if (req.status !== 200) {
          resolve(false);
        } else {
          let r = JSON.parse(req.responseText);
          if (r.success) {
            localStorage.setItem("token", req.responseText);
            resolve(r);
          } else {
            reject(r);
          }
        }
      };
      let bodyPart = {
        "username": username,
        "password": password,
        "typeAuthentication": type,
        "device": device
      };

      req.ontimeout = function (e) {
        // Timeout na chamada XMLHttpRequest. Ação de timeout aqui.
        reject('Servidor Fora: TimeOut');
      };

      req.open("POST", this.api + '/login/authenticate', true);
      req.timeout = 2000; // tempo em milisegundos
      req.setRequestHeader('Content-Type', 'application/json');
      req.send(JSON.stringify(bodyPart));
    });
  }

  logout() {
    localStorage.clear();
  }

  changePassword(email: string, cpf: string, oldPassword: string, newPassword: string, confirmNewPassword: string) {
    oldPassword = this.encode(oldPassword);
    newPassword = this.encode(newPassword);
    confirmNewPassword = this.encode(confirmNewPassword);
    return new Promise((resolve, reject) => {
      var req = new XMLHttpRequest();
      req.onloadend = function () {
        if (req.status !== 200) {
          resolve(false);
        } else {
          let r = JSON.parse(req.responseText);
          if (r.success) {
            localStorage.setItem("token", req.responseText);
            resolve(r);
          } else {
            reject(r);
          }
        }
      };
      let bodyPart = {
        "email": email,
        "cpf": cpf,
        "oldPassword": oldPassword,
        "newPassword": newPassword,
        "confirmNewPassword": confirmNewPassword
      };

      req.open("POST", this.api + '/login/change/password', true);
      req.setRequestHeader('Content-Type', 'application/json');
      req.send(JSON.stringify(bodyPart));
    });
  }


  forgotPassword(email: string, cpf: string) {
    return new Promise((resolve, reject) => {
      var req = new XMLHttpRequest();
      req.onloadend = function () {
        if (req.status !== 200) {
          resolve(false);
        } else {
          let r = JSON.parse(req.responseText);
          if (r.success) {
            resolve(r);
          } else {
            reject(r);
          }
        }
      };
      let bodyPart = {
        "email": email,
        "cpf": cpf,
        "emailSubject": "[ASBAPI] - Alteração de Senha de Usuário" 
      };

      req.open("POST", this.api + '/login/forgot/password', true);
      req.setRequestHeader('Content-Type', 'application/json');
      req.send(JSON.stringify(bodyPart));
    });
  }

  validateToken() {
    let tokenStr = localStorage.getItem("token");
    if(tokenStr) {
      let data = JSON.parse(tokenStr);
      if(data.data) {
        tokenStr = data.data.token;
      }
    }
    return new Promise((resolve, reject) => {
      var req = new XMLHttpRequest();
      req.onloadend = function () {
        if (req.status !== 200) {
          resolve(false);
        } else {
          let r = JSON.parse(req.responseText);
          if (r.success) {
            //localStorage.setItem("token", req.responseText);
            resolve(r);
          } else {
            reject(r);
          }
        }
      };
      let bodyPart = {
        "authToken": tokenStr
      };

      req.ontimeout = function (e) {
        // Timeout na chamada XMLHttpRequest. Ação de timeout aqui.
        localStorage.clear();
        reject('Servidor Fora: TimeOut');
      };

      req.open("POST", this.api + '/login/validateToken', true);
      req.timeout = 2000; // tempo em milisegundos
      req.setRequestHeader('Content-Type', 'application/json');
      req.send(tokenStr);
    });
  }

  isAuthenticated(): boolean {
    let tokenStr = localStorage.getItem("token");
    if (tokenStr) {
      let token = JSON.parse(tokenStr);
      return (token.success);
    } else {
      return false;
    }
  }

  getCurrentUser(): Usuario {
    let user = <Usuario>{};
    let tokenStr = localStorage.getItem("token");
    if (tokenStr) {
      let obj = JSON.parse(tokenStr).data;
      user.id = obj.id;
      user.name = obj.name;
      user.cpf = obj.cpf;
      user.email = obj.email;
      user.celular = obj.celular;
      user.telephone = obj.telephone;
    }
    return user;
  }

  encode(text) {
    var uuid = UUID.UUID();
    if (text.length > 0) {
      var j = uuid.length - text.length;
      while (j < 0) {
        j = j + uuid.length;
      }
      var result = uuid + "-";
      for (var i = 0; i < text.length; i++) {
        if (j > uuid.length) {
          j = 0;
        }
        result = result + this.formatNumForCode(text.charCodeAt(i) + uuid.charCodeAt(j));
        j++;
      }
      return result;
    } else {
      return text;
    }
  }

  formatNumForCode(num) {
    var text = "" + num;
    while (text.length < 3) {
      text = "0" + text;
    }
    return text;
  }

  
}
