import { Response, RequestOptions, Headers, ResponseContentType } from '@angular/http';
import { Observable } from 'rxjs';
// import { Network } from '@ionic-native/network';

const extractError = (error: Response | any): string => {
    // In a real world app, we might use a remote logging infrastructure
    let errMsg: string;
    if (error instanceof Response) {
        const body = error.json() || '';
        const err = body.error || JSON.stringify(body);
        errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
    } else {
        errMsg = error.message ? error.message : error.toString();
    }
    console.error(errMsg);
    return errMsg;
}

export abstract class BaseProvider {
    
    protected api: string = "http://localhost:9518/survey";
    // protected api: string = "/api";
    protected host: string;

    protected handlePromiseError(error: Response | any): Promise<any> {
        return Promise.reject(extractError(error));
    }

    protected handleObservableError(error: Response | any): Observable<any> {
        return Observable.throw(extractError(error));
    }

    onlyNumber(txt: string) {
        if (txt)
            return txt.toString().replace(/\D/g, '');
        return '';
    }

    protected buildOpts(file?: boolean, contentType?: string, arrayBuffer?: boolean): RequestOptions {
        let tokenStr = localStorage.getItem('token');
        //let idLoja = this.onlyNumber(localStorage.getItem('idLoja'));
        let tokenObj = JSON.parse(tokenStr);
        let opts = new RequestOptions();
        opts.headers = new Headers();
        opts.headers.append('x-auth-token', tokenObj.data.token);
        //opts.headers.append('idLoja', idLoja);
        if (!file)
            opts.headers.append('Content-Type', 'application/json');
        if (contentType) {
            opts.headers.append('Content-Type', contentType);
        }
        if (arrayBuffer)
            opts.responseType = ResponseContentType.ArrayBuffer;

        return opts;
    }
}