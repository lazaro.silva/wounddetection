import { Injectable } from '@angular/core';
import { BaseProvider } from '../base';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { PacienteModel } from '../../models/paciente.model';
import { ClassificacaoModel } from '../../models/classificacao.model';
import { stringify } from '@angular/core/src/render3/util';


@Injectable()
export class FeridaProvider extends BaseProvider {

  constructor(public http: Http) {
    super();
  }

  salvarPaciente(paciente:PacienteModel): Observable<any> {
    let url = this.api + '/ferida/salvarPaciente';
    return this.http.post(url, JSON.stringify(paciente), this.buildOpts())
      .map((response: Response) => {
        let _data = response.json().data;
        if (response.json().success)
          return _data;
        else {
          throw _data;
        }
      })
      .catch((e) => { throw e; });
  }

  pesquisarPacientes(): Observable<any> {
    let url = this.api + '/ferida/pesquisarPacientes';
    return this.http.get(url, this.buildOpts()).map((response: Response) => {
        let _data = response.json().data;
        if (response.json().success)
          return _data;
        else {
          throw _data;
        }
      })
      .catch((e) => { throw e; });
  }

  getNomeCobertura(label:String): Observable<any> {
    let url = this.api + '/ferida/nomeCobertura?label='+label;
    return this.http.get(url, this.buildOpts()).map((response: Response) => response.json().data);
  }

  /*pesquisarPacientes(): Promise<any> {
    return new Promise((resolve, reject) => {
      let url = this.api + '/ferida/pesquisarPacientes';
      var req = new XMLHttpRequest();
      req.onloadend = function (data) {
        if (req.status !== 200) {
          reject(req.responseText);
        } else {
          let _data = JSON.parse(req.response);
          if (_data.success)
            resolve(_data.data);
          else {
            throw _data.data;
          }
        }
      };

      let tokenStr = localStorage.getItem('token');
      //let idLoja = this.onlyNumber(localStorage.getItem('idLoja'));
      let tokenObj = JSON.parse(tokenStr);

      req.open('GET', url, false);
      req.setRequestHeader('Content-Type', 'application/json');
      req.setRequestHeader('x-auth-token', tokenObj.data.token);
      req.send();
    });
  }*/

  obterArquivo(pathLogical:string): Observable<any> {
    let url = this.api + '/' + pathLogical;
    return this.http.get(url, this.buildOpts(true, 'image/jpg', true)).map((response: Response) => {
      return response; //new Blob([response], { type: 'image/jpg' });
    }).catch((e) => { throw e; });
  }

  fecharAnalise(classificacao: ClassificacaoModel): Observable<any> {
    let url = this.api + '/ferida/fecharAnalise';
    return this.http.put(url, JSON.stringify(classificacao), this.buildOpts())
      .map((response: Response) => {
        let _data = response.json().data;
        if (response.json().success)
          return _data;
        else {
          throw _data;
        }
      })
      .catch((e) => { throw e; });
  }

  /*obterArquivo(pathLogical:string):Promise<any>{
      return new Promise((resolve, reject) => {
        let url = this.api + '/' + pathLogical;
        var req = new XMLHttpRequest();
        req.onloadend = function (data) {
          if (req.status !== 200) {
            reject(req.responseText);
          } else {
            //try {
              var blob = new Blob([req.response], { type: 'image/jpg' });
              resolve(blob);
            /*} catch (ex) {
              console.log(ex);
            }*
          }
        };

        let tokenStr = localStorage.getItem('token');
        //let idLoja = this.onlyNumber(localStorage.getItem('idLoja'));
        let tokenObj = JSON.parse(tokenStr);

        req.open('GET', url, false);
        req.setRequestHeader('Content-Type', 'image/jpg');
        req.setRequestHeader('x-auth-token', tokenObj.data.token);
        req.responseType = 'arraybuffer';
        req.send();
      });
  }*/

  /*salvarPaciente(paciente:PacienteModel) {
    return new Promise((resolve, reject) => {
      var req = new XMLHttpRequest();
      req.onloadend = function () {
        if (req.status !== 200) {
          resolve(false);
        } else {
          let r = JSON.parse(req.responseText);
          if (r.success) {
            resolve(r);
          } else {
            reject(r);
          }
        }
      };

      req.open("POST", this.api + '/ferida/salvarPaciente', {});
      req.setRequestHeader('Content-Type', 'application/json');
      req.send(JSON.stringify(paciente));
    });
  }*/

}
