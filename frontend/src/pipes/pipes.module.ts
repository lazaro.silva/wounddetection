import { NgModule } from '@angular/core';
import { OrderBy } from './orderBy';

@NgModule({
	declarations: [OrderBy],
	imports: [],
	exports: [OrderBy]
})
export class PipesModule {}