export interface TecidoModel {
    id: number;
    dtCreate: Date;
    idUserCreate: number;

    nome: String;
    label: String;
    filhos: TecidoModel[];
}