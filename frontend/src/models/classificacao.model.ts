import { ClassificacaoTecidoModel } from "./classificacao.tecido.model";

export interface ClassificacaoModel {
    id: number;
    dtCreate: Date;
    idUserCreate: number;
    dtUpdate: Date;
    idUserUpdate: number;

    observacoes: String;
    tecidos: ClassificacaoTecidoModel[];
}