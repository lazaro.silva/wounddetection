import { FileModel } from "./file.model";

export interface Usuario {
    id: number;
    name: string;
    cpf: string;
    email: string;
    celular: string;
    telephone: string;
    roles?:any[];
    file:FileModel;
    authorities?:any[];
}