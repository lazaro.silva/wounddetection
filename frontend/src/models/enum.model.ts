import { FileModel } from "./file.model";

export interface EnumModel {
    codigo: number;
    description: String;
    label: String;
}