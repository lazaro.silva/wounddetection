import { TecidoModel } from "./tecido.model";
import { ClassificacaoCoberturaModel } from "./classificacao.cobertura.model";

export interface ClassificacaoTecidoModel {
    id: number;
    dtCreate: Date;
    idUserCreate: number;

    tecido: TecidoModel;
    ordem: number;
    confidencia: number;
    coordenadas: String;
    coberturasClassificadas: ClassificacaoCoberturaModel[];
}