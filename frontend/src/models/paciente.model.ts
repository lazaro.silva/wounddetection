import { FeridaModel } from "./ferida.model";

export interface PacienteModel {
    id: number;
    dtCreate: Date;
    idUserCreate: number;
    dtUpdate: Date;
    idUserUpdate: number;
    nome: string;
    dtNascimento: Date;
    nuProntuario: string;
    ferida: FeridaModel;
    foto: any;
}