import { EnumModel } from "./enum.model";

export interface ClassificacaoCoberturaModel {
    id: number;
    dtCreate: Date;
    idUserCreate: number;
    dtUpdate: Date;
    idUserUpdate: number;

    isAceito: boolean;
    coberturaClassificada: EnumModel;
}