import { FileModel } from "./file.model";
import { ClassificacaoModel } from "./classificacao.model";

export interface FeridaModel {
    id: number;
    dtCreate: Date;
    idUserCreate: number;
    dtUpdate: Date;
    idUserUpdate: number;
    arquivoOriginal: FileModel;
    arquivoClassificado: FileModel;
    fotoResource: Blob;
    fotoClassificacao: Blob;
    classificacao: ClassificacaoModel;
}