export interface FileModel {
    id:string;
    dtCreate:Date;
    idUserCreate:string,
    nmEntity:string;
    name:string;
    pathPhysical:string;
    pathLogical:string;
    contentType:string;
    size:string;
}